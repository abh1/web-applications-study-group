"use strict";
var ChemicalElement = (function () {
    function ChemicalElement(atomicNumber) {
        this.number = atomicNumber;
        this.data = ChemicalElement.findJsonData(atomicNumber);
        this.type = ChemicalElement.findElementType(atomicNumber);
    }
    ChemicalElement.findJsonData = function (number) {
        for (var i = 0; i < chemicalElementData.length; i++) {
            var e = chemicalElementData[i];
            if (e['number'] == number) {
                return e;
            }
        }
        return null;
    };
    ChemicalElement.findElementType = function (number) {
        for (var t in chemicalElementCategoryData) {
            var numbers = chemicalElementCategoryData[t];
            if (numbers.indexOf(number) >= 0) {
                return t;
            }
        }
        return 'unknown';
    };
    Object.defineProperty(ChemicalElement.prototype, "symbol", {
        get: function () {
            return this.data['symbol'];
        },
        enumerable: true,
        configurable: true
    });
    ChemicalElement.prototype.getName = function (language) {
        return this.data['name'][language.code];
    };
    return ChemicalElement;
}());
exports.ChemicalElement = ChemicalElement;
//# sourceMappingURL=chemical-element.js.map