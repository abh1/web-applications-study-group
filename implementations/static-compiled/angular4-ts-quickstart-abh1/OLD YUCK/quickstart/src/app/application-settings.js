"use strict";
var language_info_1 = require('./language-info');
var ApplicationSettings = (function () {
    function ApplicationSettings() {
        this._language = ApplicationSettings.getLanguageFromLocalStorage();
        this.languageOptions = language_info_1.LanguageInfo.ALL_LANGUAGES;
    }
    ApplicationSettings.getLanguageFromLocalStorage = function () {
        var selectedLocaleId = localStorage.getItem('localeId');
        for (var _i = 0, _a = language_info_1.LanguageInfo.ALL_LANGUAGES; _i < _a.length; _i++) {
            var li = _a[_i];
            if (li.code === selectedLocaleId) {
                return li;
            }
        }
        return language_info_1.LanguageInfo.ALL_LANGUAGES[0];
    };
    Object.defineProperty(ApplicationSettings.prototype, "language", {
        get: function () {
            return this._language;
        },
        set: function (value) {
            localStorage.setItem('localeId', value.code);
            this._language = value;
        },
        enumerable: true,
        configurable: true
    });
    return ApplicationSettings;
}());
exports.ApplicationSettings = ApplicationSettings;
//# sourceMappingURL=application-settings.js.map