"use strict";
var LanguageInfo = (function () {
    function LanguageInfo(code, name) {
        this.code = code;
        this.name = name;
    }
    LanguageInfo.ALL_LANGUAGES = [
        new LanguageInfo("en", "English"),
        new LanguageInfo("de", "Deutsch"),
        new LanguageInfo("fr", "Français"),
        new LanguageInfo("es", "Español"),
        new LanguageInfo("it", "Italiano"),
    ];
    return LanguageInfo;
}());
exports.LanguageInfo = LanguageInfo;
//# sourceMappingURL=language-info.js.map