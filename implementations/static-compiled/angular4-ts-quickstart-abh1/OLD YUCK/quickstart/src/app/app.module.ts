import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { ElementCellComponent }  from './element-cell.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [ 
    AppComponent,
    ElementCellComponent 
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
