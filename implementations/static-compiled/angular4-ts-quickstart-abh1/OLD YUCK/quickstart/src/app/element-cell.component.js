"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var chemical_element_1 = require('./chemical-element');
var application_settings_1 = require('./application-settings');
var ElementCellComponent = (function () {
    function ElementCellComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', chemical_element_1.ChemicalElement)
    ], ElementCellComponent.prototype, "element", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', application_settings_1.ApplicationSettings)
    ], ElementCellComponent.prototype, "settings", void 0);
    ElementCellComponent = __decorate([
        core_1.Component({
            selector: '[element-cell]',
            template: "\n        <div class=\"{{element ? 'element ' + element.type : ''}}\">\n            <ng-container *ngIf=\"element\">\n                <div class=\"atomic-number\">{{element.number}}</div>\n                <div class=\"symbol\">{{element.symbol}}</div>\n                <div class=\"name\">{{element.getName(settings.language)}}</div>\n            </ng-container>\n        </div>\n    \n    ",
            styles: ["\n\ndiv.element {\n\tborder-radius: 8px;\n\tborder: 1px solid black;\n    padding: 3px;\n}\n\n.element .atomic-number {\n\ttext-align: right;\n}\n\n.element .symbol {\n\tfont-size: 200%;\n\ttext-align: center;\n}\n\n.element .name {\n\tfont-size: xx-small;\n\ttext-align: center;\n}\n\n.element.hydrogen {\n\tbackground: linear-gradient(white, Gainsboro);\n}\n\n.element.alkali-metal {\n\tbackground: linear-gradient(white, Thistle);\n}\n\n.element.alkaline-earth-metal {\n\tbackground: linear-gradient(white, Lavender);\n}\n\n.element.nonmetal {\n\tbackground: linear-gradient(white, Khaki);\n}\n\n.element.metalloid {\n\tbackground: linear-gradient(white, Orange);\n}\n\n.element.transition-metal {\n\tbackground: linear-gradient(white, Gray);\n}\n\n.element.post-transition-metal {\n\tbackground: linear-gradient(white, DarkGray);\n}\n\n@keyframes halogen-animation {\n    from {background-color: red;}\n    to {background-color: green;}\n}\n\n\n.element.halogen {\n    animation-name: halogen-animation;\n    animation-duration: 0.5s;\n\tanimation-iteration-count: infinite;\n\tanimation-direction: alternate;\n}\n\n.element.noble-gas {\n\tbackground: linear-gradient(white, DarkSalmon);\n}\n\n\n.element.lanthanoid {\n\tbackground: linear-gradient(white, LightBlue);\n}\n\n.element.actinoid {\n\tbackground: linear-gradient(white, Cyan);\n}\n\n    "],
        }), 
        __metadata('design:paramtypes', [])
    ], ElementCellComponent);
    return ElementCellComponent;
}());
exports.ElementCellComponent = ElementCellComponent;
//# sourceMappingURL=element-cell.component.js.map