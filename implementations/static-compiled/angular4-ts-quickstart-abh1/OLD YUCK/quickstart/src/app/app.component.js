"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var chemical_element_1 = require('./chemical-element');
var application_settings_1 = require('./application-settings');
var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Periodic Table';
        this.elements = AppComponent.initElements();
        this.settings = new application_settings_1.ApplicationSettings();
    }
    AppComponent.prototype.initLoop = function (n) {
        var ret = new Array();
        for (var i = 1; i <= n; i++) {
            ret.push(i);
        }
        return ret;
    };
    AppComponent.initElements = function () {
        var ret = new Array();
        for (var i = 1; i <= 118; i++) {
            ret.push(new chemical_element_1.ChemicalElement(i));
        }
        return ret;
    };
    AppComponent.prototype.getElementsForRow = function (rowId) {
        if (rowId == '1') {
            return AppComponent.buildRow(18, 1, 1, 2, 1);
        }
        else if (rowId == '2') {
            return AppComponent.buildRow(18, 3, 2, 10, 6);
        }
        else if (rowId == '3') {
            return AppComponent.buildRow(18, 11, 2, 18, 6);
        }
        else if (rowId == '4') {
            return AppComponent.buildRow(18, 19, 18, 0, 0);
        }
        else if (rowId == '5') {
            return AppComponent.buildRow(18, 37, 18, 0, 0);
        }
        else if (rowId == '6') {
            return AppComponent.buildRow(18, 55, 2, 86, 15);
        }
        else if (rowId == '7') {
            return AppComponent.buildRow(18, 87, 2, 118, 15);
        }
        else if (rowId == 'lanthanides') {
            return AppComponent.buildRow(15, 57, 15, 0, 0);
        }
        else if (rowId == 'actinides') {
            return AppComponent.buildRow(15, 89, 15, 0, 0);
        }
    };
    AppComponent.buildRow = function (rowLength, leftmostAtomicNumber, leftCount, rightmostAtomicNumber, rightCount) {
        var ret = new Array(rowLength);
        for (var i = 0; i < leftCount; i++) {
            ret[i] = new chemical_element_1.ChemicalElement(leftmostAtomicNumber + i);
        }
        if (rightmostAtomicNumber > leftmostAtomicNumber) {
            for (var i = 0; i < rightCount; i++) {
                ret[rowLength - 1 - i] = new chemical_element_1.ChemicalElement(rightmostAtomicNumber - i);
            }
        }
        return ret;
    };
    // idea from http://stackoverflow.com/a/40363782/316578
    AppComponent.prototype.onSelectLanguage = function (localeId) {
        location.reload();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n  \t\t<div class='periodic-table'>\n\t\t\t<h1 i18n>The periodic table of the elements</h1>\n\t\t\t<dt i18n>Select language: </dt><dd>\n\t\t\t\t<select [(ngModel)]=\"settings.language\" (change)=\"onSelectLanguage($event.target.value)\">\n\t\t\t\t\t<option *ngFor=\"let lang of settings.languageOptions\" [ngValue]=\"lang\">{{lang.name}}</option>\n\t\t\t\t</select>\n\t\t\t</dd>\n\t\t\t<h2 i18n>Main Table</h2>\n\t\t\t<table>\n\t\t\t\t<colgroup>\n\t\t\t\t\t<col class=\"periods\">\n\t\t\t\t\t<col class=\"group\" *ngFor=\"let i of this.initLoop(18)\">\n\t\t\t\t</colgroup>\t\t\n\t\t\t\t<thead>\n\t\t\t\t\t<tr>\n\t\t\t\t\t\t<th></th>\n\t\t\t\t\t\t<th *ngFor=\"let i of this.initLoop(18)\">{{i}}</th>\n\t\t\t\t\t</tr>\n\t\t\t\t</thead>\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr *ngFor=\"let periodNumber of ['1', '2', '3', '4', '5', '6', '7']\" class=\"period\" >\n\t\t\t\t\t\t<th>Period {{periodNumber}}</th>\n\t\t\t\t\t\t<td *ngFor=\"let element of this.getElementsForRow(periodNumber)\" element-cell [element]=\"element\" [settings]=\"settings\"></td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\n\n\t\t\t<h2 i18n>Lanthanides</h2>\n\n\t\t\t<table>\n\t\t\t\t<colgroup>\n\t\t\t\t\t<col class=\"periods\">\n\t\t\t\t\t<col class=\"group\" *ngFor=\"let i of this.initLoop(15)\">\n\t\t\t\t</colgroup>\t\t\n\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr class=\"period\" >\n\t\t\t\t\t\t<th>Lanthanides</th>\n\t\t\t\t\t\t<td *ngFor=\"let element of this.getElementsForRow('lanthanides')\" element-cell [element]=\"element\" [settings]=\"settings\"></td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\t\n\n\t\t\t<h2 i18n>Actinides</h2>\n\n\t\t\t<table>\n\t\t\t\t<colgroup>\n\t\t\t\t\t<col class=\"periods\">\n\t\t\t\t\t<col class=\"group\" *ngFor=\"let i of this.initLoop(15)\">\n\t\t\t\t</colgroup>\t\t\n\n\t\t\t\t<tbody>\n\t\t\t\t\t<tr class=\"period\" >\n\t\t\t\t\t\t<th>Actinides</th>\n\t\t\t\t\t\t<td *ngFor=\"let element of this.getElementsForRow('actinides')\" element-cell [element]=\"element\" [settings]=\"settings\"></td>\n\t\t\t\t\t</tr>\n\t\t\t\t</tbody>\n\t\t\t</table>\t\n\t\t</div>\t\t\t\n\t",
            styles: ["\n.periodic-table {\n\tfont-family: Arial, sans-serif;\n\t\n}\n\ntable {\n\tborder-collapse: separate;\n\tborder-spacing: 3px;\n\ttable-layout: fixed;\n}\n\ntable colgroup col.periods {\n\twidth: 20ex;\n}\n\ntable colgroup col.group {\n\twidth: 10ex;\n}\n\ntable td {\n\tpadding: 3px;\n}\n\n\n\t"]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map