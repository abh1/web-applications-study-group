import { Component, Input } from '@angular/core';
import { ChemicalElement } from './chemical-element';
import { ApplicationSettings } from './application-settings';

@Component({
    selector: '[element-cell]',
    template: `
        <div class="{{element ? 'element ' + element.type : ''}}">
            <ng-container *ngIf="element">
                <div class="atomic-number">{{element.number}}</div>
                <div class="symbol">{{element.symbol}}</div>
                <div class="name">{{element.getName(settings.language)}}</div>
            </ng-container>
        </div>
    
    `,
    styles: [`

div.element {
	border-radius: 8px;
	border: 1px solid black;
    padding: 3px;
}

.element .atomic-number {
	text-align: right;
}

.element .symbol {
	font-size: 200%;
	text-align: center;
}

.element .name {
	font-size: xx-small;
	text-align: center;
}

.element.hydrogen {
	background: linear-gradient(white, Gainsboro);
}

.element.alkali-metal {
	background: linear-gradient(white, Thistle);
}

.element.alkaline-earth-metal {
	background: linear-gradient(white, Lavender);
}

.element.nonmetal {
	background: linear-gradient(white, Khaki);
}

.element.metalloid {
	background: linear-gradient(white, Orange);
}

.element.transition-metal {
	background: linear-gradient(white, Gray);
}

.element.post-transition-metal {
	background: linear-gradient(white, DarkGray);
}

@keyframes halogen-animation {
    from {background-color: red;}
    to {background-color: green;}
}


.element.halogen {
    animation-name: halogen-animation;
    animation-duration: 0.5s;
	animation-iteration-count: infinite;
	animation-direction: alternate;
}

.element.noble-gas {
	background: linear-gradient(white, DarkSalmon);
}


.element.lanthanoid {
	background: linear-gradient(white, LightBlue);
}

.element.actinoid {
	background: linear-gradient(white, Cyan);
}

    `],
})
export class ElementCellComponent {
    @Input()
    element: ChemicalElement;

    @Input()
    settings: ApplicationSettings;    
}