import { LanguageInfo } from './language-info';
import { ChemicalElementData } from './periodic-table-data';

export class ChemicalElement {
	number: number;
	data: Object;
	type: string;

	public constructor(atomicNumber : number) {
		this.number = atomicNumber;
		this.data = ChemicalElement.findJsonData(atomicNumber);
		this.type = ChemicalElement.findElementType(atomicNumber);
	}


	private static findJsonData(number: number): Object {
		for (let i = 0; i < ChemicalElementData.chemicalElementData.length; i++) {
			let e = ChemicalElementData.chemicalElementData[i];
			if (e['number'] == number) {
				return e;
			}
		}
		return null;
	}

	
	private static findElementType(number: number): string {
		for (let t in ChemicalElementData.chemicalElementCategoryData) {
			let numbers : Array<number> = ChemicalElementData.chemicalElementCategoryData[t] as Array<number>;
			if (numbers.indexOf(number) >= 0) {
				return t;
			}
		}
		return 'unknown';
	}
	
	public get symbol() : string {
		return this.data['symbol'];
	}

	public getName(language: LanguageInfo) : string {
		return this.data['name'][language.code];
	}
}
