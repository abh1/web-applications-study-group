import { Component } from '@angular/core';
import { LanguageInfo } from './language-info';
import { ChemicalElement } from './chemical-element';
import { ApplicationSettings } from './application-settings';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  { 
	title = 'Periodic Table'; 
	elements = AppComponent.initElements();
	settings = new ApplicationSettings();
	
	public initLoop(n: number): Array<number> {
		let ret = new Array();
		for (let i = 1; i <= n; i++) {
			ret.push(i);
		}

		return ret;
	}
	private static initElements(): Array<ChemicalElement> {
		let ret = new Array<ChemicalElement>();
		for (let i = 1; i <= 118; i++) {
			ret.push(new ChemicalElement(i));
		}
		return ret;
	}

	public getElementsForRow(rowId: string) : Array<ChemicalElement> {
		if (rowId == '1') {
			return AppComponent.buildRow(18, 1, 1, 2, 1);
		} else if (rowId == '2') {
			return AppComponent.buildRow(18, 3, 2, 10, 6);
		} else if (rowId == '3') {
			return AppComponent.buildRow(18, 11, 2, 18, 6);
		} else if (rowId == '4') {
			return AppComponent.buildRow(18, 19, 18, 0, 0);
		} else if (rowId == '5') {
			return AppComponent.buildRow(18, 37, 18, 0, 0);
		} else if (rowId == '6') {
			return AppComponent.buildRow(18, 55, 2, 86, 15);
		} else if (rowId == '7') {
			return AppComponent.buildRow(18, 87, 2, 118, 15);
		} else if (rowId == 'lanthanides') {
			return AppComponent.buildRow(15, 57, 15, 0, 0);
		} else if (rowId == 'actinides') {
			return AppComponent.buildRow(15, 89, 15, 0, 0);
		}
	}

	private static buildRow(rowLength: number, leftmostAtomicNumber: number, leftCount: number, rightmostAtomicNumber: number, rightCount: number) : Array<ChemicalElement> {
		let ret = new Array<ChemicalElement>(rowLength);

		for (let i = 0; i < leftCount; i++) {
			ret[i] = new ChemicalElement(leftmostAtomicNumber + i);
		}
		if (rightmostAtomicNumber > leftmostAtomicNumber) {
			for (let i = 0; i < rightCount; i++) {
				ret[rowLength - 1 - i] = new ChemicalElement(rightmostAtomicNumber - i);
			}
		}

		return ret;

	}

	// idea from http://stackoverflow.com/a/40363782/316578
	public onSelectLanguage(localeId: string): void {
		location.reload();
	}

}