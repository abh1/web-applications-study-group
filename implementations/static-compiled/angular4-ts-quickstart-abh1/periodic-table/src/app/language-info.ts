export class LanguageInfo {
	code: string;
	name: string;

	private constructor(code: string, name: string) {
		this.code = code;
		this.name = name;
	}
	
	public static ALL_LANGUAGES: Array<LanguageInfo> = [
		new LanguageInfo("en", "English"),
		new LanguageInfo("de", "Deutsch"),
		new LanguageInfo("fr", "Français"),
		new LanguageInfo("es", "Español"),
		new LanguageInfo("it", "Italiano"),

	];
}
