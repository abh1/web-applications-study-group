import { LanguageInfo } from './language-info';

export class ApplicationSettings {
	_language: LanguageInfo = ApplicationSettings.getLanguageFromLocalStorage();

	languageOptions: Array<LanguageInfo> = LanguageInfo.ALL_LANGUAGES;

	private static getLanguageFromLocalStorage(): LanguageInfo {
		let selectedLocaleId = localStorage.getItem('localeId');
		for (let li of LanguageInfo.ALL_LANGUAGES) {
			if (li.code === selectedLocaleId) {
				return li;
			}
		}
		return LanguageInfo.ALL_LANGUAGES[0];
	}

	public get language() {
		return this._language;
	}

	public set language(value) {
		localStorage.setItem('localeId', value.code);
		this._language = value;
	}
}