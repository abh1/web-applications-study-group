To run Ant's Angular2 implementation:
- install node.js (don't uncheck option to install npm alongside)
- go into folder "quickstart"
- run "npm install"
- run "npm start"
