import React from 'react'
import ReactDOM from 'react-dom'

import HarryPotterEscapeRoom from './HarryPotterEscapeRoom'

require('./card-game.css');

ReactDOM.render(<HarryPotterEscapeRoom />, document.getElementById('app'))
