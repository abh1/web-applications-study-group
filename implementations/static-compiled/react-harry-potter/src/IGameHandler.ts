export default interface IGameHandler {
    startGame(player: number): void;

    winGame(player: number): void;
}
