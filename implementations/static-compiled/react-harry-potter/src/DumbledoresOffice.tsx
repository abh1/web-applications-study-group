import * as React from 'react';
import IGameHandler from './IGameHandler'

interface IOfficeState {
    present: boolean;
    continueButtonShown: boolean;
}

interface IOfficeProps {
    player: number;
    showSpell: boolean;
    handler: IGameHandler;
}

export default class DumbledoresOffice extends React.Component<IOfficeProps, IOfficeState> {

    constructor(props: IOfficeProps) {
        super(props);
        if (props.showSpell) {
            this.state = {present: true, continueButtonShown: false};
        } else if (props.player > 0) {
            this.state = {present: true, continueButtonShown: true};
        } else {
            this.state = {present: props.showSpell, continueButtonShown: false};
        }
        this.handlePaintingClick = this.handlePaintingClick.bind(this);
        this.handleContinueButtonClick = this.handleContinueButtonClick.bind(this);
    }

    render() {
        let paintingClass = this.state.present ? "present" : "absent";
        // TODO: what's the best way to setTimeout?  Don't really want the button straight away
        let buttonClass = this.state.continueButtonShown ? "visible" : "invisible";
        let speechClass = (this.state.continueButtonShown || this.props.showSpell) ? "visible" : "invisible";
        let speech : JSX.Element;
        let cta : string;
        if (this.props.player == 0) {
            speech = (
            <div>
                <p>Hello Orson, hello Søren.  I'm Professor Albus Dumbledore.</p> 
                <p>I need your help to pair up some cards of some of the former students.  I'll help you if you help me!</p>
                <p>You first, please, Orson.</p>
            </div>
            );
            cta = "ORSON CLICK HERE";
        } else {
            speech = 
                (
                <div>
                    <p>Good work, Orson!</p>
                    <p>Now Søren, there is one more mess of cards.  Please pair them up for me.  Then I have something for you.</p>
                </div>
                );
            cta = "SØREN CLICK HERE";
        }

        
        if (this.props.showSpell) {
            speech = (
            <div>
                <p>Well done boys.  You've sorted it out nicely for me.</p>
                <p>Here's the spell you'll need to charm Professor McMagical.  You must wave your <strong>wands</strong> around and say:</p>
                <p><strong>INGRATUS</strong></p>
                <p>Good luck, boys!</p>
            </div>
            );
        }
        return (
        <div>
            <div id="painting" className={paintingClass} onClick={this.handlePaintingClick} />
            <div id="dumbledore-speech" className={speechClass} >{speech}</div>
            <div id="continue-button" className={buttonClass} onClick={this.handleContinueButtonClick} >{cta}</div>
        </div>);
    }

    handlePaintingClick() {
        if (!this.state.present) {
            this.setState({present: true, continueButtonShown: true});
        }
    }

    handleContinueButtonClick() {
        if (this.state.continueButtonShown) {
            this.props.handler.startGame(this.props.player);
        }
    }
}
