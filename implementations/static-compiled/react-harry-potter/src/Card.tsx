import * as React from 'react';

import ICardHandler from './ICardHandler'

let cardBack = require('./card_back.png');
let card0 = require('./card_0.png');
let card1 = require('./card_1.png');
let card2 = require('./card_2.png');
let card3 = require('./card_3.png');
let card4 = require('./card_4.png');
let card5 = require('./card_5.png');
let card6 = require('./card_6.png');
let card7 = require('./card_7.png');
let card8 = require('./card_8.png');

let cardImages: Array<any> = [card0, card1, card2, card3, card4, card5, card6, card7, card8]

interface ICardState {
    revealed: boolean;
}

interface ICardProps {
    handler: ICardHandler;
    pupilIndex: number;
}

export default class Card extends React.Component<ICardProps, ICardState> {
    constructor(props: ICardProps) {
        super(props);
        this.state = {revealed: false};
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    render() {
        let card = this.state.revealed ? cardImages[this.props.pupilIndex] : cardBack;
        return (<img className="card" src={card} onClick={this.handleClick} />);
    }

    handleClick() {
        if (!this.state.revealed) {
            this.setState({revealed: true});
            this.props.handler.revealCard(this);
        }
    }

    showBack() {
        this.setState({revealed: false});
    }

}
    