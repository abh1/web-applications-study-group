import * as React from 'react';
import Card from './Card';
import ICardHandler from './ICardHandler'
import IGameHandler from './IGameHandler'

interface ICardGameState {
    won: boolean;
}

interface ICardGameProps {
    handler: IGameHandler;
    player: number;
}

export default class CardGame extends React.Component<ICardGameProps, ICardGameState> implements ICardHandler {

    revealedCard1?: Card;
    revealedCard2?: Card;

    cardPupils: Array<number>;
    cardsRevealed: number;
    
    constructor(props: ICardGameProps) {
        super(props);
        this.state = {won: false};
        this.initCards();
    }

    initCards(): void {
        this.cardPupils = [];
        let cardIndex = 0;
        for (let pupilIndex: number = 0; pupilIndex < 9; pupilIndex++) {
            this.cardPupils.push(pupilIndex);
            this.cardPupils.push(pupilIndex);
        }
        for (let shuffle: number = 0; shuffle < 30; shuffle++) {
            let i1: number = Math.floor(Math.random() * this.cardPupils.length);
            let i2: number = Math.floor(Math.random() * this.cardPupils.length);
            let temp = this.cardPupils[i1];
            this.cardPupils[i1] = this.cardPupils[i2];
            this.cardPupils[i2] = temp;
        }
        this.cardsRevealed = 0;
    }

    render() {
        let gameRows: Array<any> = Array<any>();
        for (let rowIndex:number = 0; rowIndex < 3; rowIndex++) {
            let cardCols: Array<any> = Array<any>();

            for (let colIndex:number = 0; colIndex < 6; colIndex++) {
                let p = this.cardPupils[rowIndex * 6 + colIndex];
                cardCols.push(<td key={'r'+rowIndex+'c'+colIndex}><Card pupilIndex={p} handler={this} /></td>);
            }
            gameRows.push(<tr key={'r'+rowIndex}>{cardCols}</tr>);
        }

        return (
            <div>
                <table>
                    <tbody>
                        {gameRows}
                    </tbody>
                </table>
            </div>
        );
    }

    checkWon(): void {
        if (this.cardsRevealed == 6*3) {
            this.setState({won: true});
            this.props.handler.winGame(this.props.player);
        }
    }

    revealCard(card: Card): void {
        if (this.revealedCard1) {
            if (this.revealedCard2) {
                if (this.revealedCard1.props.pupilIndex != this.revealedCard2.props.pupilIndex) {
                    this.revealedCard1.showBack();
                    this.revealedCard2.showBack();
                    this.cardsRevealed -= 2;
                }
                this.revealedCard1 = card;
                this.revealedCard2 = undefined;
                this.cardsRevealed++;
        } else {
                this.revealedCard2 = card;
                this.cardsRevealed++;
                this.checkWon();
            }
        } else {
            this.revealedCard1 = card;
            this.cardsRevealed++;
        }
    }
}
