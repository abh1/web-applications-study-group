import * as React from 'react';
import DumbledoresOffice from './DumbledoresOffice';
import CardGame from './CardGame';
import IGameHandler from './IGameHandler';

interface IEscapeRoomState {
    // 0 = Dumbledore's office asking Orson, 1 = Orson game, 2 = office asking Soren, 3 = Soren game, 4 = spell revealed
    stage: number;
}

interface IEscapeRoomProps {
}

export default class HarryPotterEscapeRoom extends React.Component<IEscapeRoomProps, IEscapeRoomState> implements IGameHandler {
    constructor(props: IEscapeRoomProps) {
        super(props);
        this.state = {stage: 0};   
    }

    render() {
        switch(this.state.stage) {
            case 0:
                return (<DumbledoresOffice handler={this} showSpell={false} player={0} />);
            case 1:
                return (<CardGame handler={this} player={0} />);
            case 2:
                return (<DumbledoresOffice handler={this} showSpell={false} player={1} />);
            case 3:
                return (<CardGame handler={this} player={1} />);
            case 4:
                return (<DumbledoresOffice handler={this} showSpell={true} player={1} />);
        }
        
    }

    startGame(player: number): void {
        this.setState({stage: 1 + player * 2});
    }

    winGame(player: number): void {
        this.setState({stage: 2 + player * 2});
    }
}
