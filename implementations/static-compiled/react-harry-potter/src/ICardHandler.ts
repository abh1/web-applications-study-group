import Card from './Card';

export default interface ICardHandler {
    revealCard(card: Card): void;
}
