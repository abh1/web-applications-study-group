From: Adams, Jonathan 
Sent: 16 December 2016 15:31
Subject: RE: Web applications study group


I’ve taken my existing application and converted the JavaScript to TypeScript and the CSS to SCSS. In this way you get proper compile checking. The only changes I’ve made are to specify variable types and add casts appropriately so the compiler can check the code and the IDE can give auto-complete.

It’s set up to work from Visual Studio Code. Press Ctrl+Shift+B to build and run it. This compiles everything, launches a web server, launches a browser and constantly watches for changes so it can re-compile and refresh the browser.

This could potentially act as starting point for more complex development and to bring in frameworks like Angular 2 that use TypeScript.
