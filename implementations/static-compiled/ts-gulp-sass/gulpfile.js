var gulp = require('gulp');
var sass = require('gulp-sass');
var ts = require('gulp-typescript');
var browserSync = require('browser-sync'); // Reload the browser on file changes  

var tsProject = ts.createProject('tsconfig.json');

//gulp.task('clean', function () {
//  return del('scripts/**/*');
//});

// TypeScript compile
gulp.task('compile-ts', function () {
  return gulp
    .src('scripts/**/*.ts')
    .pipe(tsProject())
    .pipe(gulp.dest('scripts'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('compile-sass', function() {
    gulp.src('styles/**/*.scss')
     .pipe(sass.sync().on('error', sass.logError))
     .pipe(sass())
     .pipe(gulp.dest('styles'))
     .pipe(browserSync.reload({ stream: true }))
});

// Serve application
gulp.task('serve', ['compile-sass', 'compile-ts'], function() {  
  browserSync.init({
    server: {
      baseDir: './',
    },
  });
});


// Run all Gulp tasks and serve application
gulp.task('default', ['serve'], function() {  
  gulp.watch('styles/**/*.scss', ['compile-sass']);
  gulp.watch('scripts/**/*.ts', ['compile-ts']);
  gulp.watch(['./**/*.html', './**/*.js'], browserSync.reload);
});
