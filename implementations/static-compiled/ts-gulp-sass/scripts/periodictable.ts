function initializeTable(table)
{
    var rows = 4;
    var cols = 18;
    for (var row = 1; row <= rows; row++) {
        var tr = document.createElement('tr');
        table.appendChild(tr);
        for (let col = 1; col <= cols; col++)
        {
            var td = document.createElement('td');
            tr.appendChild(td);
        }
    }
}

var elements = [{symbol: "H", number: 1, row: 1, col: 1, class:"OtherNonMetals"},
    {symbol: "He", number: 2, row: 1, col: 18, class: "NobelGases"},
    {symbol: "li", number: 3, row: 2, col: 1, class: "AlkaliMetals"},
    {symbol: "Be", number: 4, row: 2, col: 2, class: "AlkalineEarthMetals"},
    {symbol: "B", number: 5, row: 2, col: 13, class: "Metalloids"},
    {symbol: "C", number: 6, row: 2, col: 14, class: "OtherNonMetals"},
    {symbol: "N", number: 7, row: 2, col: 15, class: "OtherNonMetals"},
    {symbol: "O", number: 8, row: 2, col: 16, class: "OtherNonMetals"},
    {symbol: "F", number: 9, row: 2, col: 17, class: "Halogens"},
    {symbol: "Ne", number: 10, row: 2, col: 18, class: "NobelGases"},
    {symbol: "Na", number: 11, row: 3, col: 1, class: "AlkaliMetals"},
    {symbol: "Mg", number: 12, row: 3, col: 2, class: "AlkalineEarthMetals"},
    {symbol: "Al", number: 13, row: 3, col: 13, class: "PostTransitionMetals"},
    {symbol: "Si", number: 14, row: 3, col: 14, class: "Metalloids"},
    {symbol: "P", number: 15, row: 3, col: 15, class: "OtherNonMetals"},
    {symbol: "S", number: 16, row: 3, col: 16, class: "OtherNonMetals"},
    {symbol: "Cl", number: 17, row: 3, col: 17, class: "Halogens"},
    {symbol: "Ar", number: 18, row: 3, col: 18, class: "NobelGases"},
    {symbol: "K", number: 19, row: 4, col: 1, class: "AlkaliMetals"},
    {symbol: "Ca", number: 20, row: 4, col: 2, class: "AlkalineEarthMetals"},
    {symbol: "Sc", number: 21, row: 4, col: 3, class: "Actinoids"},
    {symbol: "Ti", number: 22, row: 4, col: 4, class: "Actinoids"},
    {symbol: "V", number: 23, row: 4, col: 5, class: "Actinoids"},
    {symbol: "Cr", number: 24, row: 4, col: 6, class: "Actinoids"},
    {symbol: "Mn", number: 25, row: 4, col: 7, class: "Actinoids"},
    {symbol: "Fe", number: 26, row: 4, col: 8, class: "Actinoids"},
    {symbol: "Co", number: 27, row: 4, col: 9, class: "Actinoids"},
    {symbol: "Ni", number: 28, row: 4, col: 10, class: "Actinoids"},
    {symbol: "Cu", number: 29, row: 4, col: 11, class: "Actinoids"},
    {symbol: "Zn", number: 30, row: 4, col: 12, class: "Actinoids"},
    {symbol: "Ga", number: 31, row: 4, col: 13, class: "PostTransitionMetals"},
    {symbol: "Ge", number: 32, row: 4, col: 14, class: "Metalloids"},
    {symbol: "As", number: 33, row: 4, col: 15, class: "Metalloids"},
    {symbol: "Se", number: 34, row: 4, col: 16, class: "OtherNonMetals"},
    {symbol: "Br", number: 35, row: 4, col: 17, class: "Halogens"},
    {symbol: "Kr", number: 36, row: 4, col: 18, class: "NobelGases"}
    ]

function populateTable(table : HTMLTableElement, template : HTMLTemplateElement)
{
    for (let i=0; i< elements.length; ++i)
    {
        let element = elements[i];
        let tableCell = table.rows[element.row - 1].cells[element.col - 1];
        let elementContent : HTMLElement = <HTMLElement>document.importNode(template.content, true);
        elementContent.querySelector(".header").textContent = element.number.toLocaleString();
        elementContent.querySelector(".symbol").textContent = element.symbol;
        tableCell.classList.add(element.class);
        tableCell.appendChild(elementContent);
    }
}