import * as React from 'react';

export class App extends React.Component<any, any> {
  render() {

    return (
     <div style={{textAlign: 'center'}}>
      <MyHelloer whatToSayHelloTo="World" />
     </div>);
  }
}

interface IMyHelloerProps {
  whatToSayHelloTo: string;
}

interface IMyHelloerState {
}

class MyHelloer extends React.Component<IMyHelloerProps, IMyHelloerState> {

  constructor(props: IMyHelloerProps) {
    super(props);
  }

  render() {
    return <h1>Hello { this.props.whatToSayHelloTo } </h1>;
  }
}