WebAssembly (WASM) POC
======================

Install basic tools
-------------------

Started from here:

http://webassembly.org/getting-started/developers-guide/

Installed cmake, already had git and python.


Install Emscripten
------------------

```
git clone https://github.com/juj/emsdk.git
cd emsdk
emsdk install latest
emsdk activate latest
```

Set environment
---------------
```
# in emsdk folder
emsdk_env --build=Release
# or for Visual Studio 2017:
emsdk_env --build=Release --vs2017
```

Compile
-------
```
mkdir output
# Use the Emscripten compiler emcc http://kripken.github.io/emscripten-site/docs/tools_reference/emcc.html#emccdoc
emcc hello.c -s WASM=1 -o output\hello.html
```

Run
---
```
emrun --no_browser --port 8080 output
```

Notes
-----

There's a lot of guff in the generated hello.js.

It seems to provide a lot of helpers to enable hello.wasm to interact with WebGL, audio, filesystems and JavaScript debugging.  For a more minimal approach, see: http://webassembly.org/docs/js/#sample-api-usage
