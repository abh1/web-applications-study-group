import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { PeriodicTableComponent } from './periodic-table/periodic-table.component';
import { ElementDetailComponent } from './element-detail/element-detail.component';
import { BohrDiagramComponent } from './bohr-diagram/bohr-diagram.component';


@NgModule({
  declarations: [
    PeriodicTableComponent,
    ElementDetailComponent,
    BohrDiagramComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [PeriodicTableComponent]
})
export class AppModule { }
