import { Injectable } from '@angular/core';

import { Element } from './element';
import { ELEMENTS, ELEMENT_CATEGORY_DATA } from './mock-elements';

@Injectable()
export class ElementService {
  getElements(): Element[] {
    let elements : Element[] = ELEMENTS;
    for (let index : number = 0; index < elements.length; ++index)
      {
        let elementNumber : number = elements[index].number;
        if (ELEMENT_CATEGORY_DATA.actinoid.includes(elementNumber))
        {
          elements[index].class = "Actinoids";
        }
        if (ELEMENT_CATEGORY_DATA.alkali_metal.includes(elementNumber))
        {
          elements[index].class = "AlkaliMetals";
        }
        if (ELEMENT_CATEGORY_DATA.alkaline_earth_metal.includes(elementNumber))
        {
          elements[index].class = "AlkalineEarthMetals";
        }
        if (ELEMENT_CATEGORY_DATA.halogen.includes(elementNumber))
        {
          elements[index].class = "Halogens";
        }
        if (ELEMENT_CATEGORY_DATA.lanthanoid.includes(elementNumber))
        {
          elements[index].class = "";
        }
        if (ELEMENT_CATEGORY_DATA.metalloid.includes(elementNumber))
        {
          elements[index].class = "Metalloids";
        }
        if (ELEMENT_CATEGORY_DATA.noble_gas.includes(elementNumber))
        {
          elements[index].class = "NobelGases";
        }
        if (ELEMENT_CATEGORY_DATA.nonmetal.includes(elementNumber))
        {
          elements[index].class = "OtherNonMetals";
        }
        if (ELEMENT_CATEGORY_DATA.post_transition_metal.includes(elementNumber))
        {
          elements[index].class = "PostTransitionMetals";
        }
        if (ELEMENT_CATEGORY_DATA.transition_metal.includes(elementNumber))
        {
          elements[index].class = "PostTransitionMetals";
        }
      }
    return ELEMENTS;
  }

  getChemicalElementCategoryData()
  {
    
  }
}