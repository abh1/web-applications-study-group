import { Component, OnInit } from '@angular/core';
import { Element } from '../element';
import { ElementService } from '../element.service';


@Component({
  selector: 'app-root',
  templateUrl: './periodic-table.component.html',
  styleUrls: ['./periodic-table.component.css'],
  providers: [ElementService]
})
export class PeriodicTableComponent implements OnInit {
  title = 'Periodic Table';
  elements : Element[];
  selectedElement : Element;
  constructor(private elementService: ElementService) { }

  ngOnInit(): void
  {
    this.elements = this.elementService.getElements();
  }

  onSelect(element: Element): void {
    this.selectedElement = element;
  }

}
