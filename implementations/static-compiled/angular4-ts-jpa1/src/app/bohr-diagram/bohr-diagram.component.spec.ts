import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BohrDiagramComponent } from './bohr-diagram.component';

describe('BohrDiagramComponent', () => {
  let component: BohrDiagramComponent;
  let fixture: ComponentFixture<BohrDiagramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BohrDiagramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BohrDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
