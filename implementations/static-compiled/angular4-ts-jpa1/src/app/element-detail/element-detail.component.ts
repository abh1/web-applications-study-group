import { Component, Input } from '@angular/core';
import { Element } from '../element';

@Component({
  selector: '[element-detail]',
  templateUrl: './element-detail.component.html',
  styleUrls: ['./element-detail.component.css']
})
export class ElementDetailComponent {
    @Input()
    element: Element;

    classes() : string {
      return 'element ' + this.element.class;
    }
}