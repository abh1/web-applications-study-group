export class ElementName {
    en: string;
    de: string;
    fr: string;
    es: string;
    it: string;
}

export class IonizationPotentialEv {
    1?: number;
    2?: number;
    3?: number;
}

export class IonizationPotentialKjmol {
    1?: number;
    2?: number;
    3?: number;
}

export class ElectronConfiguration {
    predicted?: string;
    observed?: string;
}

export class Radii {
    atomic?: number;
    ionic?: number;
    covalent?: number;
    atomicPm?: number;
    oneplusion?: number;
    twoplusion?: number;
    threeplusion?: number;
    onedashion?: number;
    twodashion?: number;
}

export class Reactions {
    withAir?: string;
    withWater?: string;
    with6mhci?: string;
    with15mhno3?: string;
    with6MNaOH?: string;
}

export class Abundance {
    solarSystem?: number;
    earthCrust?: number;
}

export class Cost {
    pure?: number;
    bulk?: number;
}

export class Element {
    number: number;
    symbol: string;
    group: string;
    period: number;
    name: ElementName;
    mass: number;
    meltingPoint?: number;
    boilingPoint?: number;
    state?: string;
    density?: number;
    electronegativity?: number;
    ionizationPotentialEv: IonizationPotentialEv;
    ionizationPotentialKjmol: IonizationPotentialKjmol;
    electronAffinityEv?: string;
    electronAffinityKjmol?: string;
    commonOxidationStates?: string;
    commonIons?: string;
    electronConfiguration: ElectronConfiguration;
    radii: Radii;
    volume?: number;
    crystalStructure?: string;
    casRegistryNo?: string;
    electricalConductivity?: number;
    specificHeat?: number;
    heatOfFusion?: number;
    heatOfVaporization?: number;
    thermalConductivity?: number;
    densityInEarthCrust?: number;
    densityInSeawater?: number;
    percentHumanBody?: number;
    notes?: string;
    source?: string;
    colour?: string;
    characteristics?: string;
    structure?: string;
    hardness?: number;
    uses?: string;
    reactions?: Reactions;
    hydrides?: string;
    oxides?: string;
    chlorides?: string;
    polarizability?: string;
    heatAtomization?: number;
    yearDiscovered?: number;
    abundance?: Abundance;
    cost?: Cost;
    toxic?: string;
    carcinogenic?: string;
    chemicalElementCategory?: string;
    class?: string;
}

