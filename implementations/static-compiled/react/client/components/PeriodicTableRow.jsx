import React from 'react';
import ChemicalElement from './ChemicalElement.jsx';
import { chemicalElementData } from '../periodic-table-data.js';

export default class PeriodicTableRow extends React.Component {

    periodElements() {
        let languageCode = this.props.languageCode;

        let rowLength = this.props.rowLength;
        let leftmostAtomicNumber = this.props.left;
        let leftCount = this.props.leftCount;
        let rightmostAtomicNumber = this.props.right;
        let rightCount = this.props.rightCount;

        let ret = new Array(rowLength);
        for (let i = 0; i < leftCount; i++) {
            let atomicNumber = leftmostAtomicNumber + i - 1;
            ret[i] = <ChemicalElement key={i} info={chemicalElementData[atomicNumber]} languageCode={languageCode} detailDisplayer={this.props.detailDisplayer}/>
        }
        let fillTo = rowLength;
        if (rightmostAtomicNumber > leftmostAtomicNumber) {
            for (let i = 0; i < rightCount; i++) {
                let atomicNumber = rightmostAtomicNumber - i - 1;
                let index = rowLength - 1 - i;
                ret[index] = <ChemicalElement key={index} info={chemicalElementData[atomicNumber]} languageCode={languageCode} detailDisplayer={this.props.detailDisplayer}/>
            }
            fillTo = rowLength - rightCount;
        }
        for (let i = leftCount; i < fillTo; i++) {
            ret[i] = <td key={i} />
        }
        return ret;
    }

    render() {
        return (
            <tr className="period">
                <th>{this.props.period}</th>
                {this.periodElements()}
            </tr>
        );
    }
}
