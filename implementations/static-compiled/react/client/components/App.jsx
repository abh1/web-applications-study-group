import React from 'react';
import PeriodicTable from './PeriodicTable.jsx';

export default class App extends React.Component {
  render() {
    return (
     <PeriodicTable />
    );
  }
}
