import React from 'react';
import { chemicalElementData, chemicalElementCategoryData } from '../periodic-table-data.js';

export default class ChemicalElement extends React.Component {

    constructor(props) {
        super();
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    getElementTypeClassName(atomicNumber) {
        for (let t in chemicalElementCategoryData) {
            let numbers = chemicalElementCategoryData[t];
            if (numbers.indexOf(atomicNumber) >= 0) {
                return t;
            }
        }

        return 'unknown';
    }

    render() {
        if (this.props.info) {
            let atomicNumber = this.props.info["number"];
            return (
                <td className={"element " + this.getElementTypeClassName(atomicNumber)} onClick={this.handleClick}>
                    <div className="atomic-number">{atomicNumber}</div>
                    <div className="symbol">{this.props.info["symbol"]}</div>
                    <div className="name">{this.props.info["name"][this.props.languageCode]}</div>
                </td>
            );
        } else {
            return <td>Bad!</td>
        }
    }

    handleClick() {
        this.props.detailDisplayer.displayElementDetails(this.props.info);
    }
}
