export var chemicalElementData = [{
	"number": 1,
	"symbol": "H",
	"group": "1",
	"period": 1,
	"name": {
		"en": "hydrogen",
		"de": "Wasserstoff",
		"fr": "hydrogène",
		"es": "hidrógeno",
		"it": "idrogeno"
	},
	"mass": 1.008,
	"meltingPoint": -259.1,
	"boilingPoint": -252.9,
	"state": "g",
	"density": 0.0000699,
	"electronegativity": 2.2,
	"ionizationPotentialEv": {
		"1": 13.5984
	},
	"ionizationPotentialKjmol": {
		"1": 1312
	},
	"electronAffinityEv": "0.75",
	"electronAffinityKjmol": "72",
	"commonOxidationStates": "±1",
	"commonIons": "±1",
	"electronConfiguration": {
		"predicted": "1s1",
		"observed": "1s1"
	},
	"radii": {
		"atomic": 0.79,
		"ionic": 1.54,
		"covalent": 0.32,
		"atomicPm": 37.1
	},
	"volume": 14.4,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "1333-74-0",
	"specificHeat": 14.304,
	"heatOfFusion": 0.05868,
	"heatOfVaporization": 0.44936,
	"thermalConductivity": 0.1815,
	"densityInEarthCrust": 1400,
	"densityInSeawater": 108000,
	"percentHumanBody": 10,
	"source": "Water, methane",
	"colour": "colorless",
	"characteristics": "diatomic,dens<air at r.t.",
	"structure": "hcp: hexagonal close pkd",
	"uses": "rocket fuel",
	"reactions": {
		"withAir": "vigorous, =>H2O",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "H2",
	"oxides": "H2O",
	"chlorides": "HCl",
	"polarizability": "0.7",
	"heatAtomization": 218,
	"yearDiscovered": 1766,
	"abundance": {
		"solarSystem": 10.4456042032736,
		"earthCrust": 3.1
	},
	"cost": {
		"pure": 12
	}
}, {
	"number": 2,
	"symbol": "He",
	"group": "18",
	"period": 1,
	"name": {
		"en": "helium",
		"de": "Helium",
		"fr": "hélium",
		"es": "helio",
		"it": "elio"
	},
	"mass": 4.003,
	"meltingPoint": -272.2,
	"boilingPoint": -268.9,
	"state": "g",
	"density": 0.000179,
	"ionizationPotentialEv": {
		"1": 24.5874,
		"2": 54.416
	},
	"ionizationPotentialKjmol": {
		"1": 2372,
		"2": 5250
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "0",
	"electronConfiguration": {
		"predicted": "1s2",
		"observed": "1s2"
	},
	"radii": {
		"atomic": 0.49,
		"covalent": 0.93,
		"atomicPm": 31
	},
	"volume": 19.5,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-59-7",
	"specificHeat": 5.193,
	"heatOfVaporization": 0.0845,
	"thermalConductivity": 0.152,
	"densityInEarthCrust": 0.008,
	"densityInSeawater": 0.0000000000000007,
	"source": "Natural gas, air",
	"colour": "colorless",
	"characteristics": "Inert, dens<air at r.t.",
	"uses": "cryogenics, blimps",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "none",
	"chlorides": "none",
	"polarizability": "0.198",
	"heatAtomization": 0,
	"yearDiscovered": 1895,
	"abundance": {
		"solarSystem": 9.4345689040342,
		"earthCrust": -2.1
	},
	"cost": {
		"pure": 5.2
	},
	"toxic": "no",
	"carcinogenic": "no"
}, {
	"number": 3,
	"symbol": "Li",
	"group": "1",
	"period": 2,
	"name": {
		"en": "lithium",
		"de": "Lithium",
		"fr": "lithium",
		"es": "litio",
		"it": "litio"
	},
	"mass": 6.968,
	"meltingPoint": 180.5,
	"boilingPoint": 1342,
	"state": "s",
	"density": 0.543,
	"electronegativity": 0.98,
	"ionizationPotentialEv": {
		"1": 5.3917,
		"2": 76.638,
		"3": 122.451
	},
	"ionizationPotentialKjmol": {
		"1": 520,
		"2": 7394,
		"3": 11815
	},
	"electronAffinityEv": "0.62",
	"electronAffinityKjmol": "60",
	"commonOxidationStates": "+1",
	"commonIons": "+1",
	"electronConfiguration": {
		"predicted": "[He] 2s1",
		"observed": "[He] 2s1"
	},
	"radii": {
		"atomic": 2.05,
		"ionic": 0.76,
		"covalent": 1.23,
		"atomicPm": 152,
		"1+ion": 90
	},
	"volume": 13.1,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7439-93-2",
	"electricalConductivity": 0.108,
	"specificHeat": 3.6,
	"heatOfFusion": 3,
	"heatOfVaporization": 145.92,
	"thermalConductivity": 84.7,
	"densityInEarthCrust": 20,
	"densityInSeawater": 0.18,
	"source": "Spodumene (silicate)",
	"colour": "silvery",
	"characteristics": "soft, lightest solid",
	"structure": "bcc: body-centered cubic",
	"hardness": 0.6,
	"uses": "batteries, lubricant",
	"reactions": {
		"withAir": "vigorous, =>Li2O",
		"withWater": "mild, =>H2, LiOH",
		"with6mhci": "vigorous, =>H2, LiCl",
		"with15mhno3": "vigorous, =>LiNO3",
		"with6MNaOH": "mild, =>H2, LiOH"
	},
	"hydrides": "LiH",
	"oxides": "Li2O",
	"chlorides": "LiCl",
	"polarizability": "24.3",
	"heatAtomization": 161,
	"yearDiscovered": 1817,
	"abundance": {
		"solarSystem": 1.75663610824585,
		"earthCrust": 1.3
	},
	"cost": {
		"pure": 27
	}
}, {
	"number": 4,
	"symbol": "Be",
	"group": "2",
	"period": 2,
	"name": {
		"en": "beryllium",
		"de": "Beryllium",
		"fr": "béryllium",
		"es": "berilio",
		"it": "berillio"
	},
	"mass": 9.012,
	"meltingPoint": 1278,
	"boilingPoint": 2970,
	"state": "s",
	"density": 1.85,
	"electronegativity": 1.57,
	"ionizationPotentialEv": {
		"1": 9.3226,
		"2": 18.211,
		"3": 153.893
	},
	"ionizationPotentialKjmol": {
		"1": 899,
		"2": 1757,
		"3": 14848
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[He] 2s2",
		"observed": "[He] 2s2"
	},
	"radii": {
		"atomic": 1.4,
		"ionic": 0.45,
		"covalent": 0.9,
		"atomicPm": 112,
		"2+ion": 59
	},
	"volume": 5,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-41-7",
	"electricalConductivity": 0.313,
	"specificHeat": 1.82,
	"heatOfFusion": 12.2,
	"heatOfVaporization": 292.4,
	"thermalConductivity": 200,
	"densityInEarthCrust": 2.8,
	"densityInSeawater": 0.0000056,
	"source": "Beryl (silicate)",
	"colour": "steel gray",
	"characteristics": "toxic",
	"structure": "hcp: hexagonal close pkd",
	"uses": "Cu alloys, X-ray windows",
	"reactions": {
		"withAir": "vigorous, w/ht=>BeO,Be3N2",
		"withWater": "none",
		"with6mhci": "mild, =>H2",
		"with15mhno3": "none",
		"with6MNaOH": "mild, =>H2, [Be(OH)4](2-)"
	},
	"hydrides": "BeH2",
	"oxides": "BeO",
	"chlorides": "BeCl2",
	"polarizability": "5.6",
	"heatAtomization": 324,
	"yearDiscovered": 1797,
	"abundance": {
		"solarSystem": -0.136677139879544,
		"earthCrust": 0.4
	},
	"cost": {
		"pure": 530,
		"bulk": 59
	},
	"toxic": "yes",
	"carcinogenic": "yes"
}, {
	"number": 5,
	"symbol": "B",
	"group": "13",
	"period": 2,
	"name": {
		"en": "boron",
		"de": "Bor",
		"fr": "bore",
		"es": "boro",
		"it": "boro"
	},
	"mass": 10.81,
	"meltingPoint": 2079,
	"boilingPoint": 2550,
	"state": "s",
	"density": 2.34,
	"electronegativity": 2.04,
	"ionizationPotentialEv": {
		"1": 8.298,
		"2": 25.154,
		"3": 37.93
	},
	"ionizationPotentialKjmol": {
		"1": 801,
		"2": 2427,
		"3": 3660
	},
	"electronAffinityEv": "0.28",
	"electronAffinityKjmol": "27",
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[He] 2s2 2p1",
		"observed": "[He] 2s2 2p1"
	},
	"radii": {
		"atomic": 1.17,
		"ionic": 0.23,
		"covalent": 0.82,
		"atomicPm": 85,
		"3+ion": 41
	},
	"volume": 4.6,
	"crystalStructure": "rhombohedral",
	"casRegistryNo": "7440-42-8",
	"electricalConductivity": 0.000000000001,
	"specificHeat": 1.02,
	"heatOfFusion": 50.2,
	"heatOfVaporization": 489.7,
	"thermalConductivity": 27,
	"densityInEarthCrust": 10,
	"densityInSeawater": 4.44,
	"percentHumanBody": 0.00007,
	"notes": "sublimes",
	"source": "Na and Ca borates (misc)",
	"colour": "black",
	"characteristics": "B12 icosahedra; 3 forms",
	"structure": "special: B12 icosahedra",
	"hardness": 9.5,
	"uses": "borax, glass making(B2O3)",
	"reactions": {
		"withAir": "mild, w/ht =>B2O3",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "B2H6 and many BxHy",
	"oxides": "B2O3",
	"chlorides": "BCl3 and many BxCly",
	"polarizability": "3",
	"heatAtomization": 573,
	"yearDiscovered": 1808,
	"abundance": {
		"solarSystem": 1.32633586092875,
		"earthCrust": 1
	},
	"cost": {
		"pure": 250
	}
}, {
	"number": 6,
	"symbol": "C",
	"group": "14",
	"period": 2,
	"name": {
		"en": "carbon",
		"de": "Kohlenlstoff",
		"fr": "carbone",
		"es": "carbono",
		"it": "carbonio"
	},
	"mass": 12.01,
	"meltingPoint": 3367,
	"boilingPoint": 4827,
	"state": "s",
	"density": 2.25,
	"electronegativity": 2.55,
	"ionizationPotentialEv": {
		"1": 11.2603,
		"2": 24.383,
		"3": 47.887
	},
	"ionizationPotentialKjmol": {
		"1": 1086,
		"2": 2353,
		"3": 4620
	},
	"electronAffinityEv": "1.26",
	"electronAffinityKjmol": "122",
	"commonOxidationStates": "±4",
	"commonIons": "-4",
	"electronConfiguration": {
		"predicted": "[He] 2s2 2p2",
		"observed": "[He] 2s2 2p2"
	},
	"radii": {
		"atomic": 0.91,
		"ionic": 0.16,
		"covalent": 0.77,
		"atomicPm": 77.2
	},
	"volume": 4.58,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-44-0",
	"electricalConductivity": 0.00061,
	"specificHeat": 0.71,
	"heatOfVaporization": 355.8,
	"thermalConductivity": 129,
	"densityInEarthCrust": 200,
	"densityInSeawater": 28,
	"percentHumanBody": 23,
	"notes": "graphite",
	"source": "Coal,Petroleum,Natural gas",
	"colour": "black",
	"characteristics": "6 forms: graphite,diamond",
	"structure": "hexagonal layers",
	"hardness": 0.8,
	"uses": "fuel(coal), lubricant",
	"reactions": {
		"withAir": "vigorous, =>CO2",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild,w/ht=>C6(COOH)6",
		"with6MNaOH": "none"
	},
	"hydrides": "CH4 and many CxHy",
	"oxides": "CO CO2",
	"chlorides": "CCl4",
	"polarizability": "1.8",
	"heatAtomization": 717,
	"abundance": {
		"solarSystem": 7.00432137378264,
		"earthCrust": 2.3
	},
	"cost": {
		"pure": 2.4
	},
	"carcinogenic": "no"
}, {
	"number": 7,
	"symbol": "N",
	"group": "15",
	"period": 2,
	"name": {
		"en": "nitrogen",
		"de": "Stickstoff",
		"fr": "azote",
		"es": "nitrógeno",
		"it": "azoto"
	},
	"mass": 14.01,
	"meltingPoint": -209.9,
	"boilingPoint": -195.8,
	"state": "g",
	"density": 0.00125,
	"electronegativity": 3.04,
	"ionizationPotentialEv": {
		"1": 14.5341,
		"2": 29.601,
		"3": 47.448
	},
	"ionizationPotentialKjmol": {
		"1": 1402,
		"2": 2856,
		"3": 4578
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "-3",
	"commonIons": "-3",
	"electronConfiguration": {
		"predicted": "[He] 2s2 2p3",
		"observed": "[He] 2s2 2p3"
	},
	"radii": {
		"atomic": 0.75,
		"ionic": 1.71,
		"covalent": 0.75,
		"atomicPm": 70,
		"3+ion": 30
	},
	"volume": 17.3,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "77727-37-9",
	"specificHeat": 1.04,
	"heatOfFusion": 0.3604,
	"heatOfVaporization": 2.7928,
	"thermalConductivity": 0.02598,
	"densityInEarthCrust": 19,
	"densityInSeawater": 50,
	"percentHumanBody": 2.6,
	"source": "Air",
	"colour": "colorless",
	"characteristics": "diatomic(N2)",
	"uses": "fertilizer, cryogenics",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "NH3 N2H4 HN3",
	"oxides": "N2O NO NO2 N2O5",
	"chlorides": "NCl3",
	"polarizability": "1.1",
	"heatAtomization": 473,
	"yearDiscovered": 1772,
	"abundance": {
		"solarSystem": 6.49554433754645,
		"earthCrust": 1.3
	},
	"cost": {
		"pure": 0.4
	},
	"toxic": "no",
	"carcinogenic": "no"
}, {
	"number": 8,
	"symbol": "O",
	"group": "16",
	"period": 2,
	"name": {
		"en": "oxygen",
		"de": "Sauerstoff",
		"fr": "oxygène",
		"es": "oxígeno",
		"it": "ossigeno"
	},
	"mass": 16.00,
	"meltingPoint": -218.4,
	"boilingPoint": -183,
	"state": "g",
	"density": 0.00143,
	"electronegativity": 3.44,
	"ionizationPotentialEv": {
		"1": 13.6181,
		"2": 35.117,
		"3": 54.934
	},
	"ionizationPotentialKjmol": {
		"1": 1314,
		"2": 3388,
		"3": 5300
	},
	"electronAffinityEv": "1.46",
	"electronAffinityKjmol": "141",
	"commonOxidationStates": "-2",
	"commonIons": "-2",
	"electronConfiguration": {
		"predicted": "[He] 2s2 2p4",
		"observed": "[He] 2s2 2p4"
	},
	"radii": {
		"atomic": 0.65,
		"ionic": 1.4,
		"covalent": 0.73,
		"2-ion": 126,
		"atomicPm": 73
	},
	"volume": 14,
	"crystalStructure": "cubic",
	"casRegistryNo": "7782-44-7",
	"specificHeat": 0.92,
	"heatOfFusion": 0.22259,
	"heatOfVaporization": 3.4099,
	"thermalConductivity": 0.02674,
	"densityInEarthCrust": 461000,
	"densityInSeawater": 857000,
	"percentHumanBody": 61,
	"source": "Air",
	"colour": "colorless",
	"characteristics": "diatomic,2 forms: O2, O3",
	"uses": "rocket fuel, steel mfr.",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "H2O",
	"oxides": "O2 O3",
	"chlorides": "Cl2O ClO2 Cl2O7",
	"polarizability": "0.793",
	"heatAtomization": 249,
	"yearDiscovered": 1774,
	"abundance": {
		"solarSystem": 7.37657695705651,
		"earthCrust": 5.7
	},
	"cost": {
		"pure": 0.3
	},
	"toxic": "no",
	"carcinogenic": "no"
}, {
	"number": 9,
	"symbol": "F",
	"group": "17",
	"period": 2,
	"name": {
		"en": "fluorine",
		"de": "Fluor",
		"fr": "fluor",
		"es": "flúor",
		"it": "fluoro"
	},
	"mass": 19.00,
	"meltingPoint": -219.8,
	"boilingPoint": -188.1,
	"state": "g",
	"density": 0.0017,
	"electronegativity": 3.98,
	"ionizationPotentialEv": {
		"1": 17.4228,
		"2": 34.97,
		"3": 62.707
	},
	"ionizationPotentialKjmol": {
		"1": 1681,
		"2": 3374,
		"3": 6050
	},
	"electronAffinityEv": "3.4",
	"electronAffinityKjmol": "328",
	"commonOxidationStates": "-1",
	"commonIons": "-1",
	"electronConfiguration": {
		"predicted": "[He] 2s2 2p5",
		"observed": "[He] 2s2 2p5"
	},
	"radii": {
		"atomic": 0.57,
		"ionic": 1.33,
		"covalent": 0.72,
		"1-ion": 119,
		"atomicPm": 72
	},
	"volume": 12.6,
	"crystalStructure": "cubic",
	"casRegistryNo": "7782-41-4",
	"specificHeat": 0.82,
	"heatOfFusion": 0.2552,
	"heatOfVaporization": 3.2698,
	"thermalConductivity": 0.0279,
	"densityInEarthCrust": 585,
	"densityInSeawater": 1.3,
	"percentHumanBody": 0.0033,
	"source": "Fluorite (misc)",
	"colour": "colorless",
	"characteristics": "diatomic(F2), corrosive",
	"structure": "layers of F2",
	"uses": "fluorocarbons, toothpaste",
	"reactions": {
		"withAir": "none",
		"withWater": "vigorous, =>HF, OF2",
		"with6mhci": "vigorous, =>HF, OF2, ClF3",
		"with6MNaOH": "vigorous, =>O2, NaF"
	},
	"hydrides": "HF",
	"oxides": "OF2",
	"chlorides": "ClF ClF3 ClF5",
	"polarizability": "0.634",
	"heatAtomization": 79,
	"yearDiscovered": 1886,
	"abundance": {
		"solarSystem": 2.92582757462474,
		"earthCrust": 2.8
	},
	"cost": {
		"pure": 190
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 10,
	"symbol": "Ne",
	"group": "18",
	"period": 2,
	"name": {
		"en": "neon",
		"de": "Neon",
		"fr": "néon",
		"es": "neón",
		"it": "neon"
	},
	"mass": 20.18,
	"meltingPoint": -248,
	"boilingPoint": -248.7,
	"state": "g",
	"density": 0.0009,
	"ionizationPotentialEv": {
		"1": 21.5645,
		"2": 40.962,
		"3": 63.45
	},
	"ionizationPotentialKjmol": {
		"1": 2081,
		"2": 3952,
		"3": 6122
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "0",
	"electronConfiguration": {
		"predicted": "[He] 2s2 2p6",
		"observed": "[He] 2s2 2p6"
	},
	"radii": {
		"atomic": 0.51,
		"covalent": 0.71,
		"atomicPm": 71
	},
	"volume": 17.3,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-01-9",
	"specificHeat": 0.904,
	"heatOfFusion": 0.3317,
	"heatOfVaporization": 1.7326,
	"thermalConductivity": 0.0493,
	"densityInEarthCrust": 0.005,
	"densityInSeawater": 0.00012,
	"source": "Air",
	"colour": "colorless",
	"characteristics": "Inert, dens<air at r.t.",
	"structure": "fcc: face-centered cubic",
	"uses": "Neon signs",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "none",
	"chlorides": "none",
	"polarizability": "0.396",
	"heatAtomization": 0,
	"yearDiscovered": 1898,
	"abundance": {
		"solarSystem": 6.53655844257153,
		"earthCrust": -2.3
	},
	"cost": {
		"pure": 33
	},
	"toxic": "no",
	"carcinogenic": "no"
}, {
	"number": 11,
	"symbol": "Na",
	"group": "1",
	"period": 3,
	"name": {
		"en": "sodium",
		"de": "Natrium",
		"fr": "sodium",
		"es": "sodio",
		"it": "sodio"
	},
	"mass": 22.99,
	"meltingPoint": 97.8,
	"boilingPoint": 883,
	"state": "s",
	"density": 0.971,
	"electronegativity": 0.93,
	"ionizationPotentialEv": {
		"1": 5.1391,
		"2": 47.286,
		"3": 71.641
	},
	"ionizationPotentialKjmol": {
		"1": 496,
		"2": 4562,
		"3": 6912
	},
	"electronAffinityEv": "0.55",
	"electronAffinityKjmol": "53",
	"commonOxidationStates": "+1",
	"commonIons": "+1",
	"electronConfiguration": {
		"predicted": "[Ne] 3s1",
		"observed": "[Ne] 3s1"
	},
	"radii": {
		"atomic": 2.23,
		"ionic": 1.02,
		"covalent": 1.54,
		"atomicPm": 186,
		"1+ion": 116
	},
	"volume": 23.7,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-23-5",
	"electricalConductivity": 0.21,
	"specificHeat": 1.23,
	"heatOfFusion": 2.598,
	"heatOfVaporization": 96.96,
	"thermalConductivity": 141,
	"densityInEarthCrust": 0.000236,
	"densityInSeawater": 10800,
	"percentHumanBody": 0.14,
	"source": "Halite (misc)",
	"colour": "silvery",
	"characteristics": "soft",
	"structure": "bcc: body-centered cubic",
	"hardness": 0.4,
	"uses": "lamps, table salt",
	"reactions": {
		"withAir": "vigorous, =>Na2O2",
		"withWater": "vigorous, =>H2, NaOH",
		"with6mhci": "vigorous, =>H2, NaCl",
		"with15mhno3": "vigorous, =>NaNO3, NOx",
		"with6MNaOH": "vigorous, =>H2, NaOH"
	},
	"hydrides": "NaH",
	"oxides": "Na2O",
	"chlorides": "NaCl",
	"polarizability": "23.6",
	"heatAtomization": 109,
	"yearDiscovered": 1807,
	"abundance": {
		"solarSystem": 4.75891189239797,
		"earthCrust": 4.4
	},
	"cost": {
		"pure": 7
	}
}, {
	"number": 12,
	"symbol": "Mg",
	"group": "2",
	"period": 3,
	"name": {
		"en": "magnesium",
		"de": "Magnesium",
		"fr": "magnésium",
		"es": "magnesio",
		"it": "magnesio"
	},
	"mass": 24.31,
	"meltingPoint": 649,
	"boilingPoint": 1090,
	"state": "s",
	"density": 1.74,
	"electronegativity": 1.31,
	"ionizationPotentialEv": {
		"1": 7.6462,
		"2": 15.035,
		"3": 80.143
	},
	"ionizationPotentialKjmol": {
		"1": 738,
		"2": 1451,
		"3": 7733
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[Ne] 3s2",
		"observed": "[Ne] 3s2"
	},
	"radii": {
		"atomic": 1.72,
		"ionic": 0.72,
		"covalent": 1.36,
		"atomicPm": 160,
		"2+ion": 86
	},
	"volume": 13.97,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7439-95-4",
	"electricalConductivity": 0.226,
	"specificHeat": 1.02,
	"heatOfFusion": 8.954,
	"heatOfVaporization": 127.4,
	"thermalConductivity": 156,
	"densityInEarthCrust": 23300,
	"densityInSeawater": 1290,
	"percentHumanBody": 0.027,
	"source": "Seawater, magnesite",
	"colour": "silvery",
	"characteristics": "burns brightly",
	"structure": "hcp: hexagonal close pkd",
	"hardness": 2,
	"uses": "alloys, flash bulbs",
	"reactions": {
		"withAir": "vigorous,w/ht =>MgO,Mg3N2",
		"withWater": "mild, w/ht =>Mg(OH)2, H2",
		"with6mhci": "mild, =>H2, MgCl2",
		"with15mhno3": "vigorous, =>NOx, Mg(NO3)2",
		"with6MNaOH": "none"
	},
	"hydrides": "MgH2",
	"oxides": "MgO",
	"chlorides": "MgCl2",
	"polarizability": "10.6",
	"heatAtomization": 148,
	"yearDiscovered": 1808,
	"abundance": {
		"solarSystem": 6.03100428136354,
		"earthCrust": 4.4
	},
	"cost": {
		"pure": 3.7,
		"bulk": 0.32
	}
}, {
	"number": 13,
	"symbol": "Al",
	"group": "13",
	"period": 3,
	"name": {
		"en": "aluminum",
		"de": "Aluminium",
		"fr": "aluminium",
		"es": "aluminio",
		"it": "alluminio"
	},
	"mass": 26.98,
	"meltingPoint": 660,
	"boilingPoint": 2467,
	"state": "s",
	"density": 2.7,
	"electronegativity": 1.61,
	"ionizationPotentialEv": {
		"1": 5.9858,
		"2": 18.828,
		"3": 28.447
	},
	"ionizationPotentialKjmol": {
		"1": 578,
		"2": 1817,
		"3": 2745
	},
	"electronAffinityEv": "0.44",
	"electronAffinityKjmol": "42",
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Ne] 3s2 3p1",
		"observed": "[Ne] 3s2 3p1"
	},
	"radii": {
		"atomic": 1.82,
		"ionic": 0.54,
		"covalent": 1.18,
		"atomicPm": 143,
		"3+ion": 67.5
	},
	"volume": 10,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7429-90-5",
	"electricalConductivity": 0.377,
	"specificHeat": 0.9,
	"heatOfFusion": 10.79,
	"heatOfVaporization": 293.4,
	"thermalConductivity": 237,
	"densityInEarthCrust": 82300,
	"densityInSeawater": 0.002,
	"percentHumanBody": 0.00009,
	"source": "Bauxite (oxide)",
	"colour": "silvery",
	"characteristics": "passivated: surface oxide",
	"structure": "fcc: face-centered cubic",
	"hardness": 2.8,
	"uses": "Al cans & foil,reflectors",
	"reactions": {
		"withAir": "mild, w/ht =>Al2O3",
		"withWater": "none",
		"with6mhci": "mild, =>H2, AlCl3",
		"with15mhno3": "passivated",
		"with6MNaOH": "mild, =>H2, [Al(OH)4]-"
	},
	"hydrides": "AlH3",
	"oxides": "Al2O3",
	"chlorides": "AlCl3Al2Cl6",
	"polarizability": "8.3",
	"heatAtomization": 326,
	"yearDiscovered": 1825,
	"abundance": {
		"solarSystem": 4.92890769024395,
		"earthCrust": 4.9
	},
	"cost": {
		"pure": 1.8,
		"bulk": 0.12
	},
	"carcinogenic": "no"
}, {
	"number": 14,
	"symbol": "Si",
	"group": "14",
	"period": 3,
	"name": {
		"en": "silicon",
		"de": "Silizium",
		"fr": "silicium",
		"es": "silicio",
		"it": "silicio"
	},
	"mass": 28.09,
	"meltingPoint": 1410,
	"boilingPoint": 2355,
	"state": "s",
	"density": 2.33,
	"electronegativity": 1.9,
	"ionizationPotentialEv": {
		"1": 8.1517,
		"2": 16.345,
		"3": 33.492
	},
	"ionizationPotentialKjmol": {
		"1": 787,
		"2": 1577,
		"3": 3231
	},
	"electronAffinityEv": "1.39",
	"electronAffinityKjmol": "134",
	"commonOxidationStates": "±4",
	"commonIons": "-4",
	"electronConfiguration": {
		"predicted": "[Ne] 3s2 3p2",
		"observed": "[Ne] 3s2 3p2"
	},
	"radii": {
		"atomic": 1.46,
		"ionic": 0.26,
		"covalent": 1.11,
		"atomicPm": 117.6
	},
	"volume": 12.1,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-21-3",
	"electricalConductivity": 0.00000000000252,
	"specificHeat": 0.71,
	"heatOfFusion": 50.55,
	"heatOfVaporization": 384.22,
	"thermalConductivity": 148,
	"densityInEarthCrust": 282000,
	"densityInSeawater": 2.2,
	"percentHumanBody": 0.026,
	"source": "Quartz (oxide)",
	"colour": "gray-black",
	"characteristics": "semimetal",
	"structure": "diamond ",
	"hardness": 7,
	"uses": "comp chip,lubricant,glass",
	"reactions": {
		"withAir": "mild, =>SiO2",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "mild, =>silicates"
	},
	"hydrides": "SiH4 Si2H6 + more",
	"oxides": "SiO2",
	"chlorides": "SiCl4 Si2Cl6 + more",
	"polarizability": "5.4",
	"heatAtomization": 452,
	"yearDiscovered": 1824,
	"abundance": {
		"solarSystem": 6,
		"earthCrust": 5.5
	},
	"cost": {
		"pure": 5.4,
		"bulk": 0.14
	},
	"carcinogenic": "no"
}, {
	"number": 15,
	"symbol": "P",
	"group": "15",
	"period": 3,
	"name": {
		"en": "phosphorus",
		"de": "Phosphor",
		"fr": "phosphore",
		"es": "fósforo",
		"it": "fosforo"
	},
	"mass": 30.97,
	"meltingPoint": 44.1,
	"boilingPoint": 280,
	"state": "s",
	"density": 1.82,
	"electronegativity": 2.19,
	"ionizationPotentialEv": {
		"1": 10.4867,
		"2": 19.725,
		"3": 30.18
	},
	"ionizationPotentialKjmol": {
		"1": 1012,
		"2": 1903,
		"3": 2912
	},
	"electronAffinityEv": "0.75",
	"electronAffinityKjmol": "72",
	"commonOxidationStates": "-3",
	"commonIons": "-3",
	"electronConfiguration": {
		"predicted": "[Ne] 3s2 3p3",
		"observed": "[Ne] 3s2 3p3"
	},
	"radii": {
		"atomic": 1.23,
		"ionic": 0.17,
		"covalent": 1.06,
		"atomicPm": 110,
		"3+ion": 58
	},
	"volume": 17,
	"crystalStructure": "monoclinic",
	"casRegistryNo": "7723-14-0",
	"electricalConductivity": 0.00000000000000001,
	"specificHeat": 0.77,
	"heatOfFusion": 0.657,
	"heatOfVaporization": 12.129,
	"thermalConductivity": 0.235,
	"densityInEarthCrust": 1050,
	"densityInSeawater": 0.06,
	"percentHumanBody": 1.1,
	"notes": "white",
	"source": "Apatite (phosphate)",
	"colour": "white-yellow",
	"characteristics": "(P4) pyrophoric, 15 forms",
	"structure": "special: P4 tetrahedra",
	"hardness": 0.5,
	"uses": "fertilizer, detergents",
	"reactions": {
		"withAir": "vigorous,=>P4O10, ignites",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>NOx",
		"with6MNaOH": "mild, =>PH3, may ignite"
	},
	"hydrides": "PH3 P2H4 + more",
	"oxides": "P4O10 P4O6",
	"chlorides": "PCl3 PCl5 P2Cl4",
	"polarizability": "3.6",
	"heatAtomization": 315,
	"yearDiscovered": 1669,
	"abundance": {
		"solarSystem": 4.01703333929878,
		"earthCrust": 3
	},
	"cost": {
		"pure": 4
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 16,
	"symbol": "S",
	"group": "16",
	"period": 3,
	"name": {
		"en": "sulfur",
		"de": "Schwefel",
		"fr": "soufre",
		"es": "azufre",
		"it": "zolfo"
	},
	"mass": 32.07,
	"meltingPoint": 112.8,
	"boilingPoint": 444.7,
	"state": "s",
	"density": 2.07,
	"electronegativity": 2.58,
	"ionizationPotentialEv": {
		"1": 10.36,
		"2": 23.33,
		"3": 34.83
	},
	"ionizationPotentialKjmol": {
		"1": 1000,
		"2": 2251,
		"3": 3361
	},
	"electronAffinityEv": "2.08",
	"electronAffinityKjmol": "201",
	"commonOxidationStates": "-2",
	"commonIons": "-2",
	"electronConfiguration": {
		"predicted": "[Ne] 3s2 3p4",
		"observed": "[Ne] 3s2 3p4"
	},
	"radii": {
		"atomic": 1.09,
		"ionic": 0.29,
		"covalent": 1.02,
		"2-ion": 170,
		"atomicPm": 103
	},
	"volume": 15.5,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7704-34-9",
	"electricalConductivity": 5E-24,
	"specificHeat": 0.71,
	"heatOfFusion": 1.7175,
	"thermalConductivity": 0.269,
	"densityInEarthCrust": 350,
	"densityInSeawater": 905,
	"percentHumanBody": 0.2,
	"notes": "yellow",
	"source": "Sulfur,pyrite,natural gas",
	"colour": "yellow(pale)",
	"characteristics": "(S8)brittle, 19 forms",
	"structure": "S8 rings",
	"hardness": 2,
	"uses": "rubber, batteries (H2SO4)",
	"reactions": {
		"withAir": "vigorous, w/ht =>SO2",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>H2SO4, NOx",
		"with6MNaOH": "none"
	},
	"hydrides": "H2S",
	"oxides": "SO2 SO3",
	"chlorides": "S2Cl2 SCl2",
	"polarizability": "2.9",
	"heatAtomization": 279,
	"abundance": {
		"solarSystem": 5.71180722904119,
		"earthCrust": 2.4
	},
	"cost": {
		"pure": 24
	}
}, {
	"number": 17,
	"symbol": "Cl",
	"group": "17",
	"period": 3,
	"name": {
		"en": "chlorine",
		"de": "Chlor",
		"fr": "chlore",
		"es": "cloro",
		"it": "cloro"
	},
	"mass": 35.45,
	"meltingPoint": -101,
	"boilingPoint": -34.6,
	"state": "g",
	"density": 0.00321,
	"electronegativity": 3.16,
	"ionizationPotentialEv": {
		"1": 12.9676,
		"2": 23.81,
		"3": 39.611
	},
	"ionizationPotentialKjmol": {
		"1": 1251,
		"2": 2297,
		"3": 3822
	},
	"electronAffinityEv": "3.61",
	"electronAffinityKjmol": "348",
	"commonOxidationStates": "-1",
	"commonIons": "-1",
	"electronConfiguration": {
		"predicted": "[Ne] 3s2 3p5",
		"observed": "[Ne] 3s2 3p5"
	},
	"radii": {
		"atomic": 0.97,
		"ionic": 1.81,
		"covalent": 0.99,
		"1-ion": 167,
		"atomicPm": 100
	},
	"volume": 16.9,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7782-50-5",
	"specificHeat": 0.48,
	"heatOfFusion": 3.23,
	"heatOfVaporization": 10.2,
	"thermalConductivity": 0.0089,
	"densityInEarthCrust": 145,
	"densityInSeawater": 19400,
	"percentHumanBody": 0.12,
	"source": "Halite, brine (misc)",
	"colour": "greenish-yellow",
	"characteristics": "diatomic(Cl2), corrosive",
	"structure": "layers of Cl2",
	"uses": "bleach (NaOCl), PVC pipe",
	"reactions": {
		"withAir": "none",
		"withWater": "mild,=>HOCl, Cl-, Cl2(aq)",
		"with6mhci": "mild, =>HOCl, Cl-",
		"with15mhno3": "mild, =>HClOx, NOxCl, NOx",
		"with6MNaOH": "mild, =>OCl-, Cl-"
	},
	"hydrides": "HCl",
	"oxides": "Cl2O ClO2 Cl2O7",
	"chlorides": "Cl2",
	"polarizability": "2.2",
	"heatAtomization": 121,
	"yearDiscovered": 1774,
	"abundance": {
		"solarSystem": 3.71933128698373,
		"earthCrust": 2.1
	},
	"cost": {
		"pure": 0.15
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 18,
	"symbol": "Ar",
	"group": "18",
	"period": 3,
	"name": {
		"en": "argon",
		"de": "Argon",
		"fr": "argon",
		"es": "argón",
		"it": "argo"
	},
	"mass": 39.95,
	"meltingPoint": -189.2,
	"boilingPoint": -185.7,
	"state": "g",
	"density": 0.00178,
	"ionizationPotentialEv": {
		"1": 15.7596,
		"2": 27.629,
		"3": 40.74
	},
	"ionizationPotentialKjmol": {
		"1": 1521,
		"2": 2666,
		"3": 3931
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "0",
	"electronConfiguration": {
		"predicted": "[Ne] 3s2 3p6",
		"observed": "[Ne] 3s2 3p6"
	},
	"radii": {
		"atomic": 0.88,
		"covalent": 0.98,
		"atomicPm": 98
	},
	"volume": 23.9,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-37-1",
	"specificHeat": 0.52,
	"heatOfFusion": 1.188,
	"heatOfVaporization": 6.447,
	"thermalConductivity": 0.01772,
	"densityInEarthCrust": 3.5,
	"densityInSeawater": 0.45,
	"source": "Air",
	"colour": "colorless",
	"characteristics": "Inert",
	"structure": "fcc: face-centered cubic",
	"uses": "lasers, light bulbs",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "none",
	"chlorides": "none",
	"polarizability": "1.586",
	"heatAtomization": 0,
	"yearDiscovered": 1894,
	"abundance": {
		"solarSystem": 5.00432137378264,
		"earthCrust": 0.5
	},
	"cost": {
		"pure": 0.5
	},
	"toxic": "no",
	"carcinogenic": "no"
}, {
	"number": 19,
	"symbol": "K",
	"group": "1",
	"period": 4,
	"name": {
		"en": "potassium",
		"de": "Kalium",
		"fr": "potassium",
		"es": "potasio",
		"it": "potassio"
	},
	"mass": 39.10,
	"meltingPoint": 63.25,
	"boilingPoint": 760,
	"state": "s",
	"density": 0.86,
	"electronegativity": 0.82,
	"ionizationPotentialEv": {
		"1": 4.3407,
		"2": 31.625,
		"3": 45.72
	},
	"ionizationPotentialKjmol": {
		"1": 419,
		"2": 3051,
		"3": 4411
	},
	"electronAffinityEv": "0.5",
	"electronAffinityKjmol": "48",
	"commonOxidationStates": "+1",
	"commonIons": "+1",
	"electronConfiguration": {
		"predicted": "[Ar] 4s1",
		"observed": "[Ar] 4s1"
	},
	"radii": {
		"atomic": 2.77,
		"ionic": 1.51,
		"covalent": 2.03,
		"atomicPm": 227,
		"1+ion": 152
	},
	"volume": 45.46,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-09-7",
	"electricalConductivity": 0.139,
	"specificHeat": 0.75,
	"heatOfFusion": 2.334,
	"heatOfVaporization": 79.87,
	"thermalConductivity": 102.4,
	"densityInEarthCrust": 20900,
	"densityInSeawater": 399,
	"percentHumanBody": 0.2,
	"source": "Sylvite, carnallite (misc)",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "bcc: body-centered cubic",
	"hardness": 0.5,
	"uses": "fertilizer, low-Na salt",
	"reactions": {
		"withAir": "vigorous, =>KO2",
		"withWater": "vigorous, =>H2, KOH",
		"with6mhci": "vigorous, =>H2, KCl",
		"with15mhno3": "vigorous, =>H2, NOx, KNO3",
		"with6MNaOH": "vigorous, =>H2, KOH"
	},
	"hydrides": "KH",
	"oxides": "K2O",
	"chlorides": "KCl",
	"polarizability": "43.4",
	"heatAtomization": 90,
	"yearDiscovered": 1807,
	"abundance": {
		"solarSystem": 3.57634135020579,
		"earthCrust": 4.3
	},
	"cost": {
		"pure": 85
	}
}, {
	"number": 20,
	"symbol": "Ca",
	"group": "2",
	"period": 4,
	"name": {
		"en": "calcium",
		"de": "Kalzium",
		"fr": "calcium",
		"es": "calcio",
		"it": "calcio"
	},
	"mass": 40.08,
	"meltingPoint": 839,
	"boilingPoint": 1484,
	"state": "s",
	"density": 1.55,
	"electronegativity": 1,
	"ionizationPotentialEv": {
		"1": 6.1132,
		"2": 11.871,
		"3": 50.908
	},
	"ionizationPotentialKjmol": {
		"1": 590,
		"2": 1145,
		"3": 4912
	},
	"electronAffinityEv": "0.04",
	"electronAffinityKjmol": "4",
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2",
		"observed": "[Ar] 4s2"
	},
	"radii": {
		"atomic": 2.23,
		"ionic": 1,
		"covalent": 1.74,
		"atomicPm": 197,
		"2+ion": 114
	},
	"volume": 25.9,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-70-2",
	"electricalConductivity": 0.298,
	"specificHeat": 0.63,
	"heatOfFusion": 8.54,
	"heatOfVaporization": 153.3,
	"thermalConductivity": 200,
	"densityInEarthCrust": 41500,
	"densityInSeawater": 412,
	"percentHumanBody": 1.4,
	"source": "Calcite (oxide)",
	"colour": "silvery-white",
	"characteristics": "burns brightly",
	"structure": "hcp: hexagonal close pkd",
	"hardness": 1.5,
	"uses": "lime, cement",
	"reactions": {
		"withAir": "vigorous, =>CaO, Ca3N2",
		"withWater": "mild, =>H2, Ca(OH)2",
		"with6mhci": "vigorous, =>H2, CaCl2",
		"with15mhno3": "vigorous, =>H2, Ca(NO3)2",
		"with6MNaOH": "none"
	},
	"hydrides": "CaH2",
	"oxides": "CaO",
	"chlorides": "CaCl2",
	"polarizability": "22.8",
	"heatAtomization": 178,
	"yearDiscovered": 1808,
	"abundance": {
		"solarSystem": 4.78604121024255,
		"earthCrust": 4.65
	},
	"cost": {
		"pure": 11
	}
}, {
	"number": 21,
	"symbol": "Sc",
	"group": "3",
	"period": 4,
	"name": {
		"en": "scandium",
		"de": "Skandium",
		"fr": "scandium",
		"es": "escandio",
		"it": "scandio"
	},
	"mass": 44.96,
	"meltingPoint": 1541,
	"boilingPoint": 2832,
	"state": "s",
	"density": 2.99,
	"electronegativity": 1.36,
	"ionizationPotentialEv": {
		"1": 6.5614,
		"2": 12.8,
		"3": 24.76
	},
	"ionizationPotentialKjmol": {
		"1": 633,
		"2": 1235,
		"3": 2389
	},
	"electronAffinityEv": "0.19",
	"electronAffinityKjmol": "18",
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d1",
		"observed": "[Ar] 4s2 3d1"
	},
	"radii": {
		"atomic": 2.09,
		"ionic": 0.75,
		"covalent": 1.44,
		"atomicPm": 162,
		"3+ion": 88.5
	},
	"volume": 15,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-20-2",
	"electricalConductivity": 0.0177,
	"specificHeat": 0.6,
	"heatOfFusion": 14.1,
	"heatOfVaporization": 314.2,
	"thermalConductivity": 15.8,
	"densityInEarthCrust": 22,
	"densityInSeawater": 0.0000006,
	"source": "U extractn by-product",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "hcp: hexagonal close pkd",
	"uses": "no significant uses",
	"reactions": {
		"withAir": "vigorous, =>Sc2O3",
		"withWater": "mild, =>H2, Sc(OH)3",
		"with6mhci": "mild, =>H2, ScCl3",
		"with15mhno3": "mild, =>Sc(NO3)3"
	},
	"hydrides": "ScH2 ScH3",
	"oxides": "Sc2O3",
	"chlorides": "ScCl3",
	"polarizability": "17.8",
	"heatAtomization": 378,
	"yearDiscovered": 1879,
	"abundance": {
		"solarSystem": 1.53402610605614,
		"earthCrust": 1.3
	},
	"cost": {
		"pure": 1400
	}
}, {
	"number": 22,
	"symbol": "Ti",
	"group": "4",
	"period": 4,
	"name": {
		"en": "titanium",
		"de": "Titan",
		"fr": "titane",
		"es": "titanio",
		"it": "titanio"
	},
	"mass": 47.87,
	"meltingPoint": 1660,
	"boilingPoint": 3287,
	"state": "s",
	"density": 4.54,
	"electronegativity": 1.54,
	"ionizationPotentialEv": {
		"1": 6.8282,
		"2": 13.58,
		"3": 27.491
	},
	"ionizationPotentialKjmol": {
		"1": 659,
		"2": 1310,
		"3": 2652
	},
	"electronAffinityEv": "0.08",
	"electronAffinityKjmol": "8",
	"commonOxidationStates": "+4,3,2",
	"commonIons": "+4,3,2",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d2",
		"observed": "[Ar] 4s2 3d2"
	},
	"radii": {
		"atomic": 2,
		"ionic": 0.61,
		"covalent": 1.32,
		"atomicPm": 147,
		"2+ion": 100,
		"3+ion": 81
	},
	"volume": 10.64,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-32-6",
	"electricalConductivity": 0.0234,
	"specificHeat": 0.52,
	"heatOfFusion": 15.45,
	"heatOfVaporization": 421,
	"thermalConductivity": 21.9,
	"densityInEarthCrust": 5650,
	"densityInSeawater": 0.001,
	"source": "Ilmenite, rutile(oxide)",
	"colour": "gray",
	"characteristics": "max strength/weight ratio",
	"structure": "hcp: hexagonal close pkd",
	"uses": "steel,white pigment(TiO2)",
	"reactions": {
		"withAir": "mild, w/ht =>TiO2, TiN",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "passivated",
		"with6MNaOH": "none"
	},
	"hydrides": "TiH2",
	"oxides": "TiO Ti2O3 TiO2 + more",
	"chlorides": "TiCl2 TiCl3 TiCl4",
	"polarizability": "14.6",
	"heatAtomization": 470,
	"yearDiscovered": 1791,
	"abundance": {
		"solarSystem": 3.38021124171161,
		"earthCrust": 3.8
	},
	"cost": {
		"pure": 6.1
	}
}, {
	"number": 23,
	"symbol": "V",
	"group": "5",
	"period": 4,
	"name": {
		"en": "vanadium",
		"de": "Vanadium",
		"fr": "vanadium",
		"es": "vanadio",
		"it": "vanadio"
	},
	"mass": 50.94,
	"meltingPoint": 1890,
	"boilingPoint": 3380,
	"state": "s",
	"density": 6.11,
	"electronegativity": 1.63,
	"ionizationPotentialEv": {
		"1": 6.7463,
		"2": 14.65,
		"3": 29.31
	},
	"ionizationPotentialKjmol": {
		"1": 651,
		"2": 1414,
		"3": 2828
	},
	"electronAffinityEv": "0.53",
	"electronAffinityKjmol": "51",
	"commonOxidationStates": "+5,2,3,4",
	"commonIons": "+5,2,3,4",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d3",
		"observed": "[Ar] 4s2 3d3"
	},
	"radii": {
		"atomic": 1.92,
		"ionic": 0.54,
		"covalent": 1.22,
		"atomicPm": 134,
		"2+ion": 93,
		"3+ion": 78
	},
	"volume": 8.78,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-62-2",
	"electricalConductivity": 0.0489,
	"specificHeat": 0.49,
	"heatOfFusion": 20.9,
	"heatOfVaporization": 0.452,
	"thermalConductivity": 30.7,
	"densityInEarthCrust": 120,
	"densityInSeawater": 0.0025,
	"source": "U, Pb vanadates (misc)",
	"colour": "bright white",
	"characteristics": "soft",
	"structure": "bcc: body-centered cubic",
	"uses": "tool steel, V2O5 catalyst",
	"reactions": {
		"withAir": "mild, w/ht =>V2O5, VN",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild",
		"with6MNaOH": "none"
	},
	"hydrides": "VH VH2",
	"oxides": "VO V2O3 VO2 V2O5",
	"chlorides": "VCl2 VCl3 VCl4",
	"polarizability": "12.4",
	"heatAtomization": 514,
	"yearDiscovered": 1830,
	"abundance": {
		"solarSystem": 2.46686762035411,
		"earthCrust": 2.1
	},
	"cost": {
		"pure": 220
	},
	"toxic": "yes"
}, {
	"number": 24,
	"symbol": "Cr",
	"group": "6",
	"period": 4,
	"name": {
		"en": "chromium",
		"de": "Chrom",
		"fr": "chrome",
		"es": "cromo",
		"it": "cromo"
	},
	"mass": 52.00,
	"meltingPoint": 1857,
	"boilingPoint": 2672,
	"state": "s",
	"density": 7.19,
	"electronegativity": 1.66,
	"ionizationPotentialEv": {
		"1": 6.7666,
		"2": 16.5,
		"3": 30.96
	},
	"ionizationPotentialKjmol": {
		"1": 653,
		"2": 1592,
		"3": 2987
	},
	"electronAffinityEv": "0.67",
	"electronAffinityKjmol": "65",
	"commonOxidationStates": "+3,2,6",
	"commonIons": "+3,2,6",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d4",
		"observed": "[Ar] 4s1 3d5"
	},
	"radii": {
		"atomic": 1.85,
		"ionic": 0.62,
		"covalent": 1.18,
		"atomicPm": 128,
		"2+ion": 90.5,
		"3+ion": 75.5
	},
	"volume": 7.23,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-47-3",
	"electricalConductivity": 0.0774,
	"specificHeat": 0.45,
	"heatOfFusion": 16.9,
	"heatOfVaporization": 344.3,
	"thermalConductivity": 93.7,
	"densityInEarthCrust": 102,
	"densityInSeawater": 0.0003,
	"percentHumanBody": 0.000003,
	"source": "Chromite (oxide)",
	"colour": "silvery-white",
	"characteristics": "hard",
	"structure": "bcc: body-centered cubic",
	"hardness": 9,
	"uses": "CrO2 tape, paint, steel",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "mild, =>H2, CrCl3",
		"with15mhno3": "passivated"
	},
	"hydrides": "CrH",
	"oxides": "Cr2O3 CrO2 CrO3+non-stoich",
	"chlorides": "CrCl2 CrCl3",
	"polarizability": "11.6",
	"heatAtomization": 397,
	"yearDiscovered": 1798,
	"abundance": {
		"solarSystem": 4.13033376849501,
		"earthCrust": 2
	},
	"cost": {
		"pure": 10
	}
}, {
	"number": 25,
	"symbol": "Mn",
	"group": "7",
	"period": 4,
	"name": {
		"en": "manganese",
		"de": "Mangan",
		"fr": "manganèse",
		"es": "manganeso",
		"it": "manganese"
	},
	"mass": 54.94,
	"meltingPoint": 1244,
	"boilingPoint": 1962,
	"state": "s",
	"density": 7.43,
	"electronegativity": 1.55,
	"ionizationPotentialEv": {
		"1": 7.434,
		"2": 15.64,
		"3": 33.667
	},
	"ionizationPotentialKjmol": {
		"1": 717,
		"2": 1509,
		"3": 3248
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "+2,3,4,6,7",
	"commonIons": "+2,3,4,6,7",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d5",
		"observed": "[Ar] 4s2 3d5"
	},
	"radii": {
		"atomic": 1.79,
		"ionic": 0.67,
		"covalent": 1.17,
		"atomicPm": 127,
		"2+ion": 89,
		"3+ion": 75.3
	},
	"volume": 7.39,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7439-96-5",
	"electricalConductivity": 0.00695,
	"specificHeat": 0.48,
	"heatOfFusion": 12.05,
	"heatOfVaporization": 266,
	"thermalConductivity": 7.82,
	"densityInEarthCrust": 950,
	"densityInSeawater": 0.0002,
	"percentHumanBody": 0.00002,
	"source": "Pyrolusite,psilomelane(oxide)",
	"colour": "gray-white",
	"characteristics": "hard, brittle",
	"structure": "special:complex (cubic)",
	"hardness": 5,
	"uses": "steel, dry cells(MnO2)",
	"reactions": {
		"withAir": "mild, w/ht =>Mn3O4, Mn3N2",
		"withWater": "none",
		"with6mhci": "mild, =>H2, MnCl2",
		"with15mhno3": "mild, =>Mn(NO3)2, NOx"
	},
	"hydrides": "none",
	"oxides": "MnO Mn3O4 Mn2O3 MnO2 Mn2O7",
	"chlorides": "MnCl2",
	"polarizability": "9.4",
	"heatAtomization": 281,
	"yearDiscovered": 1774,
	"abundance": {
		"solarSystem": 3.98000337158375,
		"earthCrust": 3
	},
	"cost": {
		"pure": 1.7
	},
	"carcinogenic": "no"
}, {
	"number": 26,
	"symbol": "Fe",
	"group": "8",
	"period": 4,
	"name": {
		"en": "iron",
		"de": "Eisen",
		"fr": "fer",
		"es": "hierro",
		"it": "ferro"
	},
	"mass": 55.85,
	"meltingPoint": 1535,
	"boilingPoint": 2750,
	"state": "s",
	"density": 7.86,
	"electronegativity": 1.83,
	"ionizationPotentialEv": {
		"1": 7.9024,
		"2": 16.18,
		"3": 30.651
	},
	"ionizationPotentialKjmol": {
		"1": 762,
		"2": 1561,
		"3": 2957
	},
	"electronAffinityEv": "0.151",
	"electronAffinityKjmol": "15",
	"commonOxidationStates": "+3,2",
	"commonIons": "+3,2",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d6",
		"observed": "[Ar] 4s2 3d6"
	},
	"radii": {
		"atomic": 1.72,
		"ionic": 0.55,
		"covalent": 1.17,
		"atomicPm": 126,
		"2+ion": 83.5,
		"3+ion": 73.8
	},
	"volume": 7.1,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7439-89-6",
	"electricalConductivity": 0.0993,
	"specificHeat": 0.44,
	"heatOfFusion": 13.8,
	"heatOfVaporization": 349.6,
	"thermalConductivity": 80.2,
	"densityInEarthCrust": 56300,
	"densityInSeawater": 0.002,
	"percentHumanBody": 0.006,
	"source": "Hematite,magnetite(oxide)",
	"colour": "gray",
	"characteristics": "fairly soft when pure",
	"structure": "bcc: body-centered cubic",
	"hardness": 4.5,
	"uses": "steel",
	"reactions": {
		"withAir": "mild, =>Fe3O4",
		"withWater": "none",
		"with6mhci": "mild, =>H2, FeCl2",
		"with15mhno3": "passivated"
	},
	"hydrides": "none",
	"oxides": "FeO Fe3O4 Fe2O3",
	"chlorides": "FeCl2 FeCl3",
	"polarizability": "8.4",
	"heatAtomization": 418,
	"abundance": {
		"solarSystem": 5.95424250943932,
		"earthCrust": 4.8
	},
	"cost": {
		"pure": 6.7,
		"bulk": 0.021
	}
}, {
	"number": 27,
	"symbol": "Co",
	"group": "9",
	"period": 4,
	"name": {
		"en": "cobalt",
		"de": "Kobalt",
		"fr": "cobalt",
		"es": "cobalto",
		"it": "cobalto"
	},
	"mass": 58.93,
	"meltingPoint": 1495,
	"boilingPoint": 2870,
	"state": "s",
	"density": 8.9,
	"electronegativity": 1.88,
	"ionizationPotentialEv": {
		"1": 7.881,
		"2": 17.06,
		"3": 33.5
	},
	"ionizationPotentialKjmol": {
		"1": 760,
		"2": 1646,
		"3": 3232
	},
	"electronAffinityEv": "0.66",
	"electronAffinityKjmol": "64",
	"commonOxidationStates": "+2,3",
	"commonIons": "+2,3",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d7",
		"observed": "[Ar] 4s2 3d7"
	},
	"radii": {
		"atomic": 1.67,
		"ionic": 0.65,
		"covalent": 1.16,
		"atomicPm": 125,
		"2+ion": 83.8,
		"3+ion": 71.8
	},
	"volume": 6.7,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-48-4",
	"electricalConductivity": 0.172,
	"specificHeat": 0.42,
	"heatOfFusion": 16.19,
	"heatOfVaporization": 376.5,
	"thermalConductivity": 100,
	"densityInEarthCrust": 25,
	"densityInSeawater": 0.00002,
	"percentHumanBody": 0.000002,
	"source": "Co sulfides & arsenides",
	"colour": "bluish-white",
	"characteristics": "hard",
	"structure": "hcp: hexagonal close pkd",
	"uses": "magnets, Co glass (blue)",
	"reactions": {
		"withAir": "mild, w/ht =>Co3O4",
		"withWater": "none",
		"with6mhci": "mild, =>H2, CoCl2",
		"with15mhno3": "vigorous, =>Co(NO3)2, NOx"
	},
	"hydrides": "none",
	"oxides": "CoO Co3O4",
	"chlorides": "CoCl2",
	"polarizability": "7.5",
	"heatAtomization": 425,
	"yearDiscovered": 1737,
	"abundance": {
		"solarSystem": 3.35218251811136,
		"earthCrust": 1.4
	},
	"cost": {
		"pure": 21
	},
	"carcinogenic": "yes"
}, {
	"number": 28,
	"symbol": "Ni",
	"group": "10",
	"period": 4,
	"name": {
		"en": "nickel",
		"de": "Nickel",
		"fr": "nickel",
		"es": "níquel",
		"it": "nichelio"
	},
	"mass": 58.69,
	"meltingPoint": 1453,
	"boilingPoint": 2730,
	"state": "s",
	"density": 8.9,
	"electronegativity": 1.91,
	"ionizationPotentialEv": {
		"1": 7.6398,
		"2": 18.168,
		"3": 35.17
	},
	"ionizationPotentialKjmol": {
		"1": 737,
		"2": 1753,
		"3": 3393
	},
	"electronAffinityEv": "1.16",
	"electronAffinityKjmol": "112",
	"commonOxidationStates": "+2,3",
	"commonIons": "+2,3",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d8",
		"observed": "[Ar] 4s2 3d8"
	},
	"radii": {
		"atomic": 1.62,
		"ionic": 0.69,
		"covalent": 1.15,
		"atomicPm": 124,
		"2+ion": 83,
		"3+ion": 72
	},
	"volume": 6.59,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-02-0",
	"electricalConductivity": 0.143,
	"specificHeat": 0.44,
	"heatOfFusion": 17.47,
	"heatOfVaporization": 370.4,
	"thermalConductivity": 90.7,
	"densityInEarthCrust": 84,
	"densityInSeawater": 0.000000000000056,
	"percentHumanBody": 0.00001,
	"source": "sulfides, oxide/silicates",
	"colour": "silvery-white",
	"characteristics": "hard, takes a high polish",
	"structure": "fcc: face-centered cubic",
	"uses": "alloys, coins, batteries",
	"reactions": {
		"withAir": "mild, w/ht =>NiO",
		"withWater": "none",
		"with6mhci": "mild, =>H2, NiCl2",
		"with15mhno3": "passivated",
		"with6MNaOH": "none"
	},
	"hydrides": "NiH",
	"oxides": "NiO",
	"chlorides": "NiCl2",
	"polarizability": "6.8",
	"heatAtomization": 430,
	"yearDiscovered": 1751,
	"abundance": {
		"solarSystem": 4.69284691927723,
		"earthCrust": 1.9
	},
	"cost": {
		"pure": 7.7,
		"bulk": 0.76
	},
	"toxic": "yes",
	"carcinogenic": "yes"
}, {
	"number": 29,
	"symbol": "Cu",
	"group": "11",
	"period": 4,
	"name": {
		"en": "copper",
		"de": "Kupfer",
		"fr": "cuivre",
		"es": "cobre",
		"it": "rame"
	},
	"mass": 63.55,
	"meltingPoint": 1083,
	"boilingPoint": 2567,
	"state": "s",
	"density": 8.96,
	"electronegativity": 1.9,
	"ionizationPotentialEv": {
		"1": 7.7264,
		"2": 20.292,
		"3": 36.83
	},
	"ionizationPotentialKjmol": {
		"1": 745,
		"2": 1958,
		"3": 3554
	},
	"electronAffinityEv": "1.24",
	"electronAffinityKjmol": "120",
	"commonOxidationStates": "+2,1",
	"commonIons": "+2,1",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d9",
		"observed": "[Ar] 4s1 3d10"
	},
	"radii": {
		"atomic": 1.57,
		"ionic": 0.73,
		"covalent": 1.17,
		"atomicPm": 128,
		"1+ion": 91,
		"2+ion": 87,
		"3+ion": 68
	},
	"volume": 7.1,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-50-8",
	"electricalConductivity": 0.596,
	"specificHeat": 0.38,
	"heatOfFusion": 13.05,
	"heatOfVaporization": 300.3,
	"thermalConductivity": 401,
	"densityInEarthCrust": 60,
	"densityInSeawater": 0.000000000000025,
	"percentHumanBody": 0.0001,
	"source": "Cu pyrite,chalcocite(sulfide)",
	"colour": "orange-red",
	"characteristics": "soft, ductile",
	"structure": "fcc: face-centered cubic",
	"hardness": 2.75,
	"uses": "wire, bronze, coins",
	"reactions": {
		"withAir": "mild, w/ht =>CuO, Cu2O",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>Cu(NO3)2, NOx"
	},
	"hydrides": "CuH",
	"oxides": "Cu2O CuO",
	"chlorides": "CuCl CuCl2",
	"polarizability": "6.7",
	"heatAtomization": 338,
	"abundance": {
		"solarSystem": 2.71767050300226,
		"earthCrust": 1.7
	},
	"cost": {
		"pure": 2.7,
		"bulk": 0.24
	},
	"carcinogenic": "no"
}, {
	"number": 30,
	"symbol": "Zn",
	"group": "12",
	"period": 4,
	"name": {
		"en": "zinc",
		"de": "Zink",
		"fr": "zinc",
		"es": "zinc",
		"it": "zinco"
	},
	"mass": 65.38,
	"meltingPoint": 419.6,
	"boilingPoint": 906,
	"state": "s",
	"density": 7.13,
	"electronegativity": 1.65,
	"ionizationPotentialEv": {
		"1": 9.3941,
		"2": 17.964,
		"3": 39.722
	},
	"ionizationPotentialKjmol": {
		"1": 906,
		"2": 1733,
		"3": 3833
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d10",
		"observed": "[Ar] 4s2 3d10"
	},
	"radii": {
		"atomic": 1.53,
		"ionic": 0.74,
		"covalent": 1.25,
		"atomicPm": 134,
		"2+ion": 88
	},
	"volume": 9.2,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-66-6",
	"electricalConductivity": 0.166,
	"specificHeat": 0.39,
	"heatOfFusion": 7.322,
	"heatOfVaporization": 115.3,
	"thermalConductivity": 116,
	"densityInEarthCrust": 70,
	"densityInSeawater": 0.0049,
	"percentHumanBody": 0.0033,
	"source": "Sphalerite (sulfide)",
	"colour": "silvery",
	"characteristics": "brittle",
	"structure": "distorted hcp structure",
	"hardness": 2.5,
	"uses": "batteries, galvanizing",
	"reactions": {
		"withAir": "vigorous, =>ZnO",
		"withWater": "none",
		"with6mhci": "mild, =>H2, ZnCl2",
		"with15mhno3": "vigorous, =>Zn(NO3)2, NOx",
		"with6MNaOH": "mild, =>[Zn(OH)4](2-), H2"
	},
	"hydrides": "ZnH2",
	"oxides": "ZnO",
	"chlorides": "ZnCl2",
	"polarizability": "6.4",
	"heatAtomization": 131,
	"yearDiscovered": 1600,
	"abundance": {
		"solarSystem": 3.10037054511756,
		"earthCrust": 1.79
	},
	"cost": {
		"pure": 3.7,
		"bulk": 0.12
	},
	"carcinogenic": "no"
}, {
	"number": 31,
	"symbol": "Ga",
	"group": "13",
	"period": 4,
	"name": {
		"en": "gallium",
		"de": "Gallium",
		"fr": "gallium",
		"es": "galio",
		"it": "gallio"
	},
	"mass": 69.72,
	"meltingPoint": 29.8,
	"boilingPoint": 2403,
	"state": "s",
	"density": 5.9,
	"electronegativity": 1.81,
	"ionizationPotentialEv": {
		"1": 5.9993,
		"2": 20.51,
		"3": 30.71
	},
	"ionizationPotentialKjmol": {
		"1": 579,
		"2": 1979,
		"3": 2963
	},
	"electronAffinityEv": "0.3",
	"electronAffinityKjmol": "29",
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d10 4p1",
		"observed": "[Ar] 4s2 3d10 4p1"
	},
	"radii": {
		"atomic": 1.81,
		"ionic": 0.62,
		"covalent": 1.26,
		"atomicPm": 135,
		"3+ion": 76
	},
	"volume": 11.8,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7440-55-3",
	"electricalConductivity": 0.0678,
	"specificHeat": 0.37,
	"heatOfFusion": 5.59,
	"heatOfVaporization": 258.7,
	"thermalConductivity": 40.6,
	"densityInEarthCrust": 19,
	"densityInSeawater": 0.00003,
	"source": "Bauxite (oxide)",
	"colour": "silvery-blue",
	"characteristics": "melts near r.t.wets glass",
	"structure": "special: complex",
	"hardness": 1.5,
	"uses": "photocells, transistors",
	"reactions": {
		"withAir": "mild, =>Ga2O3",
		"withWater": "none",
		"with6mhci": "mild, =>GaCl3, H2",
		"with6MNaOH": "mild, =>[Ga(OH)4]2-, H2"
	},
	"hydrides": "GaH3",
	"oxides": "Ga2O3",
	"chlorides": "GaCl Ga2Cl6",
	"polarizability": "8.1",
	"heatAtomization": 286,
	"yearDiscovered": 1875,
	"abundance": {
		"solarSystem": 1.57749179983723,
		"earthCrust": 1.2
	},
	"cost": {
		"pure": 220
	}
}, {
	"number": 32,
	"symbol": "Ge",
	"group": "14",
	"period": 4,
	"name": {
		"en": "germanium",
		"de": "Germanium",
		"fr": "germanium",
		"es": "germanio",
		"it": "germanio"
	},
	"mass": 72.63,
	"meltingPoint": 947.4,
	"boilingPoint": 2830,
	"state": "s",
	"density": 5.32,
	"electronegativity": 2.01,
	"ionizationPotentialEv": {
		"1": 7.9,
		"2": 15.934,
		"3": 34.22
	},
	"ionizationPotentialKjmol": {
		"1": 762,
		"2": 1537,
		"3": 3302
	},
	"electronAffinityEv": "1.23",
	"electronAffinityKjmol": "119",
	"commonOxidationStates": "+4,2",
	"commonIons": "+4,2",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d10 4p2",
		"observed": "[Ar] 4s2 3d10 4p2"
	},
	"radii": {
		"atomic": 1.52,
		"ionic": 0.53,
		"covalent": 1.22,
		"atomicPm": 122.3,
		"2+ion": 87
	},
	"volume": 13.6,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-56-2",
	"electricalConductivity": 0.0000000145,
	"specificHeat": 0.32,
	"heatOfFusion": 36.94,
	"heatOfVaporization": 330.9,
	"thermalConductivity": 59.9,
	"densityInEarthCrust": 1.5,
	"densityInSeawater": 0.00005,
	"source": "Zn smelting by-product",
	"colour": "gray-black",
	"characteristics": "semimetal, brittle",
	"structure": "diamond",
	"uses": "transistor, wide-ang lens",
	"reactions": {
		"withAir": "mild, w/ht =>GeO2",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>Ge(IV), NOx",
		"with6MNaOH": "none"
	},
	"hydrides": "GeH4 Ge2H6 + more",
	"oxides": "GeO GeO2",
	"chlorides": "GeCl2 GeCl4",
	"polarizability": "6.1",
	"heatAtomization": 377,
	"yearDiscovered": 1886,
	"abundance": {
		"solarSystem": 2.07554696139253,
		"earthCrust": 0.7
	},
	"cost": {
		"pure": 360
	}
}, {
	"number": 33,
	"symbol": "As",
	"group": "15",
	"period": 4,
	"name": {
		"en": "arsenic",
		"de": "Arsen",
		"fr": "arsenic",
		"es": "arsénico",
		"it": "arsenico"
	},
	"mass": 74.92,
	"meltingPoint": 817,
	"boilingPoint": 617,
	"state": "s",
	"density": 5.73,
	"electronegativity": 2.18,
	"ionizationPotentialEv": {
		"1": 9.8152,
		"2": 18.633,
		"3": 28.351
	},
	"ionizationPotentialKjmol": {
		"1": 947,
		"2": 1798,
		"3": 2735
	},
	"electronAffinityEv": "0.81",
	"electronAffinityKjmol": "78",
	"commonOxidationStates": "±3,+5",
	"commonIons": "-3",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d10 4p3",
		"observed": "[Ar] 4s2 3d10 4p3"
	},
	"radii": {
		"atomic": 1.33,
		"ionic": 0.58,
		"covalent": 1.2,
		"atomicPm": 120,
		"3+ion": 72
	},
	"volume": 13.1,
	"crystalStructure": "rhombohedral",
	"casRegistryNo": "7440-38-2",
	"electricalConductivity": 0.0345,
	"specificHeat": 0.33,
	"heatOfVaporization": 34.76,
	"thermalConductivity": 50,
	"densityInEarthCrust": 1.8,
	"densityInSeawater": 0.0037,
	"notes": "sublimes (28 atm) ",
	"source": "Arsenopyrite, enargite (misc)",
	"colour": "gray",
	"characteristics": "semimetal,brittle,3 forms",
	"structure": "rhombohedral",
	"hardness": 3.5,
	"uses": "photocells (GaAs), poisons",
	"reactions": {
		"withAir": "mild, w/ht =>As4O6",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, w/ht =>H3AsO4, NOx",
		"with6MNaOH": "none"
	},
	"hydrides": "AsH3",
	"oxides": "As2O3",
	"chlorides": "AsCl3 AsCl5",
	"polarizability": "4.3",
	"heatAtomization": 302,
	"yearDiscovered": 1600,
	"abundance": {
		"solarSystem": 0.81690383937566,
		"earthCrust": 0.3
	},
	"cost": {
		"pure": 320
	},
	"carcinogenic": "yes"
}, {
	"number": 34,
	"symbol": "Se",
	"group": "16",
	"period": 4,
	"name": {
		"en": "selenium",
		"de": "Selen",
		"fr": "sélénium",
		"es": "selenio",
		"it": "selenio"
	},
	"mass": 78.97,
	"meltingPoint": 217,
	"boilingPoint": 685,
	"state": "s",
	"density": 4.79,
	"electronegativity": 2.55,
	"ionizationPotentialEv": {
		"1": 9.7524,
		"2": 21.19,
		"3": 30.82
	},
	"ionizationPotentialKjmol": {
		"1": 941,
		"2": 2045,
		"3": 2974
	},
	"electronAffinityEv": "2.02",
	"electronAffinityKjmol": "195",
	"commonOxidationStates": "+4,-2,+6",
	"commonIons": "-2",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d10 4p4",
		"observed": "[Ar] 4s2 3d10 4p4"
	},
	"radii": {
		"atomic": 1.22,
		"ionic": 0.5,
		"covalent": 1.16,
		"2-ion": 184,
		"atomicPm": 119
	},
	"volume": 16.45,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7782-49-2",
	"electricalConductivity": 0.000000000001,
	"specificHeat": 0.32,
	"heatOfFusion": 6.694,
	"heatOfVaporization": 37.7,
	"thermalConductivity": 2.04,
	"densityInEarthCrust": 0.05,
	"densityInSeawater": 0.0002,
	"source": "Cu smelting by-product",
	"colour": "black",
	"characteristics": "6 forms",
	"structure": "parallel chains",
	"hardness": 2,
	"uses": "Xerography, medicines",
	"reactions": {
		"withAir": "vigorous, w/ht =>SeO2",
		"with6mhci": "none",
		"with15mhno3": "mild, =>H2SeO3, NOx"
	},
	"hydrides": "SeH2",
	"oxides": "SeO2 SeO3",
	"chlorides": "Se2Cl2 Se4Cl16",
	"polarizability": "3.8",
	"heatAtomization": 227,
	"yearDiscovered": 1818,
	"abundance": {
		"solarSystem": 1.79309160017658,
		"earthCrust": -1.3
	},
	"cost": {
		"pure": 14
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 35,
	"symbol": "Br",
	"group": "17",
	"period": 4,
	"name": {
		"en": "bromine",
		"de": "Brom",
		"fr": "brome",
		"es": "bromo",
		"it": "bromo"
	},
	"mass": 79.90,
	"meltingPoint": -7.2,
	"boilingPoint": 58.8,
	"state": "l",
	"density": 3.12,
	"electronegativity": 2.96,
	"ionizationPotentialEv": {
		"1": 11.8138,
		"2": 21.8,
		"3": 36
	},
	"ionizationPotentialKjmol": {
		"1": 1140,
		"2": 2103,
		"3": 3473
	},
	"electronAffinityEv": "3.36",
	"electronAffinityKjmol": "324",
	"commonOxidationStates": "±1,+5",
	"commonIons": "-1",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d10 4p5",
		"observed": "[Ar] 4s2 3d10 4p5"
	},
	"radii": {
		"atomic": 1.12,
		"ionic": 1.96,
		"covalent": 1.14,
		"1-ion": 182,
		"atomicPm": 114
	},
	"volume": 25.6,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7726-95-6",
	"specificHeat": 0.473,
	"heatOfFusion": 5.286,
	"heatOfVaporization": 15.438,
	"thermalConductivity": 0.122,
	"densityInEarthCrust": 2.4,
	"densityInSeawater": 67.3,
	"percentHumanBody": 0.00029,
	"source": "Seawater, brines",
	"colour": "red-brown",
	"characteristics": "diatomic,corrosive,liquid",
	"structure": "layers of Br2 ",
	"uses": "pesticides,photography",
	"reactions": {
		"withAir": "none",
		"withWater": "none, dissolves Br2(aq)",
		"with6mhci": "none, dissolves Br2(aq)",
		"with6MNaOH": "mild, =>OBr-, Br-"
	},
	"hydrides": "HBr",
	"oxides": "Br2O BrO2",
	"chlorides": "BrCl",
	"polarizability": "3.1",
	"heatAtomization": 112,
	"yearDiscovered": 1826,
	"abundance": {
		"solarSystem": 1.07188200730613,
		"earthCrust": 0.4
	},
	"cost": {
		"pure": 4.9
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 36,
	"symbol": "Kr",
	"group": "18",
	"period": 4,
	"name": {
		"en": "krypton",
		"de": "Krypton",
		"fr": "krypton",
		"es": "criptón",
		"it": "kripto"
	},
	"mass": 83.80,
	"meltingPoint": -157,
	"boilingPoint": -152,
	"state": "g",
	"density": 0.00374,
	"electronegativity": 3,
	"ionizationPotentialEv": {
		"1": 13.9996,
		"2": 24.359,
		"3": 36.95
	},
	"ionizationPotentialKjmol": {
		"1": 1351,
		"2": 2350,
		"3": 3565
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "0",
	"electronConfiguration": {
		"predicted": "[Ar] 4s2 3d10 4p6",
		"observed": "[Ar] 4s2 3d10 4p6"
	},
	"radii": {
		"atomic": 1.03,
		"covalent": 1.12,
		"atomicPm": 112
	},
	"volume": 38.9,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7439-90-9",
	"specificHeat": 0.248,
	"heatOfFusion": 1.638,
	"heatOfVaporization": 9.029,
	"thermalConductivity": 0.00949,
	"densityInEarthCrust": 0.0001,
	"densityInSeawater": 0.00021,
	"source": "Air",
	"colour": "colorless",
	"characteristics": "unreactive",
	"structure": "fcc: face-centered cubic",
	"uses": "lamps, UV-laser",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "none",
	"chlorides": "none",
	"polarizability": "2.5",
	"heatAtomization": 0,
	"yearDiscovered": 1898,
	"abundance": {
		"solarSystem": 1.65321251377534,
		"earthCrust": -4
	},
	"cost": {
		"pure": 33
	},
	"toxic": "no",
	"carcinogenic": "no"
}, {
	"number": 37,
	"symbol": "Rb",
	"group": "1",
	"period": 5,
	"name": {
		"en": "rubidium",
		"de": "Rubidium",
		"fr": "rubidium",
		"es": "rubidio",
		"it": "rubidio"
	},
	"mass": 85.47,
	"meltingPoint": 38.9,
	"boilingPoint": 686,
	"state": "s",
	"density": 1.53,
	"electronegativity": 0.82,
	"ionizationPotentialEv": {
		"1": 4.1771,
		"2": 27.28,
		"3": 40
	},
	"ionizationPotentialKjmol": {
		"1": 403,
		"2": 2632,
		"3": 3859
	},
	"electronAffinityEv": "0.49",
	"electronAffinityKjmol": "47",
	"commonOxidationStates": "+1",
	"commonIons": "+1",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2",
		"observed": "[Kr] 5s2"
	},
	"radii": {
		"atomic": 2.98,
		"ionic": 1.61,
		"covalent": 2.16,
		"atomicPm": 248,
		"1+ion": 166
	},
	"volume": 55.9,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-17-7",
	"electricalConductivity": 0.0779,
	"specificHeat": 0.363,
	"heatOfFusion": 2.192,
	"heatOfVaporization": 72.216,
	"thermalConductivity": 58.2,
	"densityInEarthCrust": 90,
	"densityInSeawater": 0.12,
	"percentHumanBody": 0.00046,
	"source": "Li extractn by-product",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "bcc: body-centered cubic",
	"hardness": 0.3,
	"uses": "photocells",
	"reactions": {
		"withAir": "vigorous, =>RbO2",
		"withWater": "vigorous, =>H2, RbOH",
		"with6mhci": "vigorous, =>H2, RbCl",
		"with15mhno3": "vigorous,=>RbNO3, H2, NOx",
		"with6MNaOH": "vigorous, =>H2, RbOH"
	},
	"hydrides": "RbH",
	"oxides": "Rb2O",
	"chlorides": "RbCl",
	"polarizability": "47.3",
	"heatAtomization": 86,
	"yearDiscovered": 1861,
	"abundance": {
		"solarSystem": 0.850646235183066,
		"earthCrust": 2
	},
	"cost": {
		"pure": 1200
	}
}, {
	"number": 38,
	"symbol": "Sr",
	"group": "2",
	"period": 5,
	"name": {
		"en": "strontium",
		"de": "Strontium",
		"fr": "strontium",
		"es": "estroncio",
		"it": "stronzio"
	},
	"mass": 87.62,
	"meltingPoint": 769,
	"boilingPoint": 1384,
	"state": "s",
	"density": 2.54,
	"electronegativity": 0.95,
	"ionizationPotentialEv": {
		"1": 5.6948,
		"2": 11.03,
		"3": 43.6
	},
	"ionizationPotentialKjmol": {
		"1": 549,
		"2": 1064,
		"3": 4207
	},
	"electronAffinityEv": "0.11",
	"electronAffinityKjmol": "11",
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2",
		"observed": "[Kr] 5s2"
	},
	"radii": {
		"atomic": 2.45,
		"ionic": 1.26,
		"covalent": 1.91,
		"atomicPm": 215,
		"2+ion": 132
	},
	"volume": 33.7,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-24-6",
	"electricalConductivity": 0.0762,
	"specificHeat": 0.3,
	"heatOfFusion": 8.3,
	"heatOfVaporization": 144,
	"thermalConductivity": 35.3,
	"densityInEarthCrust": 370,
	"densityInSeawater": 7.2,
	"percentHumanBody": 0.00046,
	"source": "Celestite (sulfide)",
	"colour": "silvery",
	"characteristics": "tarnishes",
	"structure": "hcp: hexagonal close pkd",
	"hardness": 1.8,
	"uses": "fireworks (red flame)",
	"reactions": {
		"withAir": "vigorous, =>SrO, Sr2N3",
		"withWater": "mild, =>H2, Sr(OH)2",
		"with6mhci": "vigorous, =>H2, SrCl2",
		"with15mhno3": "vigorous, =>Sr(NO3)2, H2",
		"with6MNaOH": "none"
	},
	"hydrides": "SrH2",
	"oxides": "SrO",
	"chlorides": "SrCl2",
	"polarizability": "27.6",
	"heatAtomization": 164,
	"yearDiscovered": 1808,
	"abundance": {
		"solarSystem": 1.37106786227174,
		"earthCrust": 2.6
	},
	"cost": {
		"pure": 100
	}
}, {
	"number": 39,
	"symbol": "Y",
	"group": "3",
	"period": 5,
	"name": {
		"en": "yttrium",
		"de": "Yttrium",
		"fr": "yttrium",
		"es": "itrio",
		"it": "ittrio"
	},
	"mass": 88.91,
	"meltingPoint": 1523,
	"boilingPoint": 3337,
	"state": "s",
	"density": 4.47,
	"electronegativity": 1.22,
	"ionizationPotentialEv": {
		"1": 6.217,
		"2": 12.24,
		"3": 20.52
	},
	"ionizationPotentialKjmol": {
		"1": 600,
		"2": 1181,
		"3": 1980
	},
	"electronAffinityEv": "0.31",
	"electronAffinityKjmol": "30",
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d1",
		"observed": "[Kr] 5s2 4d1"
	},
	"radii": {
		"atomic": 2.27,
		"ionic": 1.02,
		"covalent": 1.62,
		"atomicPm": 180,
		"3+ion": 104
	},
	"volume": 19.8,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-65-5",
	"electricalConductivity": 0.0166,
	"specificHeat": 0.3,
	"heatOfFusion": 11.4,
	"heatOfVaporization": 363,
	"thermalConductivity": 17.2,
	"densityInEarthCrust": 33,
	"densityInSeawater": 0.000013,
	"source": "xenotime (phosphate)",
	"colour": "silvery",
	"characteristics": "forms protective oxide",
	"structure": "hcp: hexagonal close pkd",
	"uses": "YAG laser, TV phosphor",
	"reactions": {
		"withAir": "vigorous, =>Y2O3",
		"withWater": "mild, =>H2, Y(OH)3",
		"with6mhci": "mild, =>H2, YCl3",
		"with15mhno3": "vigorous, =>Y(NO3)3"
	},
	"hydrides": "YH2 YH3",
	"oxides": "Y2O3",
	"chlorides": "YCl3",
	"polarizability": "22.7",
	"heatAtomization": 423,
	"yearDiscovered": 1794,
	"abundance": {
		"solarSystem": 0.666517980554881,
		"earthCrust": 1.51
	},
	"cost": {
		"pure": 220
	},
	"carcinogenic": "no"
}, {
	"number": 40,
	"symbol": "Zr",
	"group": "4",
	"period": 5,
	"name": {
		"en": "zirconium",
		"de": "Zirkonium",
		"fr": "zirconium",
		"es": "circonio",
		"it": "zirconio"
	},
	"mass": 91.22,
	"meltingPoint": 1852,
	"boilingPoint": 4377,
	"state": "s",
	"density": 6.51,
	"electronegativity": 1.33,
	"ionizationPotentialEv": {
		"1": 6.6339,
		"2": 13.13,
		"3": 22.99
	},
	"ionizationPotentialKjmol": {
		"1": 640,
		"2": 1267,
		"3": 2218
	},
	"electronAffinityEv": "0.43",
	"electronAffinityKjmol": "41",
	"commonOxidationStates": "+4",
	"commonIons": "+4",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d2",
		"observed": "[Kr] 5s2 4d2"
	},
	"radii": {
		"atomic": 2.16,
		"ionic": 0.84,
		"covalent": 1.45,
		"atomicPm": 160
	},
	"volume": 14.1,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-67-7",
	"electricalConductivity": 0.0236,
	"specificHeat": 0.27,
	"heatOfFusion": 16.9,
	"heatOfVaporization": 58.2,
	"thermalConductivity": 22.7,
	"densityInEarthCrust": 165,
	"densityInSeawater": 0.00003,
	"source": "Zircon (silicate)",
	"colour": "grayish-white",
	"characteristics": "hard, relatively inert",
	"structure": "hcp: hexagonal close pkd",
	"uses": "deodorants, fuel rod clad",
	"reactions": {
		"withAir": "mild, w/ht =>Zr2O3",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "passivated",
		"with6MNaOH": "none"
	},
	"hydrides": "ZrH2",
	"oxides": "ZrO2",
	"chlorides": "ZrCl3 ZrCl4",
	"polarizability": "17.9",
	"heatAtomization": 609,
	"yearDiscovered": 1789,
	"abundance": {
		"solarSystem": 1.05690485133647,
		"earthCrust": 2.24
	},
	"cost": {
		"pure": 16,
		"bulk": 16
	}
}, {
	"number": 41,
	"symbol": "Nb",
	"group": "5",
	"period": 5,
	"name": {
		"en": "niobium",
		"de": "Niob",
		"fr": "niobium",
		"es": "niobio",
		"it": "niobio"
	},
	"mass": 92.91,
	"meltingPoint": 2468,
	"boilingPoint": 4742,
	"state": "s",
	"density": 8.57,
	"electronegativity": 1.6,
	"ionizationPotentialEv": {
		"1": 6.7589,
		"2": 14.32,
		"3": 25.04
	},
	"ionizationPotentialKjmol": {
		"1": 652,
		"2": 1382,
		"3": 2416
	},
	"electronAffinityEv": "0.9",
	"electronAffinityKjmol": "87",
	"commonOxidationStates": "+5,3",
	"commonIons": "+5,3",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d3",
		"observed": "[Kr] 5s1 4d4"
	},
	"radii": {
		"atomic": 2.08,
		"ionic": 0.64,
		"covalent": 1.67,
		"atomicPm": 146,
		"3+ion": 86
	},
	"volume": 10.87,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-03-1",
	"electricalConductivity": 0.0693,
	"specificHeat": 0.26,
	"heatOfFusion": 26.4,
	"heatOfVaporization": 682,
	"thermalConductivity": 53.7,
	"densityInEarthCrust": 20,
	"densityInSeawater": 0.00001,
	"source": "columbite (oxide)",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "bcc: body-centered cubic",
	"uses": "stainless steels, magnets",
	"reactions": {
		"withAir": "none, w/ht =>Nb2O3",
		"withWater": "none",
		"with6mhci": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "NbH NbH2",
	"oxides": "NbO NbO2 Nb2O5",
	"chlorides": "NbCl3 NbCl4 NbCl5",
	"polarizability": "15.7",
	"heatAtomization": 726,
	"yearDiscovered": 1801,
	"abundance": {
		"solarSystem": -0.156144577376839,
		"earthCrust": 1.3
	},
	"cost": {
		"pure": 18
	}
}, {
	"number": 42,
	"symbol": "Mo",
	"group": "6",
	"period": 5,
	"name": {
		"en": "molybdenum",
		"de": "Molybdän",
		"fr": "molybdène",
		"es": "molibdeno",
		"it": "molibdeno"
	},
	"mass": 95.95,
	"meltingPoint": 2617,
	"boilingPoint": 4612,
	"state": "s",
	"density": 10.2,
	"electronegativity": 2.16,
	"ionizationPotentialEv": {
		"1": 7.0924,
		"2": 16.461,
		"3": 27.16
	},
	"ionizationPotentialKjmol": {
		"1": 684,
		"2": 1588,
		"3": 2621
	},
	"electronAffinityEv": "0.75",
	"electronAffinityKjmol": "72",
	"commonOxidationStates": "+6,3,5",
	"commonIons": "+6,3,5",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d4",
		"observed": "[Kr] 5s1 4d5"
	},
	"radii": {
		"atomic": 2.01,
		"ionic": 0.59,
		"covalent": 1.3,
		"atomicPm": 139,
		"3+ion": 83
	},
	"volume": 9.4,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7439-98-7",
	"electricalConductivity": 0.187,
	"specificHeat": 0.25,
	"heatOfFusion": 32,
	"heatOfVaporization": 598,
	"thermalConductivity": 138,
	"densityInEarthCrust": 1.2,
	"densityInSeawater": 0.01,
	"percentHumanBody": 0.00001,
	"source": "Molybdenite (sulfide)",
	"colour": "silvery-white",
	"characteristics": "very hard",
	"structure": "bcc: body-centered cubic",
	"uses": "catalysts, tool steel",
	"reactions": {
		"withAir": "none, w/ht =>MoO3",
		"withWater": "none",
		"with6mhci": "none"
	},
	"hydrides": "none",
	"oxides": "MoO2 MoO3",
	"chlorides": "MoClx [x=2-6]",
	"polarizability": "12.8",
	"heatAtomization": 658,
	"yearDiscovered": 1781,
	"abundance": {
		"solarSystem": 0.406540180433955,
		"earthCrust": 0.2
	},
	"cost": {
		"pure": 11
	},
	"carcinogenic": "no"
}, {
	"number": 43,
	"symbol": "Tc",
	"group": "7",
	"period": 5,
	"name": {
		"en": "technetium",
		"de": "Technetium",
		"fr": "technétium",
		"es": "tecnecio",
		"it": "tecnezio"
	},
	"mass": 98,
	"meltingPoint": 2172,
	"boilingPoint": 4877,
	"state": "s",
	"density": 11.5,
	"electronegativity": 1.9,
	"ionizationPotentialEv": {
		"1": 7.28,
		"2": 15.26,
		"3": 29.54
	},
	"ionizationPotentialKjmol": {
		"1": 702,
		"2": 1472,
		"3": 2850
	},
	"electronAffinityEv": "0.55",
	"electronAffinityKjmol": "53",
	"commonOxidationStates": "+7,4,6",
	"commonIons": "+7,4,6",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d5",
		"observed": "[Kr] 5s2 4d5"
	},
	"radii": {
		"atomic": 1.95,
		"covalent": 1.27,
		"atomicPm": 136
	},
	"volume": 8.5,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-26-8",
	"electricalConductivity": 0.067,
	"specificHeat": 0.21,
	"heatOfFusion": 24,
	"heatOfVaporization": 660,
	"thermalConductivity": 50.6,
	"source": "Synthetic (U fission)",
	"colour": "silvery gray",
	"characteristics": "Radioactive, man made",
	"structure": "hcp: hexagonal close pkd",
	"uses": "medical imaging agents",
	"reactions": {
		"withAir": "mild, w/ht, =>Tc2O7",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>HTcO4"
	},
	"hydrides": "none",
	"oxides": "TcO2 Tc2O7",
	"chlorides": "TcCl4",
	"polarizability": "11.4",
	"heatAtomization": 677,
	"yearDiscovered": 1939,
	"abundance": {},
	"cost": {},
	"carcinogenic": "no"
}, {
	"number": 44,
	"symbol": "Ru",
	"group": "8",
	"period": 5,
	"name": {
		"en": "ruthenium",
		"de": "Ruthenium",
		"fr": "ruthénium",
		"es": "rutenio",
		"it": "rutenio"
	},
	"mass": 101.1,
	"meltingPoint": 2310,
	"boilingPoint": 3900,
	"state": "s",
	"density": 12.4,
	"electronegativity": 2.2,
	"ionizationPotentialEv": {
		"1": 7.3605,
		"2": 16.76,
		"3": 28.47
	},
	"ionizationPotentialKjmol": {
		"1": 710,
		"2": 1617,
		"3": 2747
	},
	"electronAffinityEv": "1.05",
	"electronAffinityKjmol": "101",
	"commonOxidationStates": "+4,3,6,8",
	"commonIons": "+4,3,6,8",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d6",
		"observed": "[Kr] 5s1 4d7"
	},
	"radii": {
		"atomic": 1.89,
		"ionic": 0.62,
		"covalent": 1.25,
		"atomicPm": 134,
		"3+ion": 82
	},
	"volume": 8.3,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-18-8",
	"electricalConductivity": 0.137,
	"specificHeat": 0.238,
	"heatOfFusion": 24,
	"heatOfVaporization": 595,
	"thermalConductivity": 117,
	"densityInEarthCrust": 0.001,
	"densityInSeawater": 0.0000007,
	"source": "nickel ores (sulfides)",
	"colour": "silvery-white",
	"characteristics": "hard",
	"structure": "hcp: hexagonal close pkd",
	"hardness": 6.5,
	"uses": "catalysts",
	"reactions": {
		"withAir": "none below 800 deg.C",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none"
	},
	"hydrides": "none",
	"oxides": "RuO2 RuO4",
	"chlorides": "RuCl2 RuCl3",
	"polarizability": "9.6",
	"heatAtomization": 643,
	"yearDiscovered": 1844,
	"abundance": {
		"solarSystem": 0.269512944217916,
		"earthCrust": -3
	},
	"cost": {
		"pure": 1400
	}
}, {
	"number": 45,
	"symbol": "Rh",
	"group": "9",
	"period": 5,
	"name": {
		"en": "rhodium",
		"de": "Rhodium",
		"fr": "rhodium",
		"es": "rodio",
		"it": "rodio"
	},
	"mass": 102.9,
	"meltingPoint": 1966,
	"boilingPoint": 3727,
	"state": "s",
	"density": 12.4,
	"electronegativity": 2.28,
	"ionizationPotentialEv": {
		"1": 7.4589,
		"2": 18.08,
		"3": 31.06
	},
	"ionizationPotentialKjmol": {
		"1": 720,
		"2": 1744,
		"3": 2997
	},
	"electronAffinityEv": "1.14",
	"electronAffinityKjmol": "110",
	"commonOxidationStates": "+3,4,6",
	"commonIons": "+3,4,6",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d7",
		"observed": "[Kr] 5s1 4d8"
	},
	"radii": {
		"atomic": 1.83,
		"ionic": 0.67,
		"covalent": 1.25,
		"atomicPm": 134,
		"3+ion": 80.5
	},
	"volume": 8.3,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-16-6",
	"electricalConductivity": 0.211,
	"specificHeat": 0.242,
	"heatOfFusion": 21.5,
	"heatOfVaporization": 493,
	"thermalConductivity": 150,
	"densityInEarthCrust": 0.001,
	"source": "nickel ores (sulfides)",
	"colour": "silvery-white",
	"characteristics": "forms hard coatings",
	"structure": "fcc: face-centered cubic",
	"uses": "catalysts, elec. contacts",
	"reactions": {
		"withAir": "none, w/ht =>Rh2O3",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "Rh2O3 RhO2",
	"chlorides": "RhCl3",
	"polarizability": "8.6",
	"heatAtomization": 556,
	"yearDiscovered": 1803,
	"abundance": {
		"solarSystem": -0.46344155742847,
		"earthCrust": -3
	},
	"cost": {
		"pure": 13000
	},
	"carcinogenic": "no"
}, {
	"number": 46,
	"symbol": "Pd",
	"group": "10",
	"period": 5,
	"name": {
		"en": "palladium",
		"de": "Palladium",
		"fr": "palladium",
		"es": "paladio",
		"it": "palladio"
	},
	"mass": 106.4,
	"meltingPoint": 1554,
	"boilingPoint": 3140,
	"state": "s",
	"density": 12,
	"electronegativity": 2.2,
	"ionizationPotentialEv": {
		"1": 8.3369,
		"2": 19.63,
		"3": 32.93
	},
	"ionizationPotentialKjmol": {
		"1": 804,
		"2": 1894,
		"3": 3177
	},
	"electronAffinityEv": "0.56",
	"electronAffinityKjmol": "54",
	"commonOxidationStates": "+2,4",
	"commonIons": "+2,4",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d8",
		"observed": "[Kr] (5s0) 4d10"
	},
	"radii": {
		"atomic": 1.79,
		"ionic": 0.64,
		"covalent": 1.28,
		"atomicPm": 137,
		"2+ion": 100,
		"3+ion": 90
	},
	"volume": 8.9,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-05-3",
	"electricalConductivity": 0.095,
	"specificHeat": 0.24,
	"heatOfFusion": 17.6,
	"heatOfVaporization": 357,
	"thermalConductivity": 71.8,
	"densityInEarthCrust": 0.015,
	"source": "nickel ores (sulfides)",
	"colour": "silvery-white",
	"characteristics": "absorbs H2",
	"structure": "fcc: face-centered cubic",
	"hardness": 4.8,
	"uses": "catalysts, dental crowns",
	"reactions": {
		"withAir": "mild, w/ht, =>PdO",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>Pd(NO3)2",
		"with6MNaOH": "none"
	},
	"hydrides": "PdHx x<1 non-stoich",
	"oxides": "PdO",
	"chlorides": "PdCl2",
	"polarizability": "4.8",
	"heatAtomization": 378,
	"yearDiscovered": 1803,
	"abundance": {
		"solarSystem": 0.143014800254095,
		"earthCrust": -2
	},
	"cost": {
		"pure": 15,
		"bulk": 290
	}
}, {
	"number": 47,
	"symbol": "Ag",
	"group": "11",
	"period": 5,
	"name": {
		"en": "silver",
		"de": "Silber",
		"fr": "argent",
		"es": "plata",
		"it": "argento"
	},
	"mass": 107.9,
	"meltingPoint": 962,
	"boilingPoint": 2212,
	"state": "s",
	"density": 10.5,
	"electronegativity": 1.93,
	"ionizationPotentialEv": {
		"1": 7.5762,
		"2": 21.49,
		"3": 34.83
	},
	"ionizationPotentialKjmol": {
		"1": 731,
		"2": 2073,
		"3": 3361
	},
	"electronAffinityEv": "1.3",
	"electronAffinityKjmol": "125",
	"commonOxidationStates": "+1",
	"commonIons": "+1",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d9",
		"observed": "[Kr] 5s1 4d10"
	},
	"radii": {
		"atomic": 1.75,
		"ionic": 1.15,
		"covalent": 1.34,
		"atomicPm": 144,
		"1+ion": 129,
		"2+ion": 108,
		"3+ion": 89
	},
	"volume": 10.3,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-22-4",
	"electricalConductivity": 0.63,
	"specificHeat": 0.235,
	"heatOfFusion": 11.3,
	"heatOfVaporization": 250.58,
	"thermalConductivity": 429,
	"densityInEarthCrust": 0.075,
	"densityInSeawater": 0.00004,
	"source": "Argentite (sulfide)",
	"colour": "silvery",
	"characteristics": "soft, ductile, tarnishes",
	"structure": "fcc: face-centered cubic",
	"hardness": 3.25,
	"uses": "film(AgBr),coins,jewelry",
	"reactions": {
		"withAir": "mild, =>Ag2O",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>AgNO3"
	},
	"hydrides": "none",
	"oxides": "Ag2O",
	"chlorides": "AgCl",
	"polarizability": "7.9",
	"heatAtomization": 284,
	"abundance": {
		"solarSystem": -0.313363730737707,
		"earthCrust": -1.2
	},
	"cost": {
		"pure": 120,
		"bulk": 14
	},
	"carcinogenic": "no"
}, {
	"number": 48,
	"symbol": "Cd",
	"group": "12",
	"period": 5,
	"name": {
		"en": "cadmium",
		"de": "Kadmium",
		"fr": "cadmium",
		"es": "cadmio",
		"it": "cadmio"
	},
	"mass": 112.4,
	"meltingPoint": 320.9,
	"boilingPoint": 765,
	"state": "s",
	"density": 8.65,
	"electronegativity": 1.69,
	"ionizationPotentialEv": {
		"1": 8.9937,
		"2": 16.908,
		"3": 37.48
	},
	"ionizationPotentialKjmol": {
		"1": 868,
		"2": 1631,
		"3": 3616
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d10",
		"observed": "[Kr] 5s2 4d10"
	},
	"radii": {
		"atomic": 1.71,
		"ionic": 0.95,
		"covalent": 1.48,
		"atomicPm": 151,
		"2+ion": 109
	},
	"volume": 13.1,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-43-9",
	"electricalConductivity": 0.138,
	"specificHeat": 0.23,
	"heatOfFusion": 6.192,
	"heatOfVaporization": 99.57,
	"thermalConductivity": 96.8,
	"densityInEarthCrust": 0.15,
	"densityInSeawater": 0.00011,
	"percentHumanBody": 0.00007,
	"source": "Sphalerite (sulfide)",
	"colour": "bluish-white",
	"characteristics": "soft",
	"structure": "distorted hcp structure",
	"hardness": 2,
	"uses": "battery,yel. pigment(CdS)",
	"reactions": {
		"withAir": "mild, w/ht =>CdO",
		"withWater": "none",
		"with6mhci": "mild, =>H2, Cd(Cl)2",
		"with15mhno3": "mild, =>Cd(NO3)2, NOx",
		"with6MNaOH": "none"
	},
	"oxides": "CdO",
	"chlorides": "CdCl2",
	"polarizability": "7.2",
	"heatAtomization": 112,
	"yearDiscovered": 1817,
	"abundance": {
		"solarSystem": 0.20682587603185,
		"earthCrust": -0.7
	},
	"cost": {
		"pure": 6
	},
	"carcinogenic": "yes"
}, {
	"number": 49,
	"symbol": "In",
	"group": "13",
	"period": 5,
	"name": {
		"en": "indium",
		"de": "Indium",
		"fr": "indium",
		"es": "indio",
		"it": "indio"
	},
	"mass": 114.8,
	"meltingPoint": 156.6,
	"boilingPoint": 2080,
	"state": "s",
	"density": 7.31,
	"electronegativity": 1.78,
	"ionizationPotentialEv": {
		"1": 5.7864,
		"2": 18.869,
		"3": 28.03
	},
	"ionizationPotentialKjmol": {
		"1": 558,
		"2": 1821,
		"3": 2704
	},
	"electronAffinityEv": "0.3",
	"electronAffinityKjmol": "29",
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d10 5p1",
		"observed": "[Kr] 5s2 4d10 5p1"
	},
	"radii": {
		"atomic": 2,
		"ionic": 0.8,
		"covalent": 1.44,
		"atomicPm": 167,
		"3+ion": 94
	},
	"volume": 15.7,
	"crystalStructure": "tetragonal",
	"casRegistryNo": "7440-74-6",
	"electricalConductivity": 0.116,
	"specificHeat": 0.23,
	"heatOfFusion": 3.263,
	"heatOfVaporization": 231.5,
	"thermalConductivity": 81.6,
	"densityInEarthCrust": 0.25,
	"densityInSeawater": 0.002,
	"source": "Zn/Pb smelting by-product",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "distorted fcc structure",
	"hardness": 1.2,
	"uses": "transistors, photocells",
	"reactions": {
		"withAir": "mild, =>In2O3",
		"with6mhci": "mild, =>H2, InCl3",
		"with15mhno3": "mild, =>In(NO3)3",
		"with6MNaOH": "none"
	},
	"oxides": "In2O3",
	"chlorides": "InCl InCl3",
	"polarizability": "9.7",
	"heatAtomization": 243,
	"yearDiscovered": 1863,
	"abundance": {
		"solarSystem": -0.735182176990463,
		"earthCrust": -1
	},
	"cost": {
		"pure": 120
	},
	"carcinogenic": "no"
}, {
	"number": 50,
	"symbol": "Sn",
	"group": "14",
	"period": 5,
	"name": {
		"en": "tin",
		"de": "Zinn",
		"fr": "Étain",
		"es": "estaño",
		"it": "stagno"
	},
	"mass": 118.7,
	"meltingPoint": 232,
	"boilingPoint": 2270,
	"state": "s",
	"density": 7.31,
	"electronegativity": 1.96,
	"ionizationPotentialEv": {
		"1": 7.3438,
		"2": 14.632,
		"3": 30.502
	},
	"ionizationPotentialKjmol": {
		"1": 709,
		"2": 1412,
		"3": 2943
	},
	"electronAffinityEv": "1.11",
	"electronAffinityKjmol": "107",
	"commonOxidationStates": "+4,2",
	"commonIons": "+4,2",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d10 5p2",
		"observed": "[Kr] 5s2 4d10 5p2"
	},
	"radii": {
		"atomic": 1.72,
		"ionic": 0.71,
		"covalent": 1.41,
		"atomicPm": 140.5
	},
	"volume": 16.3,
	"crystalStructure": "tetragonal",
	"casRegistryNo": "7440-31-5",
	"electricalConductivity": 0.0917,
	"specificHeat": 0.227,
	"heatOfFusion": 7.029,
	"heatOfVaporization": 295.8,
	"thermalConductivity": 66.6,
	"densityInEarthCrust": 2.3,
	"densityInSeawater": 0.000004,
	"percentHumanBody": 0.00002,
	"source": "Cassiterite (oxide)",
	"colour": "silvery-white",
	"characteristics": "resists corrosion;2 forms",
	"structure": "distorted diamond",
	"hardness": 1.65,
	"uses": "solder, pewter, bronze",
	"reactions": {
		"withAir": "mild, w/ht, =>SnO2",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>SnO2, NOx",
		"with6MNaOH": "mild, =>H2, [Sn(OH)6] 2-"
	},
	"hydrides": "SnH4 Sn2H6",
	"oxides": "SnO SnO2",
	"chlorides": "SnCl2 SnCl4",
	"polarizability": "7.7",
	"heatAtomization": 302,
	"abundance": {
		"solarSystem": 0.582063362911709,
		"earthCrust": 0.3
	},
	"cost": {
		"pure": 8,
		"bulk": 0.83
	},
	"carcinogenic": "no"
}, {
	"number": 51,
	"symbol": "Sb",
	"group": "15",
	"period": 5,
	"name": {
		"en": "antimony",
		"de": "Antimon",
		"fr": "antimoine",
		"es": "antimonio",
		"it": "antimonio"
	},
	"mass": 121.8,
	"meltingPoint": 631,
	"boilingPoint": 1950,
	"state": "s",
	"density": 6.69,
	"electronegativity": 2.05,
	"ionizationPotentialEv": {
		"1": 8.64,
		"2": 16.53,
		"3": 25.3
	},
	"ionizationPotentialKjmol": {
		"1": 834,
		"2": 1595,
		"3": 2441
	},
	"electronAffinityEv": "1.07",
	"electronAffinityKjmol": "103",
	"commonOxidationStates": "+3,5",
	"commonIons": "+3,5",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d10 5p3",
		"observed": "[Kr] 5s2 4d10 5p3"
	},
	"radii": {
		"atomic": 1.53,
		"ionic": 0.76,
		"covalent": 1.4,
		"atomicPm": 140,
		"3+ion": 90
	},
	"volume": 18.23,
	"crystalStructure": "rhombohedral",
	"casRegistryNo": "7440-36-0",
	"electricalConductivity": 0.0288,
	"specificHeat": 0.21,
	"heatOfFusion": 19.87,
	"heatOfVaporization": 77.14,
	"thermalConductivity": 24.3,
	"densityInEarthCrust": 0.2,
	"densityInSeawater": 0.00024,
	"source": "Stibnite (sulfide)",
	"colour": "bluish-white",
	"characteristics": "brittle; 5 forms",
	"structure": "rhombohedral",
	"hardness": 3.15,
	"uses": "Pb alloy(battery), dyeing",
	"reactions": {
		"withAir": "mild, w/ht, =>Sb2Ox x=3-5",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>Sb2O5",
		"with6MNaOH": "none"
	},
	"hydrides": "SbH3",
	"oxides": "Sb2O3 Sb2O4 Sb2O5",
	"chlorides": "SbCl3 SbCl5",
	"polarizability": "6.6",
	"heatAtomization": 262,
	"yearDiscovered": 1600,
	"abundance": {
		"solarSystem": -0.510041520575165,
		"earthCrust": -0.7
	},
	"cost": {
		"pure": 4.5,
		"bulk": 0.44
	},
	"carcinogenic": "no"
}, {
	"number": 52,
	"symbol": "Te",
	"group": "16",
	"period": 5,
	"name": {
		"en": "tellurium",
		"de": "Tellur",
		"fr": "tellure",
		"es": "teluro",
		"it": "tellurio"
	},
	"mass": 127.6,
	"meltingPoint": 449.5,
	"boilingPoint": 989.8,
	"state": "s",
	"density": 6.24,
	"electronegativity": 2.1,
	"ionizationPotentialEv": {
		"1": 9.0096,
		"2": 18.6,
		"3": 27.96
	},
	"ionizationPotentialKjmol": {
		"1": 869,
		"2": 1795,
		"3": 2698
	},
	"electronAffinityEv": "1.97",
	"electronAffinityKjmol": "190",
	"commonOxidationStates": "+4,6,-2",
	"commonIons": "-2",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d10 5p4",
		"observed": "[Kr] 5s2 4d10 5p4"
	},
	"radii": {
		"atomic": 1.43,
		"ionic": 0.97,
		"covalent": 1.36,
		"2-ion": 207,
		"atomicPm": 142
	},
	"volume": 20.5,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "13494-80-9",
	"electricalConductivity": 0.000002,
	"specificHeat": 0.2,
	"heatOfFusion": 17.49,
	"heatOfVaporization": 52.55,
	"thermalConductivity": 2.35,
	"densityInEarthCrust": 0.001,
	"source": "Zn/Pb smelting by-product",
	"colour": "silvery",
	"characteristics": "semimetal",
	"structure": "parallel chains",
	"hardness": 2.3,
	"uses": "semiconductors, steel",
	"reactions": {
		"withAir": "mild, w/ht, =>TeO2",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>Te(IV)"
	},
	"hydrides": "H2Te",
	"oxides": "TeO2 TeO3",
	"chlorides": "Te2Cl Te3Cl2 Te4Cl16",
	"polarizability": "5.5",
	"heatAtomization": 197,
	"yearDiscovered": 1783,
	"abundance": {
		"solarSystem": 0.682145076373832,
		"earthCrust": -3
	},
	"cost": {
		"pure": 24
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 53,
	"symbol": "I",
	"group": "17",
	"period": 5,
	"name": {
		"en": "iodine",
		"de": "Iod (Jod)",
		"fr": "iode",
		"es": "yodo",
		"it": "iodio"
	},
	"mass": 126.9,
	"meltingPoint": 113.5,
	"boilingPoint": 184,
	"state": "s",
	"density": 4.93,
	"electronegativity": 2.66,
	"ionizationPotentialEv": {
		"1": 10.4513,
		"2": 19.131,
		"3": 33
	},
	"ionizationPotentialKjmol": {
		"1": 1008,
		"2": 1846,
		"3": 3184
	},
	"electronAffinityEv": "3.06",
	"electronAffinityKjmol": "295",
	"commonOxidationStates": "-1,+5,7",
	"commonIons": "-1",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d10 5p5",
		"observed": "[Kr] 5s2 4d10 5p5"
	},
	"radii": {
		"atomic": 1.32,
		"ionic": 2.2,
		"covalent": 1.33,
		"1-ion": 206,
		"atomicPm": 133
	},
	"volume": 25.74,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7553-56-2",
	"electricalConductivity": 0.0000000000000008,
	"specificHeat": 0.214,
	"heatOfFusion": 7.824,
	"heatOfVaporization": 20.752,
	"thermalConductivity": 0.449,
	"densityInEarthCrust": 0.45,
	"densityInSeawater": 0.06,
	"percentHumanBody": 0.00002,
	"source": "Brines, Chilean nitrate (misc)",
	"colour": "bluish-black",
	"characteristics": "diatomic",
	"structure": "layers of I2",
	"uses": "nutrient, antiseptic",
	"reactions": {
		"withAir": "none",
		"withWater": "none, dissolves slightly",
		"with6mhci": "none",
		"with15mhno3": "mild, =>HIO3",
		"with6MNaOH": "mild, =>OI-, I-"
	},
	"hydrides": "HI",
	"oxides": "I2O5 I4O9 I2O4",
	"chlorides": "ICl ICl3",
	"polarizability": "5",
	"heatAtomization": 107,
	"yearDiscovered": 1811,
	"abundance": {
		"solarSystem": -0.0457574905606751,
		"earthCrust": -0.3
	},
	"cost": {
		"pure": 8.3
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 54,
	"symbol": "Xe",
	"group": "18",
	"period": 5,
	"name": {
		"en": "xenon",
		"de": "Xenon",
		"fr": "xénon",
		"es": "xenón",
		"it": "xeno"
	},
	"mass": 131.3,
	"meltingPoint": -111.8,
	"boilingPoint": -107.1,
	"state": "g",
	"density": 0.00589,
	"electronegativity": 2.6,
	"ionizationPotentialEv": {
		"1": 12.1299,
		"2": 21.21,
		"3": 32.1
	},
	"ionizationPotentialKjmol": {
		"1": 1170,
		"2": 2046,
		"3": 3097
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "0",
	"electronConfiguration": {
		"predicted": "[Kr] 5s2 4d10 5p6",
		"observed": "[Kr] 5s2 4d10 5p6"
	},
	"radii": {
		"atomic": 1.24,
		"covalent": 1.31,
		"atomicPm": 131
	},
	"volume": 37.3,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-63-3",
	"specificHeat": 0.158,
	"heatOfFusion": 2.297,
	"heatOfVaporization": 12.636,
	"thermalConductivity": 0.00569,
	"densityInEarthCrust": 0.00003,
	"densityInSeawater": 0.00005,
	"source": "Air",
	"colour": "colorless",
	"characteristics": "unreactive",
	"structure": "fcc: face-centered cubic",
	"uses": "UV-laser, hi intens lamps",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "XeO3 XeO4",
	"chlorides": "none",
	"polarizability": "4",
	"heatAtomization": 0,
	"yearDiscovered": 1898,
	"abundance": {
		"solarSystem": 0.672097857935717,
		"earthCrust": -4.5
	},
	"cost": {
		"pure": 120
	},
	"toxic": "no",
	"carcinogenic": "no"
}, {
	"number": 55,
	"symbol": "Cs",
	"group": "1",
	"period": 6,
	"name": {
		"en": "cesium",
		"de": "Zäsium",
		"fr": "césium",
		"es": "cesio",
		"it": "cesio"
	},
	"mass": 132.9,
	"meltingPoint": 28.4,
	"boilingPoint": 669,
	"state": "s",
	"density": 1.87,
	"electronegativity": 0.79,
	"ionizationPotentialEv": {
		"1": 3.8939,
		"2": 25.1
	},
	"ionizationPotentialKjmol": {
		"1": 376,
		"2": 2422
	},
	"electronAffinityEv": "0.47",
	"electronAffinityKjmol": "45",
	"commonOxidationStates": "+1",
	"commonIons": "+1",
	"electronConfiguration": {
		"predicted": "[Xe] 6s1",
		"observed": "[Xe] 6s1"
	},
	"radii": {
		"atomic": 3.34,
		"ionic": 1.74,
		"covalent": 2.35,
		"atomicPm": 265,
		"1+ion": 181
	},
	"volume": 71.07,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-46-2",
	"electricalConductivity": 0.0489,
	"specificHeat": 0.24,
	"heatOfFusion": 2.092,
	"heatOfVaporization": 67.74,
	"thermalConductivity": 59,
	"densityInEarthCrust": 3,
	"densityInSeawater": 0.0003,
	"percentHumanBody": 0.000002,
	"source": "Li extractn by-product",
	"colour": "yellow/silvery",
	"characteristics": "softest metal",
	"structure": "bcc: body-centered cubic",
	"hardness": 0.2,
	"uses": "photocells, IR lamps",
	"reactions": {
		"withAir": "vigorous, =>CsO2",
		"withWater": "vigorous, =>H2, CsOH",
		"with6mhci": "vigorous, =>H2, CsCl",
		"with15mhno3": "vigorous, =>CsNO3",
		"with6MNaOH": "vigorous, =>H2, CsOH"
	},
	"hydrides": "CsH",
	"oxides": "Cs2O",
	"chlorides": "CsCl",
	"polarizability": "59.6",
	"heatAtomization": 79,
	"yearDiscovered": 1860,
	"abundance": {
		"solarSystem": -0.429457060118102,
		"earthCrust": 0
	},
	"cost": {
		"pure": 1100
	}
}, {
	"number": 56,
	"symbol": "Ba",
	"group": "2",
	"period": 6,
	"name": {
		"en": "barium",
		"de": "Barium",
		"fr": "baryum",
		"es": "bario",
		"it": "bario"
	},
	"mass": 137.3,
	"meltingPoint": 725,
	"boilingPoint": 1640,
	"state": "s",
	"density": 3.5,
	"electronegativity": 0.89,
	"ionizationPotentialEv": {
		"1": 5.2117,
		"2": 10.004
	},
	"ionizationPotentialKjmol": {
		"1": 503,
		"2": 965
	},
	"electronAffinityEv": "0.15",
	"electronAffinityKjmol": "14",
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2",
		"observed": "[Xe] 6s2"
	},
	"radii": {
		"atomic": 2.78,
		"ionic": 1.42,
		"covalent": 1.98,
		"atomicPm": 222,
		"2+ion": 149
	},
	"volume": 39.24,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-39-3",
	"electricalConductivity": 0.03,
	"specificHeat": 0.204,
	"heatOfFusion": 7.75,
	"heatOfVaporization": 142,
	"thermalConductivity": 18.4,
	"densityInEarthCrust": 425,
	"densityInSeawater": 0.013,
	"percentHumanBody": 0.00003,
	"source": "Barite (sulfate)",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "bcc: body-centered cubic",
	"uses": "X-ray imaging agnt(BaSO4)",
	"reactions": {
		"withAir": "vigorous,wt/ht=>BaO,Ba2N3",
		"withWater": "vigorous, =>H2, Ba(OH)2",
		"with6mhci": "vigorous, =>H2, BaCl2",
		"with15mhno3": "mild, =>Ba(NO3)2",
		"with6MNaOH": "passivated"
	},
	"hydrides": "BaH2",
	"oxides": "BaO",
	"chlorides": "BaCl2",
	"polarizability": "39.7",
	"heatAtomization": 180,
	"yearDiscovered": 1808,
	"abundance": {
		"solarSystem": 0.652246341003323,
		"earthCrust": 2.6
	},
	"cost": {
		"pure": 55
	},
	"carcinogenic": "no"
}, {
	"number": 57,
	"symbol": "La",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "lanthanum",
		"de": "Lanthan",
		"fr": "lanthane",
		"es": "lantano",
		"it": "lantanio"
	},
	"mass": 138.9,
	"meltingPoint": 920,
	"boilingPoint": 3454,
	"state": "s",
	"density": 6.15,
	"electronegativity": 1.1,
	"ionizationPotentialEv": {
		"1": 5.577,
		"2": 11.059,
		"3": 19.174
	},
	"ionizationPotentialKjmol": {
		"1": 538,
		"2": 1067,
		"3": 1850
	},
	"electronAffinityEv": "0.5",
	"electronAffinityKjmol": "48",
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f1",
		"observed": "[Xe] 6s2 4f1"
	},
	"radii": {
		"atomic": 2.74,
		"ionic": 1.16,
		"covalent": 1.69,
		"atomicPm": 187,
		"3+ion": 117.2
	},
	"volume": 20.73,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7439-91-0",
	"electricalConductivity": 0.0126,
	"specificHeat": 0.19,
	"heatOfFusion": 6.2,
	"heatOfVaporization": 414,
	"thermalConductivity": 13.5,
	"densityInEarthCrust": 39,
	"densityInSeawater": 0.0000034,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "soft, malleable",
	"structure": " cp: close packed (ABCB)",
	"uses": "lighter flints, CRTs",
	"reactions": {
		"withAir": "vigorous, w/ht =>La2O3",
		"withWater": "mild, =>H2, La(OH)3",
		"with6mhci": "mild, =>H2, LaCl3",
		"with15mhno3": "mild, =>La(NO3)3"
	},
	"hydrides": "LaH2 LaH3",
	"oxides": "La2O3",
	"chlorides": "LaCl3",
	"polarizability": "31.1",
	"heatAtomization": 423,
	"yearDiscovered": 1839,
	"abundance": {
		"solarSystem": -0.350665141287858,
		"earthCrust": 1.5
	},
	"cost": {
		"pure": 64
	}
}, {
	"number": 58,
	"symbol": "Ce",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "cerium",
		"de": "Zer",
		"fr": "cérium",
		"es": "cerio",
		"it": "cerio"
	},
	"mass": 140.1,
	"meltingPoint": 798,
	"boilingPoint": 3257,
	"state": "s",
	"density": 6.66,
	"electronegativity": 1.12,
	"ionizationPotentialEv": {
		"1": 5.5387,
		"2": 10.851,
		"3": 20.2
	},
	"ionizationPotentialKjmol": {
		"1": 534,
		"2": 1047,
		"3": 1949
	},
	"commonOxidationStates": "+3,4",
	"commonIons": "+3,4",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f2",
		"observed": "[Xe] 6s2 4f2"
	},
	"radii": {
		"atomic": 2.7,
		"ionic": 1.14,
		"covalent": 1.65,
		"atomicPm": 182,
		"3+ion": 115
	},
	"volume": 20.67,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-45-1",
	"electricalConductivity": 0.0115,
	"specificHeat": 0.19,
	"heatOfFusion": 5.46,
	"heatOfVaporization": 414,
	"thermalConductivity": 11.4,
	"densityInEarthCrust": 66.5,
	"densityInSeawater": 0.0000012,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "malleable",
	"structure": "fcc: face-centered cubic",
	"uses": "self-cleaning ovens(CeO2)",
	"reactions": {
		"withAir": "vigorous, w/ht =>CeO2",
		"withWater": "mild, =>H2, Ce(OH)3",
		"with6mhci": "vigorous, =>H2, CeCl3",
		"with15mhno3": "mild, =>Ce(NO3)3"
	},
	"hydrides": "CeH2 CeH3",
	"oxides": "Ce2O3 CeO2",
	"chlorides": "CeCl3",
	"polarizability": "29.6",
	"heatAtomization": 419,
	"yearDiscovered": 1803,
	"abundance": {
		"solarSystem": 0.055378331375,
		"earthCrust": 1.8
	},
	"cost": {
		"pure": 57
	}
}, {
	"number": 59,
	"symbol": "Pr",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "praseodymium",
		"de": "Praseodym",
		"fr": "praséodyme",
		"es": "prosedimio",
		"it": "praseodimio"
	},
	"mass": 140.9,
	"meltingPoint": 931,
	"boilingPoint": 3017,
	"state": "s",
	"density": 6.77,
	"electronegativity": 1.13,
	"ionizationPotentialEv": {
		"1": 5.464,
		"2": 10.551,
		"3": 21.62
	},
	"ionizationPotentialKjmol": {
		"1": 527,
		"2": 1018,
		"3": 2086
	},
	"commonOxidationStates": "+3,4",
	"commonIons": "+3,4",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f3",
		"observed": "[Xe] 6s2 4f3"
	},
	"radii": {
		"atomic": 2.67,
		"ionic": 1.13,
		"covalent": 1.65,
		"atomicPm": 182,
		"3+ion": 113
	},
	"volume": 20.8,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-10-0",
	"electricalConductivity": 0.0148,
	"specificHeat": 0.19,
	"heatOfFusion": 6.89,
	"heatOfVaporization": 296.8,
	"thermalConductivity": 12.5,
	"densityInEarthCrust": 9.2,
	"densityInSeawater": 0.00000064,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": " cp: close packed (ABCB)",
	"uses": "alloys, Welder's mask",
	"reactions": {
		"withAir": "vigorous, =>Pr6O11 approx",
		"withWater": "mild, =>H2, Pr(OH)3",
		"with6mhci": "mild, =>H2, PrCl3",
		"with15mhno3": "mild, =>Pr(NO3)3"
	},
	"hydrides": "PrH2 PrH3",
	"oxides": "PrO2 Pr6O11",
	"chlorides": "PrCl3",
	"polarizability": "28.2",
	"heatAtomization": 356,
	"yearDiscovered": 1885,
	"abundance": {
		"solarSystem": -0.777543663320753,
		"earthCrust": 0.9
	},
	"cost": {
		"pure": 170
	}
}, {
	"number": 60,
	"symbol": "Nd",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "neodymium",
		"de": "Neodym",
		"fr": "néodyme",
		"es": "neodimio",
		"it": "neodimio"
	},
	"mass": 144.2,
	"meltingPoint": 1016,
	"boilingPoint": 3127,
	"state": "s",
	"density": 7,
	"electronegativity": 1.14,
	"ionizationPotentialEv": {
		"1": 5.525,
		"2": 10.727,
		"3": 22.076
	},
	"ionizationPotentialKjmol": {
		"1": 533,
		"2": 1035,
		"3": 2130
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f4",
		"observed": "[Xe] 6s2 4f4"
	},
	"radii": {
		"atomic": 2.64,
		"covalent": 1.64,
		"atomicPm": 181,
		"3+ion": 112.3
	},
	"volume": 20.6,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-00-8",
	"electricalConductivity": 0.0157,
	"specificHeat": 0.19,
	"heatOfFusion": 7.14,
	"heatOfVaporization": 273,
	"thermalConductivity": 16.5,
	"densityInEarthCrust": 41.5,
	"densityInSeawater": 0.0000028,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"structure": " cp: close packed (ABCB)",
	"uses": "alloys, Welder's mask",
	"reactions": {
		"withAir": "vigorous, w/ht =>Nd2O3",
		"withWater": "mild, =>H2, Nd(OH)3",
		"with6mhci": "mild, =>H2, NdCl3",
		"with15mhno3": "mild, =>Nd(NO3)3"
	},
	"hydrides": "NdH2 NdH3",
	"oxides": "Nd2O3",
	"chlorides": "NdCl2 NdCl3",
	"polarizability": "31.4",
	"heatAtomization": 328,
	"yearDiscovered": 1885,
	"abundance": {
		"solarSystem": -0.0820221174070917,
		"earthCrust": 1.4
	},
	"cost": {
		"pure": 110
	}
}, {
	"number": 61,
	"symbol": "Pm",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "promethium",
		"de": "Promethium",
		"fr": "prométhium",
		"es": "promecio",
		"it": "promezio"
	},
	"mass": 145,
	"meltingPoint": 1042,
	"boilingPoint": 3000,
	"state": "s",
	"density": 7.26,
	"ionizationPotentialEv": {
		"1": 5.55,
		"2": 10.903,
		"3": 22.283
	},
	"ionizationPotentialKjmol": {
		"1": 535,
		"2": 1052,
		"3": 2150
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f5",
		"observed": "[Xe] 6s2 4f5"
	},
	"radii": {
		"atomic": 2.62,
		"ionic": 1.07,
		"covalent": 1.63,
		"atomicPm": 183,
		"3+ion": 111
	},
	"volume": 22.39,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-12-2",
	"specificHeat": 0.18,
	"thermalConductivity": 17.9,
	"source": "Synthetic",
	"colour": "silvery-white",
	"characteristics": "Radioactive",
	"structure": " cp: close packed (ABCB)",
	"uses": "nuclear battery",
	"reactions": {
		"withAir": "vigorous, =>Pm2O3",
		"withWater": "mild, =>H2, Pm(OH)3",
		"with6mhci": "mild, =>H2, PmCl3",
		"with15mhno3": "mild, =>Pm(NO3)3"
	},
	"oxides": "Pm2O3",
	"polarizability": "30.1",
	"heatAtomization": 301,
	"yearDiscovered": 1945,
	"abundance": {
		"earthCrust": -19.3
	},
	"cost": {}
}, {
	"number": 62,
	"symbol": "Sm",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "samarium",
		"de": "Samarium",
		"fr": "samarium",
		"es": "samario",
		"it": "samario"
	},
	"mass": 150.4,
	"meltingPoint": 1074,
	"boilingPoint": 1794,
	"state": "s",
	"density": 7.52,
	"electronegativity": 1.17,
	"ionizationPotentialEv": {
		"1": 5.6437,
		"2": 11.069,
		"3": 23.423
	},
	"ionizationPotentialKjmol": {
		"1": 545,
		"2": 1068,
		"3": 2260
	},
	"commonOxidationStates": "+3,2",
	"commonIons": "+3,2",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f6",
		"observed": "[Xe] 6s2 4f6"
	},
	"radii": {
		"atomic": 2.59,
		"ionic": 1.08,
		"covalent": 1.62,
		"atomicPm": 180,
		"3+ion": 109.8
	},
	"volume": 19.95,
	"crystalStructure": "rhombohedral",
	"casRegistryNo": "7440-19-9",
	"electricalConductivity": 0.00956,
	"specificHeat": 0.2,
	"heatOfFusion": 8.63,
	"heatOfVaporization": 166.4,
	"thermalConductivity": 13.3,
	"densityInEarthCrust": 7.05,
	"densityInSeawater": 0.00000045,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"structure": " cp: cls pkd (ABCBCACAB)",
	"uses": "alloys, headphones",
	"reactions": {
		"withAir": "vigorous, =>Sm2O3",
		"withWater": "mild, =>H2, Sm(OH)3",
		"with6mhci": "mild, =>H2, SmCl3",
		"with15mhno3": "mild, =>Sm(NO3)3"
	},
	"hydrides": "SmH2 SmH3",
	"oxides": "Sm2O3",
	"chlorides": "SmCl2 SmCl3",
	"polarizability": "28.8",
	"heatAtomization": 207,
	"yearDiscovered": 1880,
	"abundance": {
		"solarSystem": -0.588043762069599,
		"earthCrust": 0.8
	},
	"cost": {
		"pure": 130
	}
}, {
	"number": 63,
	"symbol": "Eu",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "europium",
		"de": "Europium",
		"fr": "europium",
		"es": "europio",
		"it": "europio"
	},
	"mass": 152.0,
	"meltingPoint": 822,
	"boilingPoint": 1529,
	"state": "s",
	"density": 5.24,
	"ionizationPotentialEv": {
		"1": 5.6704,
		"2": 11.245,
		"3": 24.926
	},
	"ionizationPotentialKjmol": {
		"1": 547,
		"2": 1085,
		"3": 2405
	},
	"commonOxidationStates": "+3,2",
	"commonIons": "+3,2",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f7",
		"observed": "[Xe] 6s2 4f7"
	},
	"radii": {
		"atomic": 2.56,
		"ionic": 1.07,
		"covalent": 1.85,
		"atomicPm": 208,
		"2+ion": 131,
		"3+ion": 108.7
	},
	"volume": 28.9,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-53-3",
	"electricalConductivity": 0.00112,
	"specificHeat": 0.18,
	"heatOfFusion": 9.21,
	"heatOfVaporization": 143.5,
	"thermalConductivity": 13.9,
	"densityInEarthCrust": 2,
	"densityInSeawater": 0.00000013,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "hard",
	"structure": "bcc: body-centered cubic",
	"uses": "alloys, color TV phosphor",
	"reactions": {
		"withAir": "vigorous, =>Eu2O3",
		"withWater": "mild, =>H2, Eu(OH)3",
		"with6mhci": "mild, =>H2, EuCl3",
		"with15mhno3": "mild, =>Eu(NO3)3"
	},
	"hydrides": "EuH2",
	"oxides": "Eu2O3",
	"chlorides": "EuCl2 EuCl3",
	"polarizability": "27.7",
	"heatAtomization": 178,
	"yearDiscovered": 1901,
	"abundance": {
		"solarSystem": -1.01188715973165,
		"earthCrust": 0.1
	},
	"cost": {
		"pure": 3600
	}
}, {
	"number": 64,
	"symbol": "Gd",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "gadolinium",
		"de": "Gadolinium",
		"fr": "gadolinium",
		"es": "gadolineo",
		"it": "gadolinio"
	},
	"mass": 157.3,
	"meltingPoint": 1313,
	"boilingPoint": 3273,
	"state": "s",
	"density": 7.9,
	"electronegativity": 1.2,
	"ionizationPotentialEv": {
		"1": 6.15,
		"2": 12.095,
		"3": 20.635
	},
	"ionizationPotentialKjmol": {
		"1": 593,
		"2": 1167,
		"3": 1991
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f8",
		"observed": "[Xe] 6s2 4f8"
	},
	"radii": {
		"atomic": 2.54,
		"ionic": 1.05,
		"covalent": 1.61,
		"atomicPm": 180,
		"3+ion": 107.8
	},
	"volume": 19.9,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-54-2",
	"electricalConductivity": 0.00736,
	"specificHeat": 0.23,
	"heatOfFusion": 10.05,
	"heatOfVaporization": 359.4,
	"thermalConductivity": 10.6,
	"densityInEarthCrust": 6.2,
	"densityInSeawater": 0.0000007,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"structure": "hcp: hexagonal close pkd",
	"uses": "alloys, CD disk",
	"reactions": {
		"withAir": "vigorous, =>Gd2O3",
		"withWater": "mild, =>H2, Gd(OH)3",
		"with6mhci": "mild, =>H2, GdCl3",
		"with15mhno3": "mild, =>Gd(NO3)3"
	},
	"hydrides": "GdH2 GdH3",
	"oxides": "Gd2O3",
	"chlorides": "GdCl3",
	"polarizability": "23.5",
	"heatAtomization": 398,
	"yearDiscovered": 1880,
	"abundance": {
		"solarSystem": -0.481486060122112,
		"earthCrust": 0.7
	},
	"cost": {
		"pure": 191
	}
}, {
	"number": 65,
	"symbol": "Tb",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "terbium",
		"de": "Terbium",
		"fr": "terbium",
		"es": "terbio",
		"it": "terbio"
	},
	"mass": 158.9,
	"meltingPoint": 1365,
	"boilingPoint": 3230,
	"state": "s",
	"density": 8.23,
	"ionizationPotentialEv": {
		"1": 5.8939,
		"2": 11.525,
		"3": 21.91
	},
	"ionizationPotentialKjmol": {
		"1": 569,
		"2": 1112,
		"3": 2114
	},
	"commonOxidationStates": "+3,4",
	"commonIons": "+3,4",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f9",
		"observed": "[Xe] 6s2 4f9"
	},
	"radii": {
		"atomic": 2.51,
		"ionic": 1.18,
		"covalent": 1.59,
		"atomicPm": 177,
		"3+ion": 106.3
	},
	"volume": 19.2,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-27-9",
	"electricalConductivity": 0.00889,
	"specificHeat": 0.18,
	"heatOfFusion": 10.8,
	"heatOfVaporization": 330.9,
	"thermalConductivity": 11.1,
	"densityInEarthCrust": 1.2,
	"densityInSeawater": 0.00000014,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "hcp: hexagonal close pkd",
	"uses": "alloys",
	"reactions": {
		"withAir": "vigorous, =>Tb4O7 (approx.)",
		"withWater": "mild, =>H2, Tb(OH)3",
		"with6mhci": "mild, =>H2, TbCl3",
		"with15mhno3": "mild, =>Tb(NO3)3"
	},
	"hydrides": "TbH2 TbH3",
	"oxides": "TbO2 Tb4O7",
	"chlorides": "TbCl3",
	"polarizability": "25.5",
	"heatAtomization": 389,
	"yearDiscovered": 1843,
	"abundance": {
		"solarSystem": -1.21968268785985,
		"earthCrust": 0
	},
	"cost": {
		"pure": 1800
	}
}, {
	"number": 66,
	"symbol": "Dy",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "dysprosium",
		"de": "Dysprosium",
		"fr": "dysprosium",
		"es": "disprosio",
		"it": "disprosio"
	},
	"mass": 162.5,
	"meltingPoint": 1412,
	"boilingPoint": 2567,
	"state": "s",
	"density": 8.55,
	"electronegativity": 1.22,
	"ionizationPotentialEv": {
		"1": 5.9389,
		"2": 11.67,
		"3": 22.802
	},
	"ionizationPotentialKjmol": {
		"1": 573,
		"2": 1126,
		"3": 2200
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f10",
		"observed": "[Xe] 6s2 4f10"
	},
	"radii": {
		"atomic": 2.49,
		"ionic": 1.03,
		"covalent": 1.59,
		"atomicPm": 178,
		"2+ion": 121,
		"3+ion": 105.2
	},
	"volume": 19,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7429-91-6",
	"electricalConductivity": 0.0108,
	"specificHeat": 0.17,
	"heatOfFusion": 11.06,
	"heatOfVaporization": 230.1,
	"thermalConductivity": 10.7,
	"densityInEarthCrust": 5.2,
	"densityInSeawater": 0.00000091,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "neutron absorber",
	"structure": "hcp: hexagonal close pkd",
	"uses": "alloys, CD disk",
	"reactions": {
		"withAir": "vigorous, =>Dy2O3",
		"withWater": "mild, =>H2, Dy(OH)3",
		"with6mhci": "mild, =>H2, DyCl3",
		"with15mhno3": "mild, =>Dy(NO3)3"
	},
	"hydrides": "DyH2 DyH3",
	"oxides": "Dy2O3",
	"chlorides": "DyCl2 DyCl3",
	"polarizability": "24.5",
	"heatAtomization": 291,
	"yearDiscovered": 1886,
	"abundance": {
		"solarSystem": -0.404283380056576,
		"earthCrust": 0.5
	},
	"cost": {
		"pure": 210
	}
}, {
	"number": 67,
	"symbol": "Ho",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "holmium",
		"de": "Holmium",
		"fr": "holmium",
		"es": "holmio",
		"it": "olmio"
	},
	"mass": 164.9,
	"meltingPoint": 1474,
	"boilingPoint": 2700,
	"state": "s",
	"density": 8.8,
	"electronegativity": 1.23,
	"ionizationPotentialEv": {
		"1": 6.0216,
		"2": 11.805,
		"3": 22.843
	},
	"ionizationPotentialKjmol": {
		"1": 581,
		"2": 1139,
		"3": 2204
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f11",
		"observed": "[Xe] 6s2 4f11"
	},
	"radii": {
		"atomic": 2.47,
		"covalent": 1.58,
		"atomicPm": 176,
		"3+ion": 104.1
	},
	"volume": 18.8,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-60-0",
	"electricalConductivity": 0.0124,
	"specificHeat": 0.16,
	"heatOfFusion": 12.2,
	"heatOfVaporization": 241,
	"thermalConductivity": 16.2,
	"densityInEarthCrust": 1.3,
	"densityInSeawater": 0.00000022,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"structure": "hcp: hexagonal close pkd",
	"uses": "alloys",
	"reactions": {
		"withAir": "vigorous, =>Ho2O3",
		"withWater": "mild, =>H2, Ho(OH)3",
		"with6mhci": "mild, =>H2, HoCl3",
		"with15mhno3": "mild, =>Ho(NO3)3"
	},
	"hydrides": "HoH2 HoH3",
	"oxides": "Ho2O3",
	"chlorides": "HoCl3",
	"polarizability": "23.6",
	"heatAtomization": 301,
	"yearDiscovered": 1879,
	"abundance": {
		"solarSystem": -1.05109823902979,
		"earthCrust": 0.1
	},
	"cost": {
		"pure": 740
	}
}, {
	"number": 68,
	"symbol": "Er",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "erbium",
		"de": "Erbium",
		"fr": "erbium",
		"es": "erbio",
		"it": "erbio"
	},
	"mass": 167.3,
	"meltingPoint": 1529,
	"boilingPoint": 2868,
	"state": "s",
	"density": 9.07,
	"electronegativity": 1.24,
	"ionizationPotentialEv": {
		"1": 6.1078,
		"2": 11.929,
		"3": 22.739
	},
	"ionizationPotentialKjmol": {
		"1": 589,
		"2": 1151,
		"3": 2194
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f12",
		"observed": "[Xe] 6s2 4f12"
	},
	"radii": {
		"atomic": 2.45,
		"ionic": 1,
		"covalent": 1.57,
		"atomicPm": 176,
		"3+ion": 103
	},
	"volume": 18.4,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-52-0",
	"electricalConductivity": 0.0117,
	"specificHeat": 0.17,
	"heatOfFusion": 19.9,
	"heatOfVaporization": 261,
	"thermalConductivity": 14.3,
	"densityInEarthCrust": 3.5,
	"densityInSeawater": 0.00000087,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"structure": "hcp: hexagonal close pkd",
	"uses": "alloys, photogr. filter",
	"reactions": {
		"withAir": "vigorous, =>Er2O3",
		"withWater": "mild, =>H2, Er(OH)3",
		"with6mhci": "mild, =>H2, ErCl3",
		"with15mhno3": "mild, =>Er(NO3)3"
	},
	"hydrides": "ErH2 ErH3",
	"oxides": "Er2O3",
	"chlorides": "ErCl3",
	"polarizability": "22.7",
	"heatAtomization": 317,
	"yearDiscovered": 1843,
	"abundance": {
		"solarSystem": -0.600672467841321,
		"earthCrust": 0.4
	},
	"cost": {
		"pure": 270
	}
}, {
	"number": 69,
	"symbol": "Tm",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "thulium",
		"de": "Thulium",
		"fr": "thulium",
		"es": "tulio",
		"it": "tulio"
	},
	"mass": 168.9,
	"meltingPoint": 1545,
	"boilingPoint": 1950,
	"state": "s",
	"density": 9.32,
	"electronegativity": 1.25,
	"ionizationPotentialEv": {
		"1": 6.1843,
		"2": 12.054,
		"3": 26.367
	},
	"ionizationPotentialKjmol": {
		"1": 597,
		"2": 1163,
		"3": 2544
	},
	"commonOxidationStates": "+3,2",
	"commonIons": "+3,2",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f13",
		"observed": "[Xe] 6s2 4f13"
	},
	"radii": {
		"atomic": 2.42,
		"ionic": 1.09,
		"covalent": 1.56,
		"atomicPm": 176,
		"2+ion": 117,
		"3+ion": 102
	},
	"volume": 18.1,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-30-4",
	"electricalConductivity": 0.015,
	"specificHeat": 0.16,
	"heatOfFusion": 16.84,
	"heatOfVaporization": 191,
	"thermalConductivity": 16.8,
	"densityInEarthCrust": 0.52,
	"densityInSeawater": 0.00000017,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "hcp: hexagonal close pkd",
	"uses": "alloys",
	"reactions": {
		"withAir": "vigorous, =>Tm2O3",
		"withWater": "mild, =>H2, Tm(OH)3",
		"with6mhci": "mild, =>H2, TmCl3",
		"with15mhno3": "mild, =>Tm(NO3)3"
	},
	"hydrides": "TmH2 TmH3",
	"oxides": "Tm2O3",
	"chlorides": "TmCl2 TmCl3",
	"polarizability": "21.8",
	"heatAtomization": 232,
	"yearDiscovered": 1879,
	"abundance": {
		"solarSystem": -1.42250820016277,
		"earthCrust": -0.3
	},
	"cost": {
		"pure": 4100
	}
}, {
	"number": 70,
	"symbol": "Yb",
	"group": "lanthanides",
	"period": 6,
	"name": {
		"en": "ytterbium",
		"de": "Ytterbium",
		"fr": "ytterbium",
		"es": "iterbio",
		"it": "itterbio"
	},
	"mass": 173.1,
	"meltingPoint": 819,
	"boilingPoint": 1196,
	"state": "s",
	"density": 6.97,
	"ionizationPotentialEv": {
		"1": 6.2542,
		"2": 12.188,
		"3": 25.03
	},
	"ionizationPotentialKjmol": {
		"1": 603,
		"2": 1176,
		"3": 2415
	},
	"commonOxidationStates": "+3,2",
	"commonIons": "+3,2",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14",
		"observed": "[Xe] 6s2 4f14"
	},
	"radii": {
		"atomic": 2.4,
		"ionic": 0.99,
		"covalent": 1.74,
		"atomicPm": 193,
		"2+ion": 116,
		"3+ion": 100.8
	},
	"volume": 24.79,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-64-4",
	"electricalConductivity": 0.0351,
	"specificHeat": 0.15,
	"heatOfFusion": 7.66,
	"heatOfVaporization": 128.9,
	"thermalConductivity": 34.9,
	"densityInEarthCrust": 3.2,
	"densityInSeawater": 0.00000082,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"characteristics": "soft",
	"structure": "fcc: face-centered cubic",
	"uses": "alloys",
	"reactions": {
		"withAir": "vigorous, =>Yb2O3",
		"withWater": "mild, =>H2, Yb(OH)3",
		"with6mhci": "mild, =>H2, YbCl3",
		"with15mhno3": "mild, =>Yb(NO3)3"
	},
	"hydrides": "YbH2 Yb2H5",
	"oxides": "Yb2O3",
	"chlorides": "YbCl2 YbCl3",
	"polarizability": "21",
	"heatAtomization": 152,
	"yearDiscovered": 1879,
	"abundance": {
		"solarSystem": -0.605723473232178,
		"earthCrust": 0.5
	},
	"cost": {
		"pure": 530
	}
}, {
	"number": 71,
	"symbol": "Lu",
	"group": "3",
	"period": 6,
	"name": {
		"en": "lutetium",
		"de": "Lutetium",
		"fr": "lutécium",
		"es": "lutecio",
		"it": "lutezio"
	},
	"mass": 175.0,
	"meltingPoint": 1663,
	"boilingPoint": 3402,
	"state": "s",
	"density": 9.84,
	"electronegativity": 1.27,
	"ionizationPotentialEv": {
		"1": 5.4259,
		"2": 13.888,
		"3": 20.957
	},
	"ionizationPotentialKjmol": {
		"1": 524,
		"2": 1340,
		"3": 2022
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d1",
		"observed": "[Xe] 6s2 4f14 5d1"
	},
	"radii": {
		"atomic": 2.25,
		"ionic": 0.98,
		"covalent": 1.56,
		"atomicPm": 174,
		"3+ion": 100.1
	},
	"volume": 17.78,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7439-94-3",
	"electricalConductivity": 0.0185,
	"specificHeat": 0.15,
	"heatOfFusion": 18.6,
	"heatOfVaporization": 355.9,
	"thermalConductivity": 16.4,
	"densityInEarthCrust": 0.8,
	"densityInSeawater": 0.00000015,
	"source": "Monazite(phosphate),bastnaesite",
	"colour": "silvery-white",
	"structure": "hcp: hexagonal close pkd",
	"uses": "alloys",
	"reactions": {
		"withAir": "vigorous, =>Lu2O3",
		"withWater": "mild, =>H2, Lu(OH)3",
		"with6mhci": "mild, =>H2, LuCl3",
		"with15mhno3": "mild, =>Lu(NO3)3"
	},
	"hydrides": "LuH2 LuH3",
	"oxides": "Lu2O3",
	"chlorides": "LuCl3",
	"polarizability": "21.9",
	"yearDiscovered": 1907,
	"abundance": {
		"solarSystem": -1.43533393574791,
		"earthCrust": -0.3
	},
	"cost": {
		"pure": 6900
	}
}, {
	"number": 72,
	"symbol": "Hf",
	"group": "4",
	"period": 6,
	"name": {
		"en": "hafnium",
		"de": "Hafnium",
		"fr": "hafnium",
		"es": "hafnio",
		"it": "hafnio"
	},
	"mass": 178.5,
	"meltingPoint": 2227,
	"boilingPoint": 4600,
	"state": "s",
	"density": 13.3,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.8251,
		"2": 14.925,
		"3": 23.32
	},
	"ionizationPotentialKjmol": {
		"1": 659,
		"2": 1440,
		"3": 2250
	},
	"electronAffinityEv": "0",
	"electronAffinityKjmol": "0",
	"commonOxidationStates": "+4",
	"commonIons": "+4",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d2",
		"observed": "[Xe] 6s2 4f14 5d2"
	},
	"radii": {
		"atomic": 2.16,
		"ionic": 0.83,
		"covalent": 1.44,
		"atomicPm": 159
	},
	"volume": 13.6,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-58-6",
	"electricalConductivity": 0.0312,
	"specificHeat": 0.14,
	"heatOfFusion": 24.06,
	"heatOfVaporization": 575,
	"thermalConductivity": 23,
	"densityInEarthCrust": 3,
	"densityInSeawater": 0.000007,
	"source": "Zircon (silicate)",
	"colour": "silvery",
	"characteristics": "hard",
	"structure": "hcp: hexagonal close pkd",
	"uses": "nuclear control rods",
	"reactions": {
		"withAir": "mild, w/ht, =>HfO2",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "passivated",
		"with6MNaOH": "none"
	},
	"hydrides": "HfH2",
	"oxides": "HfO2",
	"chlorides": "HfCl3 HfCl4",
	"polarizability": "16.2",
	"heatAtomization": 619,
	"yearDiscovered": 1923,
	"abundance": {
		"solarSystem": -0.812479279163537,
		"earthCrust": 0.4
	},
	"cost": {
		"pure": 120
	},
	"carcinogenic": "no"
}, {
	"number": 73,
	"symbol": "Ta",
	"group": "5",
	"period": 6,
	"name": {
		"en": "tantalum",
		"de": "Tantal",
		"fr": "tantale",
		"es": "tántalo",
		"it": "tantalio"
	},
	"mass": 180.9,
	"meltingPoint": 2996,
	"boilingPoint": 5425,
	"state": "s",
	"density": 16.6,
	"electronegativity": 1.5,
	"ionizationPotentialEv": {
		"1": 7.89
	},
	"ionizationPotentialKjmol": {
		"1": 761
	},
	"electronAffinityEv": "0.32",
	"electronAffinityKjmol": "31",
	"commonOxidationStates": "+5",
	"commonIons": "+5",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d3",
		"observed": "[Xe] 6s2 4f14 5d3"
	},
	"radii": {
		"atomic": 2.09,
		"ionic": 0.64,
		"covalent": 1.34,
		"atomicPm": 146,
		"3+ion": 86
	},
	"volume": 10.9,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-25-7",
	"electricalConductivity": 0.0761,
	"specificHeat": 0.14,
	"heatOfFusion": 31.6,
	"heatOfVaporization": 743,
	"thermalConductivity": 57.5,
	"densityInEarthCrust": 2,
	"densityInSeawater": 0.000002,
	"source": "Tantalite (oxide)",
	"colour": "gray",
	"characteristics": "hard, inert at <100 deg.C",
	"structure": "bcc: body-centered cubic",
	"uses": "surg. implants,heat exch.",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "TaH",
	"oxides": "TaO2 Ta2O5",
	"chlorides": "TaCl3 TaCl4 TaCl5",
	"polarizability": "13.1",
	"heatAtomization": 782,
	"yearDiscovered": 1802,
	"abundance": {
		"solarSystem": -1.68402965454308,
		"earthCrust": 0.3
	},
	"cost": {
		"pure": 120
	},
	"carcinogenic": "no"
}, {
	"number": 74,
	"symbol": "W",
	"group": "6",
	"period": 6,
	"name": {
		"en": "tungsten",
		"de": "Wolfram",
		"fr": "tungstène",
		"es": "wolframio or tungsteno",
		"it": "tungsteno"
	},
	"mass": 183.8,
	"meltingPoint": 3410,
	"boilingPoint": 5660,
	"state": "s",
	"density": 19.3,
	"electronegativity": 2.36,
	"ionizationPotentialEv": {
		"1": 7.98
	},
	"ionizationPotentialKjmol": {
		"1": 770
	},
	"electronAffinityEv": "0.86",
	"electronAffinityKjmol": "83",
	"commonOxidationStates": "+6,4",
	"commonIons": "+6,4",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d4",
		"observed": "[Xe] 6s2 4f14 5d4"
	},
	"radii": {
		"atomic": 2.02,
		"ionic": 0.6,
		"covalent": 1.3,
		"atomicPm": 139
	},
	"volume": 9.53,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-33-7",
	"electricalConductivity": 0.189,
	"specificHeat": 0.13,
	"heatOfFusion": 35.4,
	"heatOfVaporization": 824,
	"thermalConductivity": 174,
	"densityInEarthCrust": 1.25,
	"densityInSeawater": 0.0001,
	"source": "Scheelite,wolframite(oxide)",
	"colour": "bluish-gray",
	"characteristics": "highest melting metal",
	"structure": "bcc: body-centered cubic",
	"uses": "WC drill bits, bulb wire",
	"reactions": {
		"withAir": "none, w/ht =>WO3",
		"withWater": "none",
		"with6mhci": "none"
	},
	"oxides": "WO2 WO3",
	"chlorides": "WClx [x=2-6]",
	"polarizability": "11.1",
	"heatAtomization": 849,
	"yearDiscovered": 1783,
	"abundance": {
		"solarSystem": -0.876148359032914,
		"earthCrust": 0.2
	},
	"cost": {
		"pure": 11
	},
	"carcinogenic": "no"
}, {
	"number": 75,
	"symbol": "Re",
	"group": "7",
	"period": 6,
	"name": {
		"en": "rhenium",
		"de": "Rhenium",
		"fr": "rhénium",
		"es": "renio",
		"it": "renio"
	},
	"mass": 186.2,
	"meltingPoint": 3180,
	"boilingPoint": 5600,
	"state": "s",
	"density": 21,
	"electronegativity": 1.9,
	"ionizationPotentialEv": {
		"1": 7.88
	},
	"ionizationPotentialKjmol": {
		"1": 760
	},
	"electronAffinityEv": "0.15",
	"electronAffinityKjmol": "14",
	"commonOxidationStates": "+7,4,6",
	"commonIons": "+7,4,6",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d5",
		"observed": "[Xe] 6s2 4f14 5d5"
	},
	"radii": {
		"atomic": 1.97,
		"ionic": 0.53,
		"covalent": 1.28,
		"atomicPm": 137
	},
	"volume": 8.85,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-15-5",
	"electricalConductivity": 0.0542,
	"specificHeat": 0.13,
	"heatOfFusion": 33.2,
	"heatOfVaporization": 715,
	"thermalConductivity": 47.9,
	"densityInEarthCrust": 0.0007,
	"densityInSeawater": 0.000004,
	"source": "Molybdenite (sulfide)",
	"colour": "silvery-white",
	"characteristics": "high melting point",
	"structure": "hcp: hexagonal close pkd",
	"uses": "catalysts, thermocouples",
	"reactions": {
		"withAir": "mild, w/ht =>Re2O7",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>HReO4"
	},
	"hydrides": "none",
	"oxides": "Re2O5 ReO3 Re2O7",
	"chlorides": "Re3Cl9 ReCl4 ReCl5 ReCl6",
	"polarizability": "9.7",
	"heatAtomization": 770,
	"yearDiscovered": 1925,
	"abundance": {
		"solarSystem": -1.28650945690606,
		"earthCrust": -2.3
	},
	"cost": {
		"pure": 540
	}
}, {
	"number": 76,
	"symbol": "Os",
	"group": "8",
	"period": 6,
	"name": {
		"en": "osmium",
		"de": "Osmium",
		"fr": "osmium",
		"es": "osmio",
		"it": "osmio"
	},
	"mass": 190.2,
	"meltingPoint": 3045,
	"boilingPoint": 5030,
	"state": "s",
	"density": 22.6,
	"electronegativity": 2.2,
	"ionizationPotentialEv": {
		"1": 8.7
	},
	"ionizationPotentialKjmol": {
		"1": 839
	},
	"electronAffinityEv": "1.1",
	"electronAffinityKjmol": "106",
	"commonOxidationStates": "+4,6,8",
	"commonIons": "+4,6,8",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d6",
		"observed": "[Xe] 6s2 4f14 5d6"
	},
	"radii": {
		"atomic": 1.92,
		"ionic": 0.63,
		"covalent": 1.26,
		"atomicPm": 135
	},
	"volume": 8.49,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-04-02",
	"electricalConductivity": 0.109,
	"specificHeat": 0.13,
	"heatOfFusion": 31.8,
	"heatOfVaporization": 746,
	"thermalConductivity": 87.6,
	"densityInEarthCrust": 0.0015,
	"source": "nickel ores (sulfides)",
	"colour": "bluish-white",
	"characteristics": "hard, dense, inert <100 C",
	"structure": "hcp: hexagonal close pkd",
	"hardness": 7,
	"uses": "hard alloys,phono needles",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>OsO4"
	},
	"hydrides": "none",
	"oxides": "OsO2 OsO4",
	"chlorides": "OsCl3 OsCl4 OsCl5",
	"polarizability": "8.5",
	"heatAtomization": 791,
	"yearDiscovered": 1804,
	"abundance": {
		"solarSystem": -0.170696227168975,
		"earthCrust": -2.8
	},
	"cost": {
		"pure": 7700
	}
}, {
	"number": 77,
	"symbol": "Ir",
	"group": "9",
	"period": 6,
	"name": {
		"en": "iridium",
		"de": "Iridium",
		"fr": "iridium",
		"es": "iridio",
		"it": "iridio"
	},
	"mass": 192.2,
	"meltingPoint": 2410,
	"boilingPoint": 4130,
	"state": "s",
	"density": 22.4,
	"electronegativity": 2.2,
	"ionizationPotentialEv": {
		"1": 9.1
	},
	"ionizationPotentialKjmol": {
		"1": 878
	},
	"electronAffinityEv": "1.57",
	"electronAffinityKjmol": "151",
	"commonOxidationStates": "+4,3,6",
	"commonIons": "+4,3,6",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d7",
		"observed": "[Xe] 6s2 4f14 5d7"
	},
	"radii": {
		"atomic": 1.87,
		"ionic": 0.63,
		"covalent": 1.27,
		"atomicPm": 136,
		"3+ion": 82
	},
	"volume": 8.54,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7439-88-5",
	"electricalConductivity": 0.197,
	"specificHeat": 0.13,
	"heatOfFusion": 26.1,
	"heatOfVaporization": 604,
	"thermalConductivity": 147,
	"densityInEarthCrust": 0.001,
	"source": "nickel ores (sulfides)",
	"colour": "white",
	"characteristics": "hard, densest el, inert",
	"structure": "fcc: face-centered cubic",
	"hardness": 6.25,
	"uses": "Pt/Ir sparkplugs",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "IrO2",
	"chlorides": "IrCl3",
	"polarizability": "7.6",
	"heatAtomization": 665,
	"yearDiscovered": 1804,
	"abundance": {
		"solarSystem": -0.17979854051436,
		"earthCrust": -3
	},
	"cost": {
		"pure": 4200
	}
}, {
	"number": 78,
	"symbol": "Pt",
	"group": "10",
	"period": 6,
	"name": {
		"en": "platinum",
		"de": "Platin",
		"fr": "platine",
		"es": "platino",
		"it": "platino"
	},
	"mass": 195.1,
	"meltingPoint": 1772,
	"boilingPoint": 3827,
	"state": "s",
	"density": 21.4,
	"electronegativity": 2.28,
	"ionizationPotentialEv": {
		"1": 9,
		"2": 18.563
	},
	"ionizationPotentialKjmol": {
		"1": 868,
		"2": 1791
	},
	"electronAffinityEv": "2.13",
	"electronAffinityKjmol": "206",
	"commonOxidationStates": "+4,2",
	"commonIons": "+4,2",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d8",
		"observed": "[Xe] 6s2 4f14 5d8"
	},
	"radii": {
		"atomic": 1.83,
		"ionic": 0.63,
		"covalent": 1.3,
		"atomicPm": 139,
		"2+ion": 94
	},
	"volume": 9.1,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-06-4",
	"electricalConductivity": 0.0966,
	"specificHeat": 0.13,
	"heatOfFusion": 19.6,
	"heatOfVaporization": 510,
	"thermalConductivity": 71.6,
	"densityInEarthCrust": 0.005,
	"source": "nickel ores (sulfides)",
	"colour": "silvery-white",
	"characteristics": "Inert, ductile",
	"structure": "fcc: face-centered cubic",
	"hardness": 4.3,
	"uses": "jewelry, catalysts",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "PtO2",
	"chlorides": "PtCl2 PtCl4",
	"polarizability": "6.5",
	"heatAtomization": 565,
	"yearDiscovered": 1748,
	"abundance": {
		"solarSystem": 0.127104798364808,
		"earthCrust": -2.3
	},
	"cost": {
		"pure": 4700,
		"bulk": 1200
	},
	"carcinogenic": "no"
}, {
	"number": 79,
	"symbol": "Au",
	"group": "11",
	"period": 6,
	"name": {
		"en": "gold",
		"de": "Gold",
		"fr": "or",
		"es": "oro",
		"it": "oro"
	},
	"mass": 197.0,
	"meltingPoint": 1064,
	"boilingPoint": 3080,
	"state": "s",
	"density": 19.3,
	"electronegativity": 2.54,
	"ionizationPotentialEv": {
		"1": 9.2257,
		"2": 20.521
	},
	"ionizationPotentialKjmol": {
		"1": 890,
		"2": 1980
	},
	"electronAffinityEv": "2.31",
	"electronAffinityKjmol": "223",
	"commonOxidationStates": "+3,1",
	"commonIons": "+3,1",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d9",
		"observed": "[Xe] 6s2 4f14 5d9"
	},
	"radii": {
		"atomic": 1.79,
		"ionic": 0.85,
		"covalent": 1.34,
		"atomicPm": 144,
		"1+ion": 151,
		"3+ion": 99
	},
	"volume": 10.2,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-57-5",
	"electricalConductivity": 0.452,
	"specificHeat": 0.128,
	"heatOfFusion": 12.55,
	"heatOfVaporization": 334.4,
	"thermalConductivity": 317,
	"densityInEarthCrust": 0.004,
	"densityInSeawater": 0.004,
	"percentHumanBody": 0.00001,
	"source": "Gold, gold bearing rock (misc)",
	"colour": "golden yellow",
	"characteristics": "Inert, ductile malleable",
	"structure": "fcc: face-centered cubic",
	"hardness": 2.75,
	"uses": "jewelry, electronics",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "Au2O3",
	"chlorides": "AuCl AuCl3",
	"polarizability": "6.1",
	"heatAtomization": 366,
	"abundance": {
		"solarSystem": -0.728158393463501,
		"earthCrust": -2.4
	},
	"cost": {
		"pure": 4400,
		"bulk": 1200
	},
	"carcinogenic": "no"
}, {
	"number": 80,
	"symbol": "Hg",
	"group": "12",
	"period": 6,
	"name": {
		"en": "mercury",
		"de": "Quecksilber",
		"fr": "mercure",
		"es": "mercurio",
		"it": "mercurio"
	},
	"mass": 200.6,
	"meltingPoint": -38.9,
	"boilingPoint": 357,
	"state": "l",
	"density": 13.5,
	"electronegativity": 2,
	"ionizationPotentialEv": {
		"1": 10.4375,
		"2": 18.759,
		"3": 34.202
	},
	"ionizationPotentialKjmol": {
		"1": 1007,
		"2": 1810,
		"3": 3300
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "+2,1",
	"commonIons": "+2,1",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d10",
		"observed": "[Xe] 6s2 4f14 5d10"
	},
	"radii": {
		"atomic": 1.76,
		"ionic": 1.02,
		"covalent": 1.49,
		"atomicPm": 151,
		"1+ion": 133,
		"2+ion": 116
	},
	"volume": 14.82,
	"crystalStructure": "rhombohedral",
	"casRegistryNo": "7439-97-6",
	"electricalConductivity": 0.0104,
	"specificHeat": 0.139,
	"heatOfFusion": 2.295,
	"heatOfVaporization": 59.229,
	"thermalConductivity": 8.34,
	"densityInEarthCrust": 0.085,
	"densityInSeawater": 0.00003,
	"source": "Cinnabar (sulfide)",
	"colour": "silvery-white",
	"characteristics": "liquid at r.t., volatile",
	"structure": "rhombohedral",
	"uses": "amalgams, Cl2, NaOH prod.",
	"reactions": {
		"withAir": "none, w/ht (350C) =>HgO",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>NOx, Hg(NO3)2",
		"with6MNaOH": "none"
	},
	"hydrides": "none",
	"oxides": "HgO",
	"chlorides": "Hg2Cl2 HgCl2",
	"polarizability": "5.4",
	"heatAtomization": 61,
	"abundance": {
		"solarSystem": -0.468521082957745,
		"earthCrust": -1.1
	},
	"cost": {
		"pure": 5,
		"bulk": 0.51
	},
	"toxic": "yes",
	"carcinogenic": "no"
}, {
	"number": 81,
	"symbol": "Tl",
	"group": "13",
	"period": 6,
	"name": {
		"en": "thallium",
		"de": "Thallium",
		"fr": "thallium",
		"es": "talio",
		"it": "tallio"
	},
	"mass": 204.4,
	"meltingPoint": 303,
	"boilingPoint": 1457,
	"state": "s",
	"density": 11.9,
	"electronegativity": 1.62,
	"ionizationPotentialEv": {
		"1": 6.1083,
		"2": 20.428,
		"3": 29.829
	},
	"ionizationPotentialKjmol": {
		"1": 589,
		"2": 1971,
		"3": 2878
	},
	"electronAffinityEv": "0.2",
	"electronAffinityKjmol": "19",
	"commonOxidationStates": "+1,3",
	"commonIons": "+1,3",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d10 6p1",
		"observed": "[Xe] 6s2 4f14 5d10 6p1"
	},
	"radii": {
		"atomic": 2.08,
		"ionic": 1.59,
		"covalent": 1.48,
		"atomicPm": 170,
		"1+ion": 164,
		"3+ion": 102.5
	},
	"volume": 17.2,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-28-0",
	"electricalConductivity": 0.0617,
	"specificHeat": 0.13,
	"heatOfFusion": 4.142,
	"heatOfVaporization": 164.1,
	"thermalConductivity": 46.1,
	"densityInEarthCrust": 0.85,
	"densityInSeawater": 0.000019,
	"source": "Zn/Pb smelting by-product",
	"colour": "bluish-gray",
	"characteristics": "soft",
	"structure": "hcp: hexagonal close pkd",
	"hardness": 1.2,
	"uses": "IR detectors, photomult.",
	"reactions": {
		"withAir": "mild, =>Tl2O",
		"withWater": "mild, =>TlOH",
		"with15mhno3": "mild, =>TlNO3"
	},
	"oxides": "Tl2O Tl2O3",
	"chlorides": "TlCl TlCl3",
	"polarizability": "7.6",
	"heatAtomization": 182,
	"yearDiscovered": 1861,
	"abundance": {
		"solarSystem": -0.735182176990463,
		"earthCrust": -0.3
	},
	"cost": {
		"pure": 48
	},
	"carcinogenic": "no"
}, {
	"number": 82,
	"symbol": "Pb",
	"group": "14",
	"period": 6,
	"name": {
		"en": "lead",
		"de": "Blei",
		"fr": "plomb",
		"es": "plomo",
		"it": "piombo"
	},
	"mass": 207.2,
	"meltingPoint": 327.5,
	"boilingPoint": 1740,
	"state": "s",
	"density": 11.4,
	"electronegativity": 2.33,
	"ionizationPotentialEv": {
		"1": 7.4167,
		"2": 15.028,
		"3": 31.943
	},
	"ionizationPotentialKjmol": {
		"1": 716,
		"2": 1450,
		"3": 3082
	},
	"electronAffinityEv": "0.36",
	"electronAffinityKjmol": "35",
	"commonOxidationStates": "+2,4",
	"commonIons": "+2,4",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d10 6p2",
		"observed": "[Xe] 6s2 4f14 5d10 6p2"
	},
	"radii": {
		"atomic": 1.81,
		"ionic": 1.19,
		"covalent": 1.47,
		"atomicPm": 146,
		"2+ion": 133
	},
	"volume": 18.17,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7439-92-1",
	"electricalConductivity": 0.0481,
	"specificHeat": 0.13,
	"heatOfFusion": 4.799,
	"heatOfVaporization": 177.7,
	"thermalConductivity": 35.3,
	"densityInEarthCrust": 0.14,
	"densityInSeawater": 0.00003,
	"percentHumanBody": 0.00017,
	"source": "Galena (sulfide)",
	"colour": "gray",
	"characteristics": "soft",
	"structure": "fcc: face-centered cubic",
	"hardness": 1.5,
	"uses": "batteries, solder, paints",
	"reactions": {
		"withAir": "mild, w/ht, =>PbO",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>NOx, Pb(NO3)2"
	},
	"hydrides": "PbH4",
	"oxides": "PbO Pb3O4 Pb2O3 PbO2",
	"chlorides": "PbCl2 PbCl4",
	"polarizability": "6.8",
	"heatAtomization": 196,
	"abundance": {
		"solarSystem": 0.4983105537896,
		"earthCrust": 1.1
	},
	"cost": {
		"pure": 1.5,
		"bulk": 0.077
	},
	"carcinogenic": "no"
}, {
	"number": 83,
	"symbol": "Bi",
	"group": "15",
	"period": 6,
	"name": {
		"en": "bismuth",
		"de": "Wismut",
		"fr": "bismuth",
		"es": "bismuto",
		"it": "bismuto"
	},
	"mass": 209.0,
	"meltingPoint": 271,
	"boilingPoint": 1560,
	"state": "s",
	"density": 9.75,
	"electronegativity": 2.02,
	"ionizationPotentialEv": {
		"1": 7.289,
		"2": 16.687,
		"3": 25.559
	},
	"ionizationPotentialKjmol": {
		"1": 703,
		"2": 1610,
		"3": 2466
	},
	"electronAffinityEv": "0.95",
	"electronAffinityKjmol": "92",
	"commonOxidationStates": "+3,5",
	"commonIons": "+3,5",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d10 6p3",
		"observed": "[Xe] 6s2 4f14 5d10 6p3"
	},
	"radii": {
		"atomic": 1.63,
		"ionic": 1.03,
		"covalent": 1.46,
		"atomicPm": 150,
		"3+ion": 117
	},
	"volume": 21.3,
	"crystalStructure": "rhombohedral",
	"casRegistryNo": "7440-69-9",
	"electricalConductivity": 0.00867,
	"specificHeat": 0.12,
	"heatOfFusion": 11.3,
	"heatOfVaporization": 104.8,
	"thermalConductivity": 7.87,
	"densityInEarthCrust": 0.0085,
	"densityInSeawater": 0.00002,
	"source": "Pb smelting by-product",
	"colour": "white",
	"characteristics": "brittle",
	"structure": "rhombohedral",
	"hardness": 2.5,
	"uses": "antacid, fusible alloy",
	"reactions": {
		"withAir": "mild, w/ht =>Bi2O3",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "mild, =>Bi(NO3)3, NOx"
	},
	"hydrides": "BiH3",
	"oxides": "Bi2O3",
	"chlorides": "BiCl3 BiCl4",
	"polarizability": "7.4",
	"heatAtomization": 207,
	"yearDiscovered": 1600,
	"abundance": {
		"solarSystem": -0.84163750790475,
		"earthCrust": -0.8
	},
	"cost": {
		"pure": 11
	}
}, {
	"number": 84,
	"symbol": "Po",
	"group": "16",
	"period": 6,
	"name": {
		"en": "polonium",
		"de": "Polonium",
		"fr": "polonium",
		"es": "polonio",
		"it": "polonio"
	},
	"mass": 209,
	"meltingPoint": 254,
	"boilingPoint": 962,
	"state": "s",
	"density": 9.32,
	"electronegativity": 2,
	"ionizationPotentialEv": {
		"1": 8.4167
	},
	"ionizationPotentialKjmol": {
		"1": 812
	},
	"electronAffinityEv": "1.9",
	"electronAffinityKjmol": "183",
	"commonOxidationStates": "+4,2",
	"commonIons": "+4,2",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d10 6p4",
		"observed": "[Xe] 6s2 4f14 5d10 6p4"
	},
	"radii": {
		"atomic": 1.53,
		"covalent": 1.46,
		"atomicPm": 168
	},
	"volume": 22.23,
	"crystalStructure": "monoclinic",
	"casRegistryNo": "7440-08-6",
	"electricalConductivity": 0.0219,
	"specificHeat": 0.12,
	"thermalConductivity": 20,
	"densityInEarthCrust": 0.0000000002,
	"densityInSeawater": 0.000000000000014,
	"source": "pitchblende, synthetic",
	"colour": "blue glow",
	"characteristics": "Radioactive; 2 forms",
	"structure": "special: simple cubic",
	"uses": "thermoelectric power",
	"reactions": {
		"withAir": "mild, =>PoO2",
		"withWater": "none",
		"with6mhci": "mild, =>PoCl2"
	},
	"oxides": "PoO PoO2",
	"chlorides": "PoCl2 PoCl4",
	"polarizability": "6.8",
	"heatAtomization": 144,
	"yearDiscovered": 1898,
	"abundance": {
		"earthCrust": -9.7
	},
	"cost": {}
}, {
	"number": 85,
	"symbol": "At",
	"group": "17",
	"period": 6,
	"name": {
		"en": "astatine",
		"de": "Astat",
		"fr": "astate",
		"es": "astato",
		"it": "astato"
	},
	"mass": 210,
	"meltingPoint": 302,
	"boilingPoint": 337,
	"state": "s",
	"electronegativity": 2.2,
	"ionizationPotentialEv": {
		"1": 9.5
	},
	"ionizationPotentialKjmol": {
		"1": 917
	},
	"electronAffinityEv": "2.8",
	"electronAffinityKjmol": "270",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d10 6p5",
		"observed": "[Xe] 6s2 4f14 5d10 6p5"
	},
	"radii": {
		"atomic": 1.43,
		"covalent": 1.45
	},
	"casRegistryNo": "7440-68-8",
	"thermalConductivity": 1.7,
	"source": "Synthetic(Th or U decay)",
	"characteristics": "Radioactive",
	"reactions": {},
	"hydrides": "HAt",
	"polarizability": "6",
	"heatAtomization": 92,
	"yearDiscovered": 1940,
	"abundance": {},
	"cost": {}
}, {
	"number": 86,
	"symbol": "Rn",
	"group": "18",
	"period": 6,
	"name": {
		"en": "radon",
		"de": "Radon",
		"fr": "radon",
		"es": "radón",
		"it": "radon"
	},
	"mass": 222,
	"meltingPoint": -71,
	"boilingPoint": -61.8,
	"state": "g",
	"density": 0.00973,
	"ionizationPotentialEv": {
		"1": 10.7485
	},
	"ionizationPotentialKjmol": {
		"1": 1037
	},
	"electronAffinityEv": "<0",
	"electronAffinityKjmol": "<0",
	"commonOxidationStates": "0",
	"electronConfiguration": {
		"predicted": "[Xe] 6s2 4f14 5d10 6p6",
		"observed": "[Xe] 6s2 4f14 5d10 6p6"
	},
	"radii": {
		"atomic": 1.34
	},
	"volume": 50.5,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "10043-92-2",
	"specificHeat": 0.09,
	"heatOfFusion": 2.89,
	"heatOfVaporization": 16.4,
	"thermalConductivity": 0.00364,
	"densityInEarthCrust": 0.0000000000004,
	"densityInSeawater": 0.0000000000000006,
	"source": "Synthetic(Ra-226 decay)",
	"colour": "colorless",
	"characteristics": "Radioactive,air pollutant",
	"uses": "earthquake prediction",
	"reactions": {
		"withAir": "none",
		"withWater": "none",
		"with6mhci": "none",
		"with15mhno3": "none",
		"with6MNaOH": "none"
	},
	"polarizability": "5.3",
	"heatAtomization": 0,
	"yearDiscovered": 1900,
	"abundance": {
		"earthCrust": -12.4
	},
	"cost": {}
}, {
	"number": 87,
	"symbol": "Fr",
	"group": "1",
	"period": 7,
	"name": {
		"en": "francium",
		"de": "Franzium",
		"fr": "francium",
		"es": "francio",
		"it": "francio"
	},
	"mass": 223,
	"meltingPoint": 27,
	"boilingPoint": 677,
	"state": "s",
	"electronegativity": 0.7,
	"ionizationPotentialEv": {
		"1": 3.94
	},
	"ionizationPotentialKjmol": {
		"1": 380
	},
	"electronAffinityEv": "0.46",
	"electronAffinityKjmol": "44",
	"commonOxidationStates": "+1",
	"commonIons": "+1",
	"electronConfiguration": {
		"predicted": "[Rn] 7s1",
		"observed": "[Rn] 7s1"
	},
	"radii": {
		"1+ion": 194
	},
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-73-5",
	"electricalConductivity": 0.03,
	"thermalConductivity": 15,
	"source": "Synthetic(U-235 decay)",
	"characteristics": "Radioactive",
	"reactions": {
		"withWater": "vigorous, =>H2, FrOH",
		"with6mhci": "vigorous, =>H2, FrCl",
		"with6MNaOH": "vigorous, =>H2, FrOH"
	},
	"polarizability": "48.7",
	"heatAtomization": 73,
	"yearDiscovered": 1939,
	"abundance": {},
	"cost": {}
}, {
	"number": 88,
	"symbol": "Ra",
	"group": "2",
	"period": 7,
	"name": {
		"en": "radium",
		"de": "Radium",
		"fr": "radium",
		"es": "radio",
		"it": "radio"
	},
	"mass": 226,
	"meltingPoint": 700,
	"boilingPoint": 1140,
	"state": "s",
	"density": 5,
	"electronegativity": 0.9,
	"ionizationPotentialEv": {
		"1": 5.2789,
		"2": 10.148
	},
	"ionizationPotentialKjmol": {
		"1": 509,
		"2": 979
	},
	"commonOxidationStates": "+2",
	"commonIons": "+2",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2",
		"observed": "[Rn] 7s2"
	},
	"radii": {
		"ionic": 1.62,
		"2+ion": 162
	},
	"volume": 45.2,
	"crystalStructure": "cubic: body centered",
	"casRegistryNo": "7440-14-4",
	"specificHeat": 0.12,
	"thermalConductivity": 18.6,
	"densityInEarthCrust": 0.0000009,
	"densityInSeawater": 0.000000000089,
	"source": "pitchblende(U-238 decay)",
	"colour": "white",
	"characteristics": "Radioactive, luminescent",
	"structure": "bcc: body-centered cubic",
	"uses": "neutron source",
	"reactions": {
		"withAir": "vigorous, =>RaO2, Ra3N2"
	},
	"chlorides": "RaCl2",
	"polarizability": "38.3",
	"heatAtomization": 159,
	"yearDiscovered": 1898,
	"abundance": {
		"earthCrust": -6
	},
	"cost": {}
}, {
	"number": 89,
	"symbol": "Ac",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "actinium",
		"de": "Aktinium",
		"fr": "actinium",
		"es": "actinio",
		"it": "attinio"
	},
	"mass": 227,
	"meltingPoint": 1050,
	"boilingPoint": 3200,
	"state": "s",
	"density": 10.1,
	"electronegativity": 1.1,
	"ionizationPotentialEv": {
		"1": 5.17,
		"2": 12.126
	},
	"ionizationPotentialKjmol": {
		"1": 499,
		"2": 1170
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f1",
		"observed": "[Rn] 7s2 5f1"
	},
	"radii": {
		"3+ion": 126
	},
	"volume": 22.54,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-34-8",
	"thermalConductivity": 12,
	"densityInEarthCrust": 0.00000000055,
	"source": "Synthetic(U-235 decay)",
	"colour": "silvery-white",
	"characteristics": "Radioactive",
	"structure": "fcc: face-centered cubic",
	"uses": "thermoelectric power",
	"reactions": {
		"withAir": "mild, w/ht, =>Ac2O3",
		"withWater": "mild, =>H2, Ac(OH)3",
		"with6mhci": "mild, =>H2, AcCl3",
		"with15mhno3": "mild, =>Ac(NO3)3"
	},
	"hydrides": "AcH2",
	"oxides": "Ac2O3",
	"chlorides": "AcCl3",
	"polarizability": "32.1",
	"heatAtomization": 385,
	"yearDiscovered": 1899,
	"abundance": {
		"earthCrust": -9.3
	},
	"cost": {}
}, {
	"number": 90,
	"symbol": "Th",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "thorium",
		"de": "Thorium",
		"fr": "thorium",
		"es": "torio",
		"it": "torio"
	},
	"mass": 232.0,
	"meltingPoint": 1750,
	"boilingPoint": 4790,
	"state": "s",
	"density": 11.7,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.08,
		"2": 11.504,
		"3": 20.003
	},
	"ionizationPotentialKjmol": {
		"1": 587,
		"2": 1110,
		"3": 1930
	},
	"commonOxidationStates": "+4",
	"commonIons": "+4",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f2",
		"observed": "[Rn] 7s2 5f2"
	},
	"radii": {
		"ionic": 1.05,
		"covalent": 1.65,
		"atomicPm": 179
	},
	"volume": 19.9,
	"crystalStructure": "cubic: face centered",
	"casRegistryNo": "7440-29-1",
	"electricalConductivity": 0.0653,
	"specificHeat": 0.12,
	"heatOfFusion": 16.1,
	"heatOfVaporization": 514.4,
	"thermalConductivity": 54,
	"densityInEarthCrust": 9.6,
	"densityInSeawater": 0.000001,
	"source": "Monazite(phosphate),U extractn",
	"colour": "white",
	"characteristics": "Radioactive",
	"structure": "fcc: face-centered cubic",
	"uses": "gas mantles (ThO2)",
	"reactions": {
		"withAir": "mild, w/ht ignites =>ThO2",
		"withWater": "none",
		"with6mhci": "mild",
		"with15mhno3": "passivated",
		"with6MNaOH": "none"
	},
	"hydrides": "ThH2 Th4H15",
	"oxides": "ThO2",
	"chlorides": "ThCl4",
	"polarizability": "32.1",
	"heatAtomization": 576,
	"yearDiscovered": 1829,
	"abundance": {
		"solarSystem": -1.47495519296315,
		"earthCrust": 1
	},
	"cost": {}
}, {
	"number": 91,
	"symbol": "Pa",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "protactinium",
		"de": "Protaktinium",
		"fr": "protactinium",
		"es": "protactinio",
		"it": "protoattinio"
	},
	"mass": 231.0,
	"meltingPoint": 1570,
	"boilingPoint": 4000,
	"state": "s",
	"density": 15.4,
	"electronegativity": 1.5,
	"ionizationPotentialEv": {
		"1": 5.89
	},
	"ionizationPotentialKjmol": {
		"1": 568
	},
	"commonOxidationStates": "+5,4",
	"commonIons": "+5,4",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f3",
		"observed": "[Rn] 7s2 5f3"
	},
	"radii": {
		"atomicPm": 163,
		"3+ion": 118
	},
	"volume": 15,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7440-13-3",
	"electricalConductivity": 0.0529,
	"specificHeat": 0.12,
	"heatOfFusion": 12.3,
	"thermalConductivity": 47,
	"densityInEarthCrust": 0.0000014,
	"densityInSeawater": 0.00000000005,
	"source": "Synthetic(U-235 decay)",
	"colour": "silvery",
	"characteristics": "Radioactive",
	"structure": "fcc: face-centered cubic",
	"reactions": {
		"withWater": "none",
		"with6MNaOH": "none"
	},
	"hydrides": "PaH3",
	"oxides": "PaO PaO2 Pa2O5",
	"chlorides": "PaCl4 PaCl5",
	"polarizability": "25.4",
	"heatAtomization": 527,
	"yearDiscovered": 1917,
	"abundance": {
		"earthCrust": -5.9
	},
	"cost": {}
}, {
	"number": 92,
	"symbol": "U",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "uranium",
		"de": "Uran",
		"fr": "uranium",
		"es": "uranio",
		"it": "uranio"
	},
	"mass": 238.0,
	"meltingPoint": 1132,
	"boilingPoint": 3818,
	"state": "s",
	"density": 19,
	"electronegativity": 1.38,
	"ionizationPotentialEv": {
		"1": 6.1941
	},
	"ionizationPotentialKjmol": {
		"1": 598
	},
	"commonOxidationStates": "+6,3,4,5",
	"commonIons": "+6,3,4,5",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f4",
		"observed": "[Rn] 7s2 5f4"
	},
	"radii": {
		"ionic": 0.81,
		"covalent": 1.42,
		"atomicPm": 156,
		"3+ion": 116.5
	},
	"volume": 12.59,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7440-61-1",
	"electricalConductivity": 0.038,
	"specificHeat": 0.12,
	"heatOfFusion": 8.52,
	"heatOfVaporization": 477,
	"thermalConductivity": 27.6,
	"densityInEarthCrust": 2.7,
	"densityInSeawater": 0.0032,
	"percentHumanBody": 0.0000001,
	"source": "Uranite (oxide)",
	"colour": "silvery",
	"characteristics": "Radioactive",
	"structure": "hcp:hex cls pkd distorted",
	"uses": "nuclear reactor fuel",
	"reactions": {
		"withAir": "mild, =>U3O8",
		"withWater": "mild (powder)",
		"with6mhci": "mild, =>H2",
		"with15mhno3": "passivated",
		"with6MNaOH": "none"
	},
	"hydrides": "UH3",
	"oxides": "UO UO2 U2O5 U3O8 UO3",
	"chlorides": "UCl3 UCl4 UCl5 UCl6",
	"polarizability": "27.4",
	"heatAtomization": 490,
	"yearDiscovered": 1789,
	"abundance": {
		"solarSystem": -2.04575749056067,
		"earthCrust": 0.4
	},
	"cost": {},
	"carcinogenic": "no"
}, {
	"number": 93,
	"symbol": "Np",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "neptunium",
		"de": "Neptunium",
		"fr": "neptunium",
		"es": "neptunio",
		"it": "nettunio"
	},
	"mass": 237,
	"meltingPoint": 640,
	"boilingPoint": 3900,
	"state": "s",
	"density": 20.2,
	"electronegativity": 1.36,
	"ionizationPotentialEv": {
		"1": 6.2657
	},
	"ionizationPotentialKjmol": {
		"1": 605
	},
	"commonOxidationStates": "+5,3,4,6",
	"commonIons": "+5,3,4,6",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f5",
		"observed": "[Rn] 7s2 5f5"
	},
	"radii": {
		"atomicPm": 155,
		"2+ion": 124,
		"3+ion": 115
	},
	"volume": 11.62,
	"crystalStructure": "orthorhombic",
	"casRegistryNo": "7439-99-8",
	"electricalConductivity": 0.00822,
	"specificHeat": 0.12,
	"heatOfFusion": 5.19,
	"thermalConductivity": 6.3,
	"source": "Synthetic",
	"colour": "silvery",
	"characteristics": "Radioactive",
	"structure": "special:complex",
	"reactions": {},
	"hydrides": "NpH2 NpH3",
	"oxides": "NpO NpO2 Np2O5",
	"chlorides": "NpCl3 NpCl4",
	"polarizability": "24.8",
	"yearDiscovered": 1940,
	"abundance": {},
	"cost": {}
}, {
	"number": 94,
	"symbol": "Pu",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "plutonium",
		"de": "Plutonium",
		"fr": "plutonium",
		"es": "plutonio",
		"it": "plutonio"
	},
	"mass": 244,
	"meltingPoint": 641,
	"boilingPoint": 3232,
	"state": "s",
	"density": 19.8,
	"electronegativity": 1.28,
	"ionizationPotentialEv": {
		"1": 6.06
	},
	"ionizationPotentialKjmol": {
		"1": 585
	},
	"commonOxidationStates": "+4,3,5,6",
	"commonIons": "+4,3,5,6",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f6",
		"observed": "[Rn] 7s2 5f6"
	},
	"radii": {
		"atomicPm": 159,
		"3+ion": 114
	},
	"volume": 12.32,
	"crystalStructure": "monoclinic",
	"casRegistryNo": "7440-07-5",
	"electricalConductivity": 0.00666,
	"specificHeat": 0.13,
	"heatOfFusion": 2.84,
	"heatOfVaporization": 344,
	"thermalConductivity": 6.74,
	"source": "Synthetic",
	"colour": "silvery",
	"characteristics": "Radioactive",
	"structure": "fcc: face-centered cubic",
	"uses": "weapon fuel, pacemakers",
	"reactions": {
		"with15mhno3": "passivated"
	},
	"hydrides": "PuH2 PuH3",
	"oxides": "PuO Pu2O3 PuO2",
	"chlorides": "PuCl2 PuCl3",
	"polarizability": "24.5",
	"heatAtomization": 352,
	"yearDiscovered": 1940,
	"abundance": {},
	"cost": {}
}, {
	"number": 95,
	"symbol": "Am",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "americium",
		"de": "Amerizium",
		"fr": "américium",
		"es": "americio",
		"it": "americio"
	},
	"mass": 243,
	"meltingPoint": 994,
	"boilingPoint": 2607,
	"state": "s",
	"density": 13.7,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 5.9933
	},
	"ionizationPotentialKjmol": {
		"1": 578
	},
	"commonOxidationStates": "+3,4,5,6",
	"commonIons": "+3,4,5,6",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f7",
		"observed": "[Rn] 7s2 5f7"
	},
	"radii": {
		"atomicPm": 173,
		"3+ion": 111.5
	},
	"volume": 17.86,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-35-9",
	"electricalConductivity": 0.022,
	"specificHeat": 0.11,
	"heatOfFusion": 14.4,
	"thermalConductivity": 10,
	"source": "Synthetic",
	"colour": "silvery-white",
	"characteristics": "Radioactive",
	"structure": "fcc: face-centered cubic",
	"uses": "smoke detector",
	"reactions": {},
	"hydrides": "AmH2 AmH3",
	"oxides": "AmO Am2O3 AmO2",
	"chlorides": "AmCl2 AmCl3",
	"polarizability": "23.3",
	"yearDiscovered": 1944,
	"abundance": {},
	"cost": {}
}, {
	"number": 96,
	"symbol": "Cm",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "curium",
		"de": "Kurium",
		"fr": "curium",
		"es": "curio",
		"it": "curio"
	},
	"mass": 247,
	"meltingPoint": 1340,
	"state": "s",
	"density": 13.5,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.02
	},
	"ionizationPotentialKjmol": {
		"1": 581
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f8",
		"observed": "[Rn] 7s2 5f8"
	},
	"radii": {
		"atomicPm": 174,
		"3+ion": 111
	},
	"volume": 18.28,
	"crystalStructure": "hexagonal",
	"casRegistryNo": "7440-51-9",
	"heatOfFusion": 15,
	"thermalConductivity": 10,
	"source": "Synthetic",
	"colour": "silvery-white",
	"characteristics": "Radioactive",
	"structure": "fcc: face-centered cubic",
	"uses": "thermoelec. pwr, n source",
	"reactions": {},
	"hydrides": "CmH2",
	"oxides": "CmO Cm2O3 CmO2",
	"chlorides": "CmCl3",
	"polarizability": "23",
	"yearDiscovered": 1944,
	"abundance": {},
	"cost": {}
}, {
	"number": 97,
	"symbol": "Bk",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "berkelium",
		"de": "Berkelium",
		"fr": "berkélium",
		"es": "berkelio",
		"it": "berkelio"
	},
	"mass": 247,
	"meltingPoint": 986,
	"state": "s",
	"density": 14,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.23
	},
	"ionizationPotentialKjmol": {
		"1": 601
	},
	"commonOxidationStates": "+3,4",
	"commonIons": "+3,4",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f9",
		"observed": "[Rn] 7s2 5f9"
	},
	"radii": {
		"atomicPm": 170,
		"3+ion": 110
	},
	"casRegistryNo": "7440-40-6",
	"thermalConductivity": 10,
	"source": "Synthetic",
	"colour": "silvery",
	"characteristics": "Radioactive",
	"structure": "fcc: face-centered cubic",
	"reactions": {},
	"oxides": "BkO Bk2O3 BkO2",
	"chlorides": "BkCl3",
	"polarizability": "22.7",
	"yearDiscovered": 1949,
	"abundance": {},
	"cost": {}
}, {
	"number": 98,
	"symbol": "Cf",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "californium",
		"de": "Kalifornium",
		"fr": "californium",
		"es": "californio",
		"it": "californio"
	},
	"mass": 251,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.3
	},
	"ionizationPotentialKjmol": {
		"1": 608
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f10",
		"observed": "[Rn] 7s2 5f10"
	},
	"radii": {
		"atomicPm": 186,
		"3+ion": 109
	},
	"casRegistryNo": "7440-71-3",
	"thermalConductivity": 10,
	"source": "Synthetic",
	"characteristics": "Radioactive",
	"uses": "neutron source",
	"reactions": {},
	"oxides": "CfO Cf2O3 CfO2",
	"chlorides": "CfCl3",
	"polarizability": "20.5",
	"yearDiscovered": 1949,
	"abundance": {},
	"cost": {}
}, {
	"number": 99,
	"symbol": "Es",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "einsteinium",
		"de": "Einsteinium",
		"fr": "einsteinium",
		"es": "einstenio",
		"it": "einstenio"
	},
	"mass": 252,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.42
	},
	"ionizationPotentialKjmol": {
		"1": 619
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f11",
		"observed": "[Rn] 7s2 5f11"
	},
	"radii": {
		"atomicPm": 186
	},
	"casRegistryNo": "7429-92-7",
	"thermalConductivity": 10,
	"source": "Synthetic",
	"characteristics": "Radioactive",
	"reactions": {},
	"oxides": "Es2O3",
	"polarizability": "19.7",
	"yearDiscovered": 1954,
	"abundance": {},
	"cost": {}
}, {
	"number": 100,
	"symbol": "Fm",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "fermium",
		"de": "Fermium",
		"fr": "fermium",
		"es": "fermio",
		"it": "fermio"
	},
	"mass": 257,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.5
	},
	"ionizationPotentialKjmol": {
		"1": 627
	},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f12",
		"observed": "[Rn] 7s2 5f12"
	},
	"radii": {},
	"casRegistryNo": "7440-72-4",
	"thermalConductivity": 10,
	"source": "Synthetic",
	"characteristics": "Radioactive",
	"reactions": {},
	"polarizability": "23.8",
	"yearDiscovered": 1954,
	"abundance": {},
	"cost": {}
}, {
	"number": 101,
	"symbol": "Md",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "mendelevium",
		"de": "Mendelevium",
		"fr": "mendélévium",
		"es": "mendelevio",
		"it": "mendelevio"
	},
	"mass": 258,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.58
	},
	"ionizationPotentialKjmol": {
		"1": 635
	},
	"commonOxidationStates": "+3,2",
	"commonIons": "+3,2",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f13",
		"observed": "[Rn] 7s2 5f13"
	},
	"radii": {},
	"casRegistryNo": "7440-11-1",
	"thermalConductivity": 10,
	"source": "Synthetic",
	"characteristics": "Radioactive",
	"reactions": {},
	"polarizability": "18.2",
	"yearDiscovered": 1955,
	"abundance": {},
	"cost": {}
}, {
	"number": 102,
	"symbol": "No",
	"group": "actinides",
	"period": 7,
	"name": {
		"en": "nobelium",
		"de": "Nobelium",
		"fr": "nobélium",
		"es": "nobelio",
		"it": "nobelio"
	},
	"mass": 259,
	"electronegativity": 1.3,
	"ionizationPotentialEv": {
		"1": 6.65
	},
	"ionizationPotentialKjmol": {
		"1": 642
	},
	"commonOxidationStates": "+2,3",
	"commonIons": "+2,3",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14",
		"observed": "[Rn] 7s2 5f14"
	},
	"radii": {
		"2+ion": 124
	},
	"casRegistryNo": "10028-14-5",
	"thermalConductivity": 10,
	"source": "Synthetic",
	"characteristics": "Radioactive",
	"reactions": {},
	"polarizability": "17.5",
	"yearDiscovered": 1958,
	"abundance": {},
	"cost": {}
}, {
	"number": 103,
	"symbol": "Lr",
	"group": "3",
	"period": 7,
	"name": {
		"en": "lawrencium",
		"de": "Lawrencium",
		"fr": "lawrencium",
		"es": "lawrencio",
		"it": "laurenzio"
	},
	"mass": 262,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"commonOxidationStates": "+3",
	"commonIons": "+3",
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d1",
		"observed": "[Rn] 7s2 5f14 6d1"
	},
	"radii": {},
	"casRegistryNo": "22537-19-5",
	"thermalConductivity": 10,
	"source": "Synthetic",
	"characteristics": "Radioactive,few atms made",
	"reactions": {},
	"yearDiscovered": 1961,
	"abundance": {},
	"cost": {}
}, {
	"number": 104,
	"symbol": "Rf",
	"group": "4",
	"period": 7,
	"name": {
		"en": "rutherfordium",
		"de": "Rutherfordium",
		"fr": "rutherfordium",
		"es": "ruterfordio",
		"it": "rutherfordio"
	},
	"mass": 267,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d2",
		"observed": "[Rn] 7s2 5f14 6d2"
	},
	"radii": {},
	"casRegistryNo": "53850-36-5",
	"thermalConductivity": 23,
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 105,
	"symbol": "Db",
	"group": "5",
	"period": 7,
	"name": {
		"en": "dubnium",
		"de": "Dubnium",
		"fr": "dubnium",
		"es": "dubnio",
		"it": "dubnio"
	},
	"mass": 268,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d3",
		"observed": "[Rn] 7s2 5f14 6d3"
	},
	"radii": {},
	"casRegistryNo": "53850-35-4",
	"thermalConductivity": 58,
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 106,
	"symbol": "Sg",
	"group": "6",
	"period": 7,
	"name": {
		"en": "seaborgium",
		"de": "Seaborgium",
		"fr": "seaborgium",
		"es": "seaborgio",
		"it": "seaborgio"
	},
	"mass": 271,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d4",
		"observed": "[Rn] 7s2 5f14 6d4"
	},
	"radii": {},
	"casRegistryNo": "54038-81-2",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 107,
	"symbol": "Bh",
	"group": "7",
	"period": 7,
	"name": {
		"en": "bohrium",
		"de": "Bohrium",
		"fr": "bohrium",
		"es": "bohrio",
		"it": "bohrio"
	},
	"mass": 272,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d5",
		"observed": "[Rn] 7s2 5f14 6d5"
	},
	"radii": {},
	"casRegistryNo": "54037-14-8",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 108,
	"symbol": "Hs",
	"group": "8",
	"period": 7,
	"name": {
		"en": "hassium",
		"de": "Hassium",
		"fr": "hassium",
		"es": "hassio",
		"it": "hassio"
	},
	"mass": 270,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d6",
		"observed": "[Rn] 7s2 5f14 6d6"
	},
	"radii": {},
	"casRegistryNo": "54037-57-9",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 109,
	"symbol": "Mt",
	"group": "9",
	"period": 7,
	"name": {
		"en": "meitnerium",
		"de": "Meitnerium ",
		"fr": "meitnérium",
		"es": "meitnerio",
		"it": "meitnerio"
	},
	"mass": 276,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d7",
		"observed": "[Rn] 7s2 5f14 6d7"
	},
	"radii": {},
	"casRegistryNo": "54038-01-6",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 110,
	"symbol": "Ds",
	"group": "10",
	"period": 7,
	"name": {
		"en": "darmstadtium",
		"de": "Darmstadtium",
		"fr": "darmstadtium",
		"es": "darmstadtio",
		"it": "darmstadtio"
	},
	"mass": 281,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d8",
		"observed": "[Rn] 7s2 5f14 6d8"
	},
	"radii": {},
	"casRegistryNo": "54083-77-1",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 111,
	"symbol": "Rg",
	"group": "11",
	"period": 7,
	"name": {
		"en": "roentgentium",
		"de": "Roentgentium",
		"fr": "roentgentium",
		"es": "roentgentio",
		"it": "roentgentio"
	},
	"mass": 280,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d9",
		"observed": "[Rn] 7s2 5f14 6d9"
	},
	"radii": {},
	"casRegistryNo": "54386-24-2",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 112,
	"symbol": "Cn",
	"group": "12",
	"period": 7,
	"name": {
		"en": "copernicum",
		"de": "Kopernikum",
		"fr": "copérnicum",
		"es": "copernico",
		"it": "copernico"
	},
	"mass": 285,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d10",
		"observed": "[Rn] 7s2 5f14 6d10"
	},
	"radii": {},
	"casRegistryNo": "54084-26-3",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 113,
	"symbol": "Nh",
	"group": "13",
	"period": 7,
	"name": {
		"en": "nihonium",
		"de": "Nihonium",
		"fr": "nihonium",
		"es": "nihonio",
		"it": "nihonio"
	},
	"mass": 284,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d10 7p1",
		"observed": "[Rn] 7s2 5f14 6d10 7p1"
	},
	"radii": {},
	"casRegistryNo": "54084-70-7",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 114,
	"symbol": "Fl",
	"group": "14",
	"period": 7,
	"name": {
		"en": "flerovium",
		"de": "Flerovium",
		"fr": "flerovium",
		"es": "flerovio",
		"it": "flerovio"
	},
	"mass": 289,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d10 7p2",
		"observed": "[Rn] 7s2 5f14 6d10 7p2"
	},
	"radii": {},
	"casRegistryNo": "54085-16-4",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 115,
	"symbol": "Mc",
	"group": "15",
	"period": 7,
	"name": {
		"en": "moscovium",
		"de": "Moscovium",
		"fr": "moscovium",
		"es": "moscovio",
		"it": "moscovio"
	},
	"mass": 288,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d10 7p3",
		"observed": "[Rn] 7s2 5f14 6d10 7p3"
	},
	"radii": {},
	"casRegistryNo": "54085-64-2",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 116,
	"symbol": "Lv",
	"group": "16",
	"period": 7,
	"name": {
		"en": "livermorium",
		"de": "Livermorium",
		"fr": "livermorium",
		"es": "livermorio",
		"it": "livermorio"
	},
	"mass": 293,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d10 7p4",
		"observed": "[Rn] 7s2 5f14 6d10 7p4"
	},
	"radii": {},
	"casRegistryNo": "54100-71-9",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 117,
	"symbol": "Ts",
	"group": "17",
	"period": 7,
	"name": {
		"en": "tennessine",
		"de": "Tennessine",
		"fr": "tennessine",
		"es": "tennessio",
		"it": "tennessio"
	},
	"mass": 292,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {},
	"radii": {},
	"casRegistryNo": "87658-56-8",
	"reactions": {},
	"abundance": {},
	"cost": {}
}, {
	"number": 118,
	"symbol": "Og",
	"group": "18",
	"period": 7,
	"name": {
		"en": "oganesson",
		"de": "Oganesson",
		"fr": "oganesson",
		"es": "oganessio",
		"it": "oganessio"
	},
	"mass": 294,
	"ionizationPotentialEv": {},
	"ionizationPotentialKjmol": {},
	"electronConfiguration": {
		"predicted": "[Rn] 7s2 5f14 6d10 7p6",
		"observed": "[Rn] 7s2 5f14 6d10 7p6"
	},
	"radii": {},
	"casRegistryNo": "54144-19-3",
	"reactions": {},
	"abundance": {},
	"cost": {}
}];

export var chemicalElementCategoryData = {
	"nonmetal": [1, 6, 7, 8, 15, 16, 34],
	"metalloid": [5, 14, 32, 33, 51, 52, 84],
	"halogen": [9, 17, 35, 53, 85, 117],
	"noble-gas": [2, 10, 18, 36, 54, 86, 118],
	"alkali-metal": [3, 11, 19, 37, 55, 87],
	"alkaline-earth-metal": [4, 12, 20, 38, 56, 88],
	"lanthanoid": [57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71],
	"actinoid": [89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103],
	"transition-metal": [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 72, 73, 74, 75, 76, 77, 78, 79, 80, 104, 105, 106, 107, 108, 109, 110, 111, 112],
	"post-transition-metal": [13, 31, 49, 50, 81, 82, 83, 113, 114, 115, 116]
}