class TranslationManager {
    constructor() {
        this.translations = null;
        this.enStrings = {};
    }

    loadTranslations(lang) {

        if (lang == 'en') {
            this.translations = null;
        } else {
            let xlfText = require('./locale/messages.' + lang + '.xlf');
            let xlfXml = new DOMParser().parseFromString(xlfText, 'text/xml');
            this.translations = this.parseXlf(xlfXml);
        } 
    }

    parseXlf(xlf) {
        let ret = {};

        function nsResolver(prefix) {
            let ns = {
                'xliff' : 'urn:oasis:names:tc:xliff:document:1.2'
            };
            return ns[prefix] || null;
        }

        let iterator = xlf.evaluate('/xliff:xliff/xliff:file/xliff:body/xliff:trans-unit', xlf, nsResolver, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
        for (let thisNode = iterator.iterateNext(); thisNode; thisNode = iterator.iterateNext()) {
            let tId = thisNode.getAttribute('id');
            let tText = xlf.evaluate('xliff:target', thisNode, nsResolver, XPathResult.STRING_TYPE, null).stringValue;
            ret[tId] = tText;
        }
        return ret;
    }

    t(tKey, enString) {
        if (tKey) {
            if (this.translations) {
                if (tKey in this.translations) {
                    if (!(tKey in this.enStrings)) {
                        this.enStrings[tKey] = enString; //TODO: check for clash
                    }
                    return this.translations[tKey];
                }
            } else if (tKey in this.enStrings) {
                return this.enStrings[tKey];
            }
        }
        return enString;
    }
}

var translationManager = new TranslationManager();
export default translationManager;
