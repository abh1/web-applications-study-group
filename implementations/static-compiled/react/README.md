Periodic Table implementation in react + babel + webpack

Based on https://scotch.io/tutorials/setup-a-react-environment-using-webpack-and-babel

You need to install node.js and yarn

You then have two options:

1. Run "yarn start-dev" and go to http://localhost:8080/ in your browser.  This is
   the webpack-dev-server.

2. Run "webpack".  The application is compiled into static files in the dist
   directory.  You can open index.html in your browser.

You might have to do some other stuff to make sure yarn installs all the dependencies in package.json



Debug in Visual Studio Code
---------------------------

1. Install "chrome" extension

2. Tasks > Run Task... npm run start-dev

3. Debug > Start Debugging

Now you can set a breakpoint in the JS in Visual Studio Code.
