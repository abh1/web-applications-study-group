//import * as Map from 'core-js/es6/map';

class TranslationManager {
    translations: Map<string, string>;
    enStrings: Map<string, string>;

    static XLIFF_NAMESPACE: string = 'urn:oasis:names:tc:xliff:document:1.2';

    constructor() {
        this.translations = null;
        this.enStrings = new Map<string, string>();
    }

    loadTranslations(lang: string) {

        if (lang == 'en') {
            this.translations = null;
        } else {
            let xlfText = require('./locale/messages.' + lang + '.xlf');
            let xlfXml = new DOMParser().parseFromString(xlfText, 'text/xml');
            this.translations = this.parseXlf(xlfXml);
        } 
    }


    parseXlf(xlf: Document): Map<string, string> {
        let ret: Map<string, string> = new Map<string, string>();

        let transUnits: HTMLCollectionOf<Element> = xlf.getElementsByTagNameNS(TranslationManager.XLIFF_NAMESPACE, 'trans-unit');
        for (let transUnitIndex = 0; transUnitIndex < transUnits.length; transUnitIndex++) {
            let thisNode: Element = transUnits[transUnitIndex];
            let tId = thisNode.getAttribute("id");
            let tText = thisNode.getElementsByTagNameNS(TranslationManager.XLIFF_NAMESPACE, 'target')[0].textContent;
            ret[tId] = tText;
        }
        return ret;
    }

/* IE doesn't support XPath...

    parseXlf(xlf: Document): Map<string, string> {
        let ret = new Map<string, string>();

        let iterator = xlf.evaluate('/xliff:xliff/xliff:file/xliff:body/xliff:trans-unit', xlf, this.nsResolver, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
        for (let thisNode = iterator.iterateNext(); thisNode; thisNode = iterator.iterateNext()) {
            let tId = thisNode.attributes['id'];
            let tText = xlf.evaluate('xliff:target', thisNode, this.nsResolver, XPathResult.STRING_TYPE, null).stringValue;
            ret[tId] = tText;
        }
        return ret;
    }
*/

    t(tKey: string, enString: string): string {
        if (tKey) {
            if (this.translations) {
                if (tKey in this.translations) {
                    if (!(tKey in this.enStrings)) {
                        this.enStrings[tKey] = enString; //TODO: check for clash
                    }
                    return this.translations[tKey];
                }
            } else if (tKey in this.enStrings) {
                return this.enStrings[tKey];
            }
        }
        return enString;
    }
}

var translationManager = new TranslationManager();
export default translationManager;
