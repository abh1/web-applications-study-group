import * as React from 'react';
import PeriodicTable from './PeriodicTable.tsx';

export default class App extends React.Component<any, any> {
  render() {
    return (
     <PeriodicTable />
    );
  }
}
