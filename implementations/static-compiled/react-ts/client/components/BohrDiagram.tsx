import * as React from 'react';
import ChemicalElementData from '../ChemicalElementData.ts';

interface IBohrDiagramProps {
    elementToDisplay: ChemicalElementData,
    languageCode: string
}

export default class BohrDiagram extends React.Component<IBohrDiagramProps, any> {

    constructor(props) {
        super(props);
    }


    getElectronConfiguration(elementData) : Map<number, number> {
        let ret = new Map<number, number>();
        let atomicNumber = elementData["number"];
        let electronCount = atomicNumber;

        let electronsSoFar = 0;

        if ("electronConfiguration" in elementData) {
            let configInfo = elementData["electronConfiguration"];
            if ("observed" in configInfo) {
                let observedConfig = configInfo["observed"];
                let electronConfigurationDescription = observedConfig.match(/(\[([A-Za-z]+)\])?([0-9spdfg ]*)/);
                let baseNobleGasSymbol = electronConfigurationDescription[2];
                if (baseNobleGasSymbol) {
                    let baseNobleGasElementData = ChemicalElementData.getElementByCategoryAndSymbol("noble-gas", baseNobleGasSymbol);
                    ret = this.getElectronConfiguration(baseNobleGasElementData);
                }
                if (3 in electronConfigurationDescription) {
                    // specialisation of noble gas
                    let extraSubshellDescription = electronConfigurationDescription[3];
                    let subshellPattern = /([0-9])([spdfg])([0-9]+)/g;
                    while (true) {
                        let subshellSpec = subshellPattern.exec(extraSubshellDescription);
                        if (subshellSpec === null) {
                            break;
                        }
                        let shellNumber = parseInt(subshellSpec[1]);
                        let subshellLetter = subshellSpec[2];
                        let electronCount = parseInt(subshellSpec[3]);
                        if (ret.has(shellNumber)) {
                            ret.set(shellNumber, ret.get(shellNumber) + electronCount); //TODO: show subshells somehow
                        } else {
                            ret.set(shellNumber, electronCount);
                        }
                    }
                }

            }
        }
        return ret;
    }

    
    render() {

        if (this.props.elementToDisplay) {

            let configuration: Map<number, number> = this.getElectronConfiguration(this.props.elementToDisplay);

            let electronRings = [];
            for (let i = 0; i < 7; i++) {
                electronRings.push(<circle key={i} cx="280" cy="280" r={70 + i * 30} fill="none" stroke="gray" />);
            }

            // TypeScript target=es5 breaks nice map iteration syntax:
            // for (let [electronShellNumber, filledSlots] of configuration)
            // I think because it's trying to turn this into an array index for loop.

            let electrons = [];

            for (let [electronShellNumber, filledSlots] of Array.from(configuration.entries())) {
                let displayRadius: number = 40 + electronShellNumber * 30;
                let angleIncrement: number = Math.PI * 2 / filledSlots;
                let angle: number = 0;
                // go anticlockwise from 12 o'clock.  x = -r * sin(theta), y = -r * cos(theta)
                for (let slotIndex = 0; slotIndex < filledSlots; slotIndex++) {
                    let x = 280 - 10 - displayRadius * Math.sin(angle);
                    let y = 280 - 10 - displayRadius * Math.cos(angle);

                    electrons.push(<use xlinkHref="#ec-electron" x={x} y={y} key={electronShellNumber * 100 + slotIndex} />);

                    angle += angleIncrement;
                }
            }


            return (

                <svg id="electron-configuration" width="560" height="560" viewBox="0 0 560 560">
                    <symbol id="ec-electron" x="0" y="0" width="21" height="21" >
                        <circle cx="10" cy="10" r="10" fill="#6a6af0" stroke="white" strokeWidth="3" />
                    </symbol>
                    <symbol id="ec-empty-electron-slot" x="-10" y="-10" width="21" height="21">
                        <circle cx="10" cy="10" r="10" fill="white" stroke="gray" strokeWidth="3" />
                    </symbol>
                    <g id="ec-electron-shells">
                        {electronRings}
                    </g>
                    <g id="ec-electrons">
                        {electrons}
                    </g>
                    <g id="ec-nucleus">
                        <circle cx="280" cy="280" r="50" fill="#ED6E46" />
                        <text id="ec-symbol" x="280" y="298" fontSize="40" textAnchor="middle">{this.props.elementToDisplay.symbol}</text>
                    </g>
                </svg>
            );
        } else {
            return null;
        }
    }

}
