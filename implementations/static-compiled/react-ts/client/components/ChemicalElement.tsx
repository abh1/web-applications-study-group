import * as React from 'react';
import ChemicalElementData from '../ChemicalElementData.ts';
import { IDetailDisplayer } from "./PeriodicTable.tsx";

interface IChemicalElementProps {
    info: ChemicalElementData, 
    languageCode: string,
    detailDisplayer: IDetailDisplayer
}

export default class ChemicalElement extends React.Component<IChemicalElementProps, any> {

    constructor(props: IChemicalElementProps) {
        super(props);
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    getElementTypeClassName(atomicNumber: number): string {
        return ChemicalElementData.getElementByNumber(atomicNumber).chemicalElementCategory;
    }

    render() {
        if (this.props.info) {
            let atomicNumber = this.props.info.number;
            return (
                <td className={"element " + this.props.info.chemicalElementCategory} onClick={this.handleClick}>
                    <div className="atomic-number">{atomicNumber}</div>
                    <div className="symbol">{this.props.info.symbol}</div>
                    <div className="name">{this.props.info.name[this.props.languageCode]}</div>
                </td>
            );
        } else {
            return <td>Bad!</td>
        }
    }

    handleClick() {
        this.props.detailDisplayer.displayElementDetails(this.props.info);
    }
}
