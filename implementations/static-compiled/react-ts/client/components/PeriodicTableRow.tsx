import * as React from 'react';
import ChemicalElement from './ChemicalElement.tsx';
import ChemicalElementData from '../ChemicalElementData.ts';
import { IDetailDisplayer } from "./PeriodicTable.tsx";

interface IPeriodicTableRowProps {
    languageCode: string,
    rowLength: number,
    left: number,
    leftCount: number,
    right: number,
    rightCount: number,
    period: string,
    detailDisplayer: IDetailDisplayer
}

export default class PeriodicTableRow extends React.Component<IPeriodicTableRowProps, any> {

    periodElements() {
        let languageCode = this.props.languageCode;

        let rowLength = this.props.rowLength;
        let leftmostAtomicNumber = this.props.left;
        let leftCount = this.props.leftCount;
        let rightmostAtomicNumber = this.props.right;
        let rightCount = this.props.rightCount;

        let ret = new Array(rowLength);
        for (let i = 0; i < leftCount; i++) {
            let atomicNumber = leftmostAtomicNumber + i;
            ret[i] = <ChemicalElement key={i} info={ChemicalElementData.getElementByNumber(atomicNumber)} languageCode={languageCode} detailDisplayer={this.props.detailDisplayer}/>
        }
        let fillTo = rowLength;
        if (rightmostAtomicNumber > leftmostAtomicNumber) {
            for (let i = 0; i < rightCount; i++) {
                let atomicNumber = rightmostAtomicNumber - i;
                let index = rowLength - i;
                ret[index] = <ChemicalElement key={index} info={ChemicalElementData.getElementByNumber(atomicNumber)} languageCode={languageCode} detailDisplayer={this.props.detailDisplayer}/>
            }
            fillTo = rowLength - rightCount;
        }
        for (let i = leftCount; i < fillTo; i++) {
            ret[i] = <td key={i} />
        }
        return ret;
    }

    render() {
        return (
            <tr className="period">
                <th>{this.props.period}</th>
                {this.periodElements()}
            </tr>
        );
    }
}
