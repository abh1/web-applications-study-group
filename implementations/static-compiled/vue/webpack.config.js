const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './index.html',
    filename: 'index.html',
    inject: true,
    chunksSortMode: 'dependency'
})

module.exports = {
    devtool: 'inline-source-map',
    entry: {
        'app': ['./app.js']
    },
    output: {
        path: path.resolve('dist'),
        filename: 'index_bundle.js'
    },
    module: {
        loaders: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.vue$/, loader: 'vue-loader', exclude: /node_modules/ },
            { test: /\.css$/, use: ["style-loader", "css-loader"] },
            { test: /\.xlf$/, use: 'raw-loader' }
        ]
    },
    plugins: [HtmlWebpackPluginConfig]
}