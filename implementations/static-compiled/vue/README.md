Implementation of periodic table using vue.js

Running
-------
You need node.js and npm installed.

npm install

Then either start a webpack-dev-server on localhost:8080
npm run start-dev

Or bundle static code using webpack
npm run bundle


Packages reference
------------------

npm install vue vue-loader vue-template-compiler css-loader babel-core babel-loader raw-loader webpack webpack-dev-server html-webpack-plugin --save

