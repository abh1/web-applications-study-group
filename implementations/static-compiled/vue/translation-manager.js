var translations = null;
var translationsEn = {};

export var loadTranslations = function(lang, callback) {
    
    let parseXlf = function(xlf) {
        let ret = {};

        function nsResolver(prefix) {
            let ns = {
                'xliff' : 'urn:oasis:names:tc:xliff:document:1.2'
            };
            return ns[prefix] || null;
        }

        let iterator = xlf.evaluate('/xliff:xliff/xliff:file/xliff:body/xliff:trans-unit', xlf, nsResolver, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
        for (let thisNode = iterator.iterateNext(); thisNode; thisNode = iterator.iterateNext()) {
            let tId = thisNode.getAttribute('id');
            let tText = xlf.evaluate('xliff:target', thisNode, nsResolver, XPathResult.STRING_TYPE, null).stringValue;
            ret[tId] = tText;
        }
        return ret;
    };

    if (lang == 'en') {
        translations = null;
    } else {

        let xlfText = require('./locale/messages.' + lang + '.xlf');
        let xlfXml = new DOMParser().parseFromString(xlfText, 'text/xml');
        translations = parseXlf(xlfXml);
        callback(lang);
    } 
};

export var translate = function(tKey, enString) {
    if (tKey) {
        if (translations) {
            if (tKey in translations) {
                if (!(tKey in translationsEn)) {
                    translationsEn[tKey] = enString; //TODO: check for clash
                }
                return translations[tKey];
            }
        } else if (tKey in translationsEn) {
            return translationsEn[tKey];
        }
    }
    return enString;
};
