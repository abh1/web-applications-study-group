import Vue from 'vue';
import App from './components/periodic-table.vue';

new Vue({
  el: '#app',
  render: h => h(App)
})
