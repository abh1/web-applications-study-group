var timer1; //global timer handles 
var timer2;

//function that highlights nonmetal elements 
function displayNonMetal() {
	stopFlash(); //stop any flashing that is underway
	
	//change background colour of all data elements to page colour
	elements = document.getElementsByTagName("td");
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#CCCCCC";
	}		
	
	//Return colour of all button elements to default
	 elements = document.getElementsByClassName("groupButton");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="";
	}
		
	//Change background colour of non metal elements to red and initiate flashing
	 elements = document.getElementsByClassName("nonmetal");
     changeRed(elements);
	
}

//function that changes background of elements to red
function changeRed(elements){
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="red";
	}
	//call changeGrey function after delay for flashing action
	timer1 = setTimeout('changeGrey(elements)', 500);
		
}

//function that changes background of elements to grey
function changeGrey(elements){
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#CCCCCC";
	}
	//call changeRed function after delay for flashing action
	timer2 = setTimeout('changeRed(elements)', 500);
	
}

//function that clears the timeouts to stop the repeated colour change function calls
function stopFlash(){
	clearTimeout(timer1);
	clearTimeout(timer2);
}

function displayNobleGas() {
	stopFlash();
	
	elements = document.getElementsByTagName("td");
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#CCCCCC";
	}	
				
	 elements = document.getElementsByClassName("groupButton");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="";
	}
	
	 elements = document.getElementsByClassName("noblegas");
     changeRed(elements);	
}

function displayAlkaliMetal() {
	stopFlash();
	elements = document.getElementsByTagName("td");
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#CCCCCC";
	}	
	
	 elements = document.getElementsByClassName("groupButton");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="";
	}
		
	 elements = document.getElementsByClassName("alkalimetal");
    changeRed(elements);		
}

function displayAlkalineEarth() {
	stopFlash();
	elements = document.getElementsByTagName("td");
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#CCCCCC";
	}	
		
	elements = document.getElementsByClassName("groupButton");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="";
	}
	
	 elements = document.getElementsByClassName("alkalineearth");
    changeRed(elements);
	 
}

function displaySemiMetal() {
	stopFlash();
	elements = document.getElementsByTagName("td");
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#CCCCCC";
	}	
	
	 elements = document.getElementsByClassName("groupButton");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="";
	}
		
	 elements = document.getElementsByClassName("semimetal");
    changeRed(elements);
	
}

function displayHalogen() {
	stopFlash();
	elements = document.getElementsByTagName("td");
	for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#CCCCCC";
	}	
	
	 elements = document.getElementsByClassName("groupButton");
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="";
	}
		
	 elements = document.getElementsByClassName("halogen");
    changeRed(elements);
		
}

//function that returns colour of all elements to original
function restoreDefault(){
	
	//launch windows pop up alert asking user to confirm
	if (confirm("Are you sure you would like to go back to defaults?") == true) {
		
		stopFlash();
		
		elements = document.getElementsByTagName("td");
		for (var i = 0; i < elements.length; i++) {
			elements[i].style.backgroundColor="";
		}	
		
		elements = document.getElementsByClassName("groupButton");
		for (var i = 0; i < elements.length; i++) {
			elements[i].style.backgroundColor="";
		}
	}
	else{}	
}


 