
function buildTable() {
    var rows = 10;
    var cols = 18;
    var htmlTable = document.getElementById("periodicTable");
    htmlTable.innerHTML = '';
    for (var row = 1; row <= rows; row++) {
        var tr = document.createElement('tr');
        htmlTable.appendChild(tr);
        for (var col = 1; col <= cols; col++)
        {
            var td = document.createElement('td');
            tr.appendChild(td);
        }
    }
    refreshTable();
}

function refreshTable() {
    var htmlTable = document.getElementById("periodicTable");
    
    for (i = 0; i < chemicalElementData.length; ++i)
    {
        var element = chemicalElementData[i];
        var row = element.period - 1;
        var col = element.group - 1;
        if (element.group === "lanthanides") {
            row = 8;
            col = element.number - 55;
        }
        if (element.group === "actinides") {
            row = 9;
            col = element.number - 87;
        }
        var tableCell = htmlTable.rows[row].cells[col];
        
        var template = document.importNode(document.getElementById("elementTemplate"), true);
        var elementContent = template.content ? template.content : template;
        
        elementContent.querySelector(".number").textContent = element.number;
        elementContent.querySelector(".symbol").textContent = element.symbol;
        var language = document.getElementById("languageSelect").value;
        elementContent.querySelector(".name").textContent = element.name[language];
        tableCell.classList.add(getCategory(element.number));
        tableCell.classList.add(getRtpState(element.number));
        tableCell[window.addEventListener ? 'addEventListener' : 'attachEvent']( window.addEventListener ? 'mouseover' : 'onmouseover', function() {
            this.classList.add("selected");
        }, false);
        tableCell[window.addEventListener ? 'addEventListener' : 'attachEvent']( window.addEventListener ? 'mouseout' : 'onmouseout', function() {
            this.classList.remove("selected");
        }, false);
        tableCell.innerHTML = '';
        tableCell.appendChild(elementContent);
    }
}

function getCategory(atomicNumber) {
    if (atomicNumber === 1) {
        return "hydrogen";
    }
    if (elementIsIn(chemicalElementCategoryData.nonmetal, atomicNumber)) {
        return "nonMetal";
    }
    if (elementIsIn(chemicalElementCategoryData.metalloid, atomicNumber)) {
        return "semiMetal";
    }
    if (elementIsIn(chemicalElementCategoryData.halogen, atomicNumber)) {
        return "halogen";
    }
    if (elementIsIn(chemicalElementCategoryData.nobleGas, atomicNumber)) {
        return "noble";
    }
    if (elementIsIn(chemicalElementCategoryData.alkaliMetal, atomicNumber)) {
        return "alkali";
    }
    if (elementIsIn(chemicalElementCategoryData.alkalineEarthMetal, atomicNumber)) {
        return "alkalineEarth";
    }
    if (elementIsIn(chemicalElementCategoryData.lanthanide, atomicNumber)) {
        return "lanthanide";
    }
    if (elementIsIn(chemicalElementCategoryData.actinide, atomicNumber)) {
        return "actinide";
    }
    if (elementIsIn(chemicalElementCategoryData.postTransitionMetal, atomicNumber)) {
        return "metal";
    }
    return "transition";
}

function getRtpState(atomicNumber) {
    if (elementIsIn(chemicalElementRtpStateData.liquid, atomicNumber)) {
        return "liquid";
    }
    if (elementIsIn(chemicalElementRtpStateData.gas, atomicNumber)) {
        return "gas";
    }
    return "solid";
}

function elementIsIn(array, atomicNumber) {
    return array.some(function(e) { 
        return e == atomicNumber; 
    });
}
