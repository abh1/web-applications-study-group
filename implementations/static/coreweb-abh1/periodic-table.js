var periodicTable = {};

periodicTable.getRowData = function(rowLength, leftmostAtomicNumber, leftCount, rightmostAtomicNumber, rightCount) {
    let ret = new Array(rowLength);
    for (let i = 0; i < leftCount; i++) {
        ret[i] = chemicalElementData[leftmostAtomicNumber + i - 1];
    }
    if (rightmostAtomicNumber > leftmostAtomicNumber) {
        for (let i = 0; i < rightCount; i++) {
            ret[rowLength - 1 - i] = chemicalElementData[rightmostAtomicNumber - i - 1];
        }
    }
    return ret;
}

periodicTable.findElementCategory = function(atomicNumber) {
    for (let t in chemicalElementCategoryData) {
        let numbers = chemicalElementCategoryData[t];
        if (numbers.indexOf(atomicNumber) >= 0) {
            return t;
        }
    }
    return 'unknown';
}


periodicTable.handleClickElement = function(e) {
    let atomicNumber = e.target.parentElement.dataset.atomicNumber; //TODO: this is a really flaky way
    let elementData = chemicalElementData[atomicNumber - 1];
    periodicTable.showElement(elementData);
}


periodicTable.initElectronConfigurationDisplay = function() {
    let electronShellsGroupElement = document.getElementById("ec-electron-shells");
    let shells = electronShellsGroupElement.children;
    if (shells.length == 1) {
        let firstShellRadius = parseInt(shells[0].getAttribute("r"));
        for (let i = 1; i < 7; i++) {
            let newShell = shells[0].cloneNode();
            newShell.setAttribute("r", firstShellRadius + i * 30);
            electronShellsGroupElement.appendChild(newShell);
        }
        periodicTable.showElement(chemicalElementData[91]);
    }
}


periodicTable.showElement = function(elementData) {
    let electronGroup = document.getElementById("ec-electrons");
    electronGroup.innerHTML = "";

    let configuration = periodicTable.getElectronConfiguration(elementData);

    for (electronShellNumber in configuration) {
        let filledSlots = configuration[electronShellNumber];
        let displayRadius = 40 + electronShellNumber * 30;
        let angleIncrement = Math.PI * 2 / filledSlots;
        let angle = 0;
        //go anticlockwise from 12 o'clock.  x = -r * sin(theta), y = -r * cos(theta)
        for (let slotIndex = 0; slotIndex < filledSlots; slotIndex++) {
            let x = 280 - displayRadius * Math.sin(angle);
            let y = 280 - displayRadius * Math.cos(angle);
            let ee = document.createElementNS("http://www.w3.org/2000/svg", "use");
            ee.setAttribute("href", "#ec-electron");
            ee.setAttribute("x", x);
            ee.setAttribute("y", y);
            electronGroup.appendChild(ee);
            angle += angleIncrement;
        }
        electronShellNumber++;
    }

    document.getElementById("ec-symbol").textContent = elementData["symbol"];

}

periodicTable.findElementDataByCategoryAndSymbol = function(elementCategory, elementSymbol) {
    let atomicNumbers = chemicalElementCategoryData[elementCategory]
    for (let i = 0; i < atomicNumbers.length; i++) {
        let atomicNumber = atomicNumbers[i];
        let elementData = chemicalElementData[atomicNumber - 1];
        if (elementSymbol === elementData["symbol"]) {
            return elementData;
        }
    }
    return null;
}

periodicTable.getElectronConfiguration = function(elementData) {
    let ret = [];
    let atomicNumber = elementData["number"];
    let electronCount = atomicNumber;

    let electronsSoFar = 0;

    if ("electronConfiguration" in elementData) {
        let configInfo = elementData["electronConfiguration"];
        if ("observed" in configInfo) {
            let observedConfig = configInfo["observed"];
            let electronConfigurationDescription = observedConfig.match(/(\[([A-Za-z]+)\])?([0-9spdfg ]*)/);
            let baseNobleGasSymbol = electronConfigurationDescription[2];
            if (baseNobleGasSymbol) {
                let baseNobleGasElementData = periodicTable.findElementDataByCategoryAndSymbol("noble-gas", baseNobleGasSymbol);
                ret = periodicTable.getElectronConfiguration(baseNobleGasElementData);
            }
            if (3 in electronConfigurationDescription) {
                // specialisation of noble gas
                let extraSubshellDescription = electronConfigurationDescription[3];
                let subshellPattern = /([0-9])([spdfg])([0-9]+)/g;
                while (true) {
                    let subshellSpec = subshellPattern.exec(extraSubshellDescription);
                    if (subshellSpec === null) {
                        break;
                    }
                    let shellNumber = parseInt(subshellSpec[1]);
                    let subshellLetter = subshellSpec[2];
                    let electronCount = parseInt(subshellSpec[3]);
                    if (shellNumber in ret) {
                        ret[shellNumber] += electronCount; //TODO: show subshells somehow
                    } else {
                        ret[shellNumber] = electronCount;
                    }
                }
            }

        }
    }
    return ret;
}

periodicTable.buildRow = function(header, rowLength, leftmostAtomicNumber, leftCount, rightmostAtomicNumber, rightCount) {

    let periodTemplateEl = document.importNode(document.getElementById("periodTemplate"), true);
    let periodTemplateContent = periodTemplateEl.content;
    periodTemplateContent.querySelector("th").innerText = header;
    let lang = periodicTable.getLanguage();

    let rowData = periodicTable.getRowData(rowLength, leftmostAtomicNumber, leftCount, rightmostAtomicNumber, rightCount);
    for (let colIndex = 0; colIndex < rowLength; colIndex++) {
        let elementData = rowData[colIndex];
        if (elementData === undefined || elementData === null) {
            periodTemplateContent.querySelector("tr").appendChild(document.createElement("td"));
        } else {
            let elementCategory = periodicTable.findElementCategory(elementData["number"]);
            let elementTemplateEl = document.importNode(document.getElementById("elementTemplate"), true);
            let elementTemplateContent = elementTemplateEl.content;
            let td = elementTemplateContent.querySelector("td");

            td.dataset.atomicNumber = elementData["number"];
            elementTemplateContent.querySelector("td").className += " " + elementCategory;
            elementTemplateContent.querySelector(".atomic-number").innerText = elementData["number"];
            elementTemplateContent.querySelector(".symbol").innerText = elementData["symbol"];
            elementTemplateContent.querySelector(".name").innerText = elementData["name"][lang];
            td.onclick = periodicTable.handleClickElement;
            periodTemplateContent.querySelector("tr").appendChild(elementTemplateContent);
        }

    }

    return periodTemplateContent;
}

periodicTable.buildTable = function() {

    let tbodyEl = document.querySelector("#main-table tbody");

    let periodTr = periodicTable.buildRow("Period 1", 18, 1, 1, 2, 1);
    tbodyEl.appendChild(periodTr);
    periodTr = periodicTable.buildRow("Period 2", 18, 3, 2, 10, 6);
    tbodyEl.appendChild(periodTr);
    periodTr = periodicTable.buildRow("Period 3", 18, 11, 2, 18, 6);
    tbodyEl.appendChild(periodTr);
    periodTr = periodicTable.buildRow("Period 4", 18, 19, 18, 0, 0);
    tbodyEl.appendChild(periodTr);
    periodTr = periodicTable.buildRow("Period 5", 18, 37, 18, 0, 0);
    tbodyEl.appendChild(periodTr);
    periodTr = periodicTable.buildRow("Period 6", 18, 55, 2, 86, 15);
    tbodyEl.appendChild(periodTr);
    periodTr = periodicTable.buildRow("Period 7", 18, 87, 2, 118, 15);
    tbodyEl.appendChild(periodTr);

    tbodyEl = document.querySelector("#lanthanides-table tbody");
    periodTr = periodicTable.buildRow("", 15, 57, 15, 0, 0);
    tbodyEl.appendChild(periodTr);

    tbodyEl = document.querySelector("#actinides-table tbody");
    periodTr = periodicTable.buildRow("", 15, 89, 15, 0, 0);
    tbodyEl.appendChild(periodTr);

}

periodicTable.getLanguage = function() {
    let ret = localStorage.getItem('localeId');
    if (ret === null || ret === undefined) {
        let languageSelector = document.getElementById("language-selector");
        ret = languageSelector.value;
        localStorage.setItem('localeId', ret);
    }
    return ret;

}

periodicTable.updateTranslations = function(lang) {

    //translate static text

    //load from xlf
    translationManager.loadTranslations(lang, function() {
        let i18nElements = document.querySelectorAll(".i18n");
        for (let elementIndex = 0; elementIndex < i18nElements.length; elementIndex++) {
            let e = i18nElements[elementIndex];
            translationManager.translateElement(e);
        }
    });

    //translate element names

    let elementElements = document.querySelectorAll("td.element");
    for (let elementIndex = 0; elementIndex < elementElements.length; elementIndex++) {
        let e = elementElements[elementIndex];
        let atomicNumber = e.dataset.atomicNumber;
        let elementData = chemicalElementData[atomicNumber - 1];
        e.querySelector(".name").innerText = elementData["name"][lang];
    }
}

window.addEventListener('load', function() {

    let lang = periodicTable.getLanguage();
    periodicTable.updateTranslations(lang);
    periodicTable.buildTable();
    periodicTable.initElectronConfigurationDisplay();

    let languageSelector = document.getElementById("language-selector");
    languageSelector.addEventListener('change', function() {
        lang = languageSelector.value;
        localStorage.setItem('localeId', lang);
        periodicTable.updateTranslations(lang);
    });

}, false);