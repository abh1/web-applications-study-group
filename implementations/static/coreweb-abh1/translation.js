var translationManager = {};

translationManager.loadTranslations = function(lang, callback) {

    if (lang == 'en') {
        translationManager.translations = null;
        callback();
    } else {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'locale/messages.' + lang + '.xlf');
        xhr.onload = function() {
            if (xhr.status === 200) {
                translationManager.translations = translationManager.parseXlf(xhr.responseXML);
                callback();
            }
            else {
                alert('Failed to load translations for language ' + lang);
            }
        };
        xhr.send();
    }
}

translationManager.parseXlf = function(xlf) {
    let ret = {};

    function nsResolver(prefix) {
        let ns = {
            'xliff' : 'urn:oasis:names:tc:xliff:document:1.2'
        };
        return ns[prefix] || null;
    }
    let iterator = xlf.evaluate('/xliff:xliff/xliff:file/xliff:body/xliff:trans-unit', xlf, nsResolver, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null);
    for (let thisNode = iterator.iterateNext(); thisNode; thisNode = iterator.iterateNext()) {
        let tId = thisNode.getAttribute('id');
        let tText = xlf.evaluate('xliff:target', thisNode, nsResolver, XPathResult.STRING_TYPE, null).stringValue;
        ret[tId] = tText;
    }
    return ret;
}


translationManager.translateElement = function(e) {
    let tKey = e.dataset.t;
    if (tKey) {
        if (translationManager.translations) {
            if (tKey in translationManager.translations) {
                if (!e.dataset.translationEn) {
                    e.dataset.translationEn = e.innerHTML;
                }
                e.innerHTML = translationManager.translations[tKey];
            }
        } else if (e.dataset.translationEn) {
            e.innerHTML = e.dataset.translationEn;
        }
    }
}