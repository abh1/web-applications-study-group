package studygroup.data;

import java.io.*;
import java.util.*;

import io.swagger.model.*;
import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.springframework.stereotype.Repository;

@Repository
public final class ChemicalElementRepository {

    private final Sheet fullData;
    private final Map<Integer, String> atomicNumberToCategory;

    public ChemicalElementRepository() {
        try (final InputStream excelFileIn = getClass().getResourceAsStream("/Periodic-Table.xlsx")) {
            final Workbook workbook = new XSSFWorkbook(excelFileIn);
            this.fullData = workbook.getSheet("Full Data");
        } catch (IOException e) {
            throw new RuntimeException("Can't read Excel file", e);
        }
        this.atomicNumberToCategory = new HashMap<>();
        for (int a : new int[]{1, 6, 7, 8, 15, 16, 34}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "nonmetal");
        }
        for (int a : new int[]{5, 14, 32, 33, 51, 52, 84}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "metalloid");
        }
        for (int a : new int[]{9, 17, 35, 53, 85, 117}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "halogen");
        }
        for (int a : new int[]{2, 10, 18, 36, 54, 86, 118}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "noble-gas");
        }
        for (int a : new int[]{3, 11, 19, 37, 55, 87}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "alkali-metal");
        }
        for (int a : new int[]{4, 12, 20, 38, 56, 88}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "alkaline-earth-metal");
        }
        for (int a : new int[]{57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "lanthanoid");
        }
        for (int a : new int[]{89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "actinoid");
        }
        for (int a : new int[]{21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 72, 73, 74, 75, 76, 77, 78, 79, 80, 104, 105, 106, 107, 108, 109, 110, 111, 112}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "transition-metal");
        }
        for (int a : new int[]{13, 31, 49, 50, 81, 82, 83, 113, 114, 115, 116}) {
            atomicNumberToCategory.put(Integer.valueOf(a), "post-transition-metal");
        }
    }

    private static String getCellString(final Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellTypeEnum()) {
            case NUMERIC:
                return Integer.toString((int)cell.getNumericCellValue());
            case STRING:
                return cell.getStringCellValue();
            case BLANK:
                return "";
            case FORMULA:
                return cell.getStringCellValue();
            default:
                throw new RuntimeException("WTF type " + cell.getCellTypeEnum());
        }

    }

    public ChemicalElement getElementByAtomicNumber(final int atomicNumber, final String languageCode) {

        final Row elementRow = this.fullData.getRow(4 + atomicNumber);
        final double checkAtomicNumber = elementRow.getCell(0).getNumericCellValue();
        if ((int)checkAtomicNumber != atomicNumber) {
            throw new RuntimeException("Bad data on row!  Expected atomic number " + atomicNumber + " but found " + checkAtomicNumber);
        }

        final int nameColumnNumber;
        switch (languageCode.toLowerCase()) {
            case "de":
                nameColumnNumber = 6;
                break;
            case "fr":
                nameColumnNumber = 7;
                break;
            case "es":
                nameColumnNumber = 8;
                break;
            case "it":
                nameColumnNumber = 9;
                break;
            case "en":
            default:
                nameColumnNumber = 5;
        }

        final String group = getCellString(elementRow.getCell(2));

        ChemicalElement ret = new ChemicalElement()
            .number(atomicNumber)
            .symbol(elementRow.getCell(1).getStringCellValue())
            .name(elementRow.getCell(nameColumnNumber).getStringCellValue())
            .category(this.atomicNumberToCategory.get(Integer.valueOf(atomicNumber)))
            .group(group)
            .period((int)elementRow.getCell(4).getNumericCellValue())
            .mass(new BigDecimal(elementRow.getCell(10).getNumericCellValue()))
            .electronConfiguration(new ChemicalElementElectronConfiguration()
                .predicted(getCellString(elementRow.getCell(27)))
                .observed(getCellString(elementRow.getCell(28))));
        return ret;
    }

    public ChemicalElement getElementByCategoryAndSymbol(final String category, final String symbol, final String languageCode) {
        for (Map.Entry<Integer, String> me : this.atomicNumberToCategory.entrySet()) {
            if (category.equals(me.getValue())) {
                final int atomicNumber = me.getKey().intValue();
                final ChemicalElement e = getElementByAtomicNumber(atomicNumber, languageCode);
                if (symbol.equals(e.getSymbol())) {
                    return e;
                }
            }
        }
        return null;
    }

    public ChemicalElement[] getElementsByAtomicNumberRange(final int minAtomicNumberIncl, final int maxAtomicNumberIncl, final String languageCode) {
        if (maxAtomicNumberIncl >= minAtomicNumberIncl) {
            final ChemicalElement[] ret = new ChemicalElement[maxAtomicNumberIncl + 1 - minAtomicNumberIncl];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = getElementByAtomicNumber(minAtomicNumberIncl + i, languageCode);
            }
            return ret;
        } else {
            return new ChemicalElement[0];
        }
    }
}