package studygroup.api;

import io.swagger.model.ChemicalElement;

import io.swagger.api.ElementApi;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import javax.validation.constraints.*;
import javax.validation.Valid;

import studygroup.data.ChemicalElementRepository;

// adapted from generated/src/main/java/io/swagger/api/ElementApiController.java.  Merge changes from there into here.
@CrossOrigin(origins = "http://localhost:8080")
@Controller
public class ElementApiController implements ElementApi {

    private final ChemicalElementRepository repo;
    
    @Autowired
    public ElementApiController(final ChemicalElementRepository repo) {
        this.repo = repo;
    }

    private static String getLanguageCode(final String langParam) {
        if (StringUtils.hasText(langParam)) {
            return langParam.trim();
        } else {
            return "en";
        }
    }

    public ResponseEntity<List<ChemicalElement>> findElementsByAtomicNumberRange( @NotNull@ApiParam(value = "Minimum atomic number, inclusive", required = true) @RequestParam(value = "min", required = true) Integer min,
         @NotNull@ApiParam(value = "Maximum atomic number, inclusive", required = true) @RequestParam(value = "max", required = true) Integer max,
        @ApiParam(value = "Language code") @RequestParam(value = "lang", required = false) String lang) {
        
        ChemicalElement[] elements = repo.getElementsByAtomicNumberRange(min.intValue(), max.intValue(), getLanguageCode(lang));

        return new ResponseEntity<List<ChemicalElement>>(Arrays.asList(elements), HttpStatus.OK);
    }

    public ResponseEntity<ChemicalElement> getElementByAtomicNumber(@ApiParam(value = "atomic number of element to return",required=true ) @PathVariable("atomicNumber") Integer atomicNumber,
        @ApiParam(value = "Language code") @RequestParam(value = "lang", required = false) String lang) {

        ChemicalElement element = repo.getElementByAtomicNumber(atomicNumber.intValue(), getLanguageCode(lang));
            
        return new ResponseEntity<ChemicalElement>(element, HttpStatus.OK);
    }


    public ResponseEntity<ChemicalElement> getElementByCategoryAndSymbol( @NotNull@ApiParam(value = "category to search.  Valid categories are: nonmetal metalloid halogen noble-gas alkali-metal alkaline-earth-metal lanthanoid actinoid transition-metal post-transition-metal", required = true) @RequestParam(value = "category", required = true) String category,
        @NotNull@ApiParam(value = "Element symbol to match.  E.g. Xe", required = true) @RequestParam(value = "symbol", required = true) String symbol,
        @ApiParam(value = "Language code") @RequestParam(value = "lang", required = false) String lang) {

        ChemicalElement element = repo.getElementByCategoryAndSymbol(category, symbol, getLanguageCode(lang));
            
        return new ResponseEntity<ChemicalElement>(element, HttpStatus.OK);
    }
}
