package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.ChemicalElementAbundance;
import io.swagger.model.ChemicalElementCost;
import io.swagger.model.ChemicalElementElectronConfiguration;
import io.swagger.model.ChemicalElementIonizationPotentialEv;
import io.swagger.model.ChemicalElementRadii;
import io.swagger.model.ChemicalElementReactions;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChemicalElement
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

public class ChemicalElement   {
  @JsonProperty("number")
  private Integer number = null;

  @JsonProperty("symbol")
  private String symbol = null;

  @JsonProperty("group")
  private String group = null;

  @JsonProperty("period")
  private Integer period = null;

  @JsonProperty("category")
  private String category = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("mass")
  private BigDecimal mass = null;

  @JsonProperty("meltingPoint")
  private BigDecimal meltingPoint = null;

  @JsonProperty("boilingPoint")
  private BigDecimal boilingPoint = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("density")
  private BigDecimal density = null;

  @JsonProperty("electronegativity")
  private BigDecimal electronegativity = null;

  @JsonProperty("ionizationPotentialEv")
  private ChemicalElementIonizationPotentialEv ionizationPotentialEv = null;

  @JsonProperty("ionizationPotentialKjmol")
  private ChemicalElementIonizationPotentialEv ionizationPotentialKjmol = null;

  @JsonProperty("electronAffinityEv")
  private String electronAffinityEv = null;

  @JsonProperty("electronAffinityKjmol")
  private String electronAffinityKjmol = null;

  @JsonProperty("commonOxidationStates")
  private String commonOxidationStates = null;

  @JsonProperty("commonIons")
  private String commonIons = null;

  @JsonProperty("electronConfiguration")
  private ChemicalElementElectronConfiguration electronConfiguration = null;

  @JsonProperty("radii")
  private ChemicalElementRadii radii = null;

  @JsonProperty("volume")
  private BigDecimal volume = null;

  @JsonProperty("crystalStructure")
  private String crystalStructure = null;

  @JsonProperty("casRegistryNo")
  private String casRegistryNo = null;

  @JsonProperty("electricalConductivity")
  private BigDecimal electricalConductivity = null;

  @JsonProperty("specificHeat")
  private BigDecimal specificHeat = null;

  @JsonProperty("heatOfFusion")
  private BigDecimal heatOfFusion = null;

  @JsonProperty("heatOfVaporization")
  private BigDecimal heatOfVaporization = null;

  @JsonProperty("thermalConductivity")
  private BigDecimal thermalConductivity = null;

  @JsonProperty("densityInEarthCrust")
  private BigDecimal densityInEarthCrust = null;

  @JsonProperty("densityInSeawater")
  private BigDecimal densityInSeawater = null;

  @JsonProperty("percentHumanBody")
  private BigDecimal percentHumanBody = null;

  @JsonProperty("source")
  private String source = null;

  @JsonProperty("colour")
  private String colour = null;

  @JsonProperty("characteristics")
  private String characteristics = null;

  @JsonProperty("structure")
  private String structure = null;

  @JsonProperty("hardness")
  private BigDecimal hardness = null;

  @JsonProperty("uses")
  private String uses = null;

  @JsonProperty("reactions")
  private ChemicalElementReactions reactions = null;

  @JsonProperty("hydrides")
  private String hydrides = null;

  @JsonProperty("oxides")
  private String oxides = null;

  @JsonProperty("chlorides")
  private String chlorides = null;

  @JsonProperty("polarizability")
  private String polarizability = null;

  @JsonProperty("heatAtomization")
  private BigDecimal heatAtomization = null;

  @JsonProperty("yearDiscovered")
  private BigDecimal yearDiscovered = null;

  @JsonProperty("abundance")
  private ChemicalElementAbundance abundance = null;

  @JsonProperty("cost")
  private ChemicalElementCost cost = null;

  public ChemicalElement number(Integer number) {
    this.number = number;
    return this;
  }

   /**
   * Get number
   * @return number
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  public ChemicalElement symbol(String symbol) {
    this.symbol = symbol;
    return this;
  }

   /**
   * Get symbol
   * @return symbol
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public ChemicalElement group(String group) {
    this.group = group;
    return this;
  }

   /**
   * Get group
   * @return group
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public ChemicalElement period(Integer period) {
    this.period = period;
    return this;
  }

   /**
   * Get period
   * @return period
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getPeriod() {
    return period;
  }

  public void setPeriod(Integer period) {
    this.period = period;
  }

  public ChemicalElement category(String category) {
    this.category = category;
    return this;
  }

   /**
   * Get category
   * @return category
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public ChemicalElement name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ChemicalElement mass(BigDecimal mass) {
    this.mass = mass;
    return this;
  }

   /**
   * Get mass
   * @return mass
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public BigDecimal getMass() {
    return mass;
  }

  public void setMass(BigDecimal mass) {
    this.mass = mass;
  }

  public ChemicalElement meltingPoint(BigDecimal meltingPoint) {
    this.meltingPoint = meltingPoint;
    return this;
  }

   /**
   * Get meltingPoint
   * @return meltingPoint
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getMeltingPoint() {
    return meltingPoint;
  }

  public void setMeltingPoint(BigDecimal meltingPoint) {
    this.meltingPoint = meltingPoint;
  }

  public ChemicalElement boilingPoint(BigDecimal boilingPoint) {
    this.boilingPoint = boilingPoint;
    return this;
  }

   /**
   * Get boilingPoint
   * @return boilingPoint
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getBoilingPoint() {
    return boilingPoint;
  }

  public void setBoilingPoint(BigDecimal boilingPoint) {
    this.boilingPoint = boilingPoint;
  }

  public ChemicalElement state(String state) {
    this.state = state;
    return this;
  }

   /**
   * Get state
   * @return state
  **/
  @ApiModelProperty(value = "")


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public ChemicalElement density(BigDecimal density) {
    this.density = density;
    return this;
  }

   /**
   * Get density
   * @return density
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDensity() {
    return density;
  }

  public void setDensity(BigDecimal density) {
    this.density = density;
  }

  public ChemicalElement electronegativity(BigDecimal electronegativity) {
    this.electronegativity = electronegativity;
    return this;
  }

   /**
   * Get electronegativity
   * @return electronegativity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getElectronegativity() {
    return electronegativity;
  }

  public void setElectronegativity(BigDecimal electronegativity) {
    this.electronegativity = electronegativity;
  }

  public ChemicalElement ionizationPotentialEv(ChemicalElementIonizationPotentialEv ionizationPotentialEv) {
    this.ionizationPotentialEv = ionizationPotentialEv;
    return this;
  }

   /**
   * Get ionizationPotentialEv
   * @return ionizationPotentialEv
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ChemicalElementIonizationPotentialEv getIonizationPotentialEv() {
    return ionizationPotentialEv;
  }

  public void setIonizationPotentialEv(ChemicalElementIonizationPotentialEv ionizationPotentialEv) {
    this.ionizationPotentialEv = ionizationPotentialEv;
  }

  public ChemicalElement ionizationPotentialKjmol(ChemicalElementIonizationPotentialEv ionizationPotentialKjmol) {
    this.ionizationPotentialKjmol = ionizationPotentialKjmol;
    return this;
  }

   /**
   * Get ionizationPotentialKjmol
   * @return ionizationPotentialKjmol
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ChemicalElementIonizationPotentialEv getIonizationPotentialKjmol() {
    return ionizationPotentialKjmol;
  }

  public void setIonizationPotentialKjmol(ChemicalElementIonizationPotentialEv ionizationPotentialKjmol) {
    this.ionizationPotentialKjmol = ionizationPotentialKjmol;
  }

  public ChemicalElement electronAffinityEv(String electronAffinityEv) {
    this.electronAffinityEv = electronAffinityEv;
    return this;
  }

   /**
   * Get electronAffinityEv
   * @return electronAffinityEv
  **/
  @ApiModelProperty(value = "")


  public String getElectronAffinityEv() {
    return electronAffinityEv;
  }

  public void setElectronAffinityEv(String electronAffinityEv) {
    this.electronAffinityEv = electronAffinityEv;
  }

  public ChemicalElement electronAffinityKjmol(String electronAffinityKjmol) {
    this.electronAffinityKjmol = electronAffinityKjmol;
    return this;
  }

   /**
   * Get electronAffinityKjmol
   * @return electronAffinityKjmol
  **/
  @ApiModelProperty(value = "")


  public String getElectronAffinityKjmol() {
    return electronAffinityKjmol;
  }

  public void setElectronAffinityKjmol(String electronAffinityKjmol) {
    this.electronAffinityKjmol = electronAffinityKjmol;
  }

  public ChemicalElement commonOxidationStates(String commonOxidationStates) {
    this.commonOxidationStates = commonOxidationStates;
    return this;
  }

   /**
   * Get commonOxidationStates
   * @return commonOxidationStates
  **/
  @ApiModelProperty(value = "")


  public String getCommonOxidationStates() {
    return commonOxidationStates;
  }

  public void setCommonOxidationStates(String commonOxidationStates) {
    this.commonOxidationStates = commonOxidationStates;
  }

  public ChemicalElement commonIons(String commonIons) {
    this.commonIons = commonIons;
    return this;
  }

   /**
   * Get commonIons
   * @return commonIons
  **/
  @ApiModelProperty(value = "")


  public String getCommonIons() {
    return commonIons;
  }

  public void setCommonIons(String commonIons) {
    this.commonIons = commonIons;
  }

  public ChemicalElement electronConfiguration(ChemicalElementElectronConfiguration electronConfiguration) {
    this.electronConfiguration = electronConfiguration;
    return this;
  }

   /**
   * Get electronConfiguration
   * @return electronConfiguration
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ChemicalElementElectronConfiguration getElectronConfiguration() {
    return electronConfiguration;
  }

  public void setElectronConfiguration(ChemicalElementElectronConfiguration electronConfiguration) {
    this.electronConfiguration = electronConfiguration;
  }

  public ChemicalElement radii(ChemicalElementRadii radii) {
    this.radii = radii;
    return this;
  }

   /**
   * Get radii
   * @return radii
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ChemicalElementRadii getRadii() {
    return radii;
  }

  public void setRadii(ChemicalElementRadii radii) {
    this.radii = radii;
  }

  public ChemicalElement volume(BigDecimal volume) {
    this.volume = volume;
    return this;
  }

   /**
   * Get volume
   * @return volume
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getVolume() {
    return volume;
  }

  public void setVolume(BigDecimal volume) {
    this.volume = volume;
  }

  public ChemicalElement crystalStructure(String crystalStructure) {
    this.crystalStructure = crystalStructure;
    return this;
  }

   /**
   * Get crystalStructure
   * @return crystalStructure
  **/
  @ApiModelProperty(value = "")


  public String getCrystalStructure() {
    return crystalStructure;
  }

  public void setCrystalStructure(String crystalStructure) {
    this.crystalStructure = crystalStructure;
  }

  public ChemicalElement casRegistryNo(String casRegistryNo) {
    this.casRegistryNo = casRegistryNo;
    return this;
  }

   /**
   * Get casRegistryNo
   * @return casRegistryNo
  **/
  @ApiModelProperty(value = "")


  public String getCasRegistryNo() {
    return casRegistryNo;
  }

  public void setCasRegistryNo(String casRegistryNo) {
    this.casRegistryNo = casRegistryNo;
  }

  public ChemicalElement electricalConductivity(BigDecimal electricalConductivity) {
    this.electricalConductivity = electricalConductivity;
    return this;
  }

   /**
   * Get electricalConductivity
   * @return electricalConductivity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getElectricalConductivity() {
    return electricalConductivity;
  }

  public void setElectricalConductivity(BigDecimal electricalConductivity) {
    this.electricalConductivity = electricalConductivity;
  }

  public ChemicalElement specificHeat(BigDecimal specificHeat) {
    this.specificHeat = specificHeat;
    return this;
  }

   /**
   * Get specificHeat
   * @return specificHeat
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getSpecificHeat() {
    return specificHeat;
  }

  public void setSpecificHeat(BigDecimal specificHeat) {
    this.specificHeat = specificHeat;
  }

  public ChemicalElement heatOfFusion(BigDecimal heatOfFusion) {
    this.heatOfFusion = heatOfFusion;
    return this;
  }

   /**
   * Get heatOfFusion
   * @return heatOfFusion
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getHeatOfFusion() {
    return heatOfFusion;
  }

  public void setHeatOfFusion(BigDecimal heatOfFusion) {
    this.heatOfFusion = heatOfFusion;
  }

  public ChemicalElement heatOfVaporization(BigDecimal heatOfVaporization) {
    this.heatOfVaporization = heatOfVaporization;
    return this;
  }

   /**
   * Get heatOfVaporization
   * @return heatOfVaporization
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getHeatOfVaporization() {
    return heatOfVaporization;
  }

  public void setHeatOfVaporization(BigDecimal heatOfVaporization) {
    this.heatOfVaporization = heatOfVaporization;
  }

  public ChemicalElement thermalConductivity(BigDecimal thermalConductivity) {
    this.thermalConductivity = thermalConductivity;
    return this;
  }

   /**
   * Get thermalConductivity
   * @return thermalConductivity
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getThermalConductivity() {
    return thermalConductivity;
  }

  public void setThermalConductivity(BigDecimal thermalConductivity) {
    this.thermalConductivity = thermalConductivity;
  }

  public ChemicalElement densityInEarthCrust(BigDecimal densityInEarthCrust) {
    this.densityInEarthCrust = densityInEarthCrust;
    return this;
  }

   /**
   * Get densityInEarthCrust
   * @return densityInEarthCrust
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDensityInEarthCrust() {
    return densityInEarthCrust;
  }

  public void setDensityInEarthCrust(BigDecimal densityInEarthCrust) {
    this.densityInEarthCrust = densityInEarthCrust;
  }

  public ChemicalElement densityInSeawater(BigDecimal densityInSeawater) {
    this.densityInSeawater = densityInSeawater;
    return this;
  }

   /**
   * Get densityInSeawater
   * @return densityInSeawater
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getDensityInSeawater() {
    return densityInSeawater;
  }

  public void setDensityInSeawater(BigDecimal densityInSeawater) {
    this.densityInSeawater = densityInSeawater;
  }

  public ChemicalElement percentHumanBody(BigDecimal percentHumanBody) {
    this.percentHumanBody = percentHumanBody;
    return this;
  }

   /**
   * Get percentHumanBody
   * @return percentHumanBody
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPercentHumanBody() {
    return percentHumanBody;
  }

  public void setPercentHumanBody(BigDecimal percentHumanBody) {
    this.percentHumanBody = percentHumanBody;
  }

  public ChemicalElement source(String source) {
    this.source = source;
    return this;
  }

   /**
   * Get source
   * @return source
  **/
  @ApiModelProperty(value = "")


  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public ChemicalElement colour(String colour) {
    this.colour = colour;
    return this;
  }

   /**
   * Get colour
   * @return colour
  **/
  @ApiModelProperty(value = "")


  public String getColour() {
    return colour;
  }

  public void setColour(String colour) {
    this.colour = colour;
  }

  public ChemicalElement characteristics(String characteristics) {
    this.characteristics = characteristics;
    return this;
  }

   /**
   * Get characteristics
   * @return characteristics
  **/
  @ApiModelProperty(value = "")


  public String getCharacteristics() {
    return characteristics;
  }

  public void setCharacteristics(String characteristics) {
    this.characteristics = characteristics;
  }

  public ChemicalElement structure(String structure) {
    this.structure = structure;
    return this;
  }

   /**
   * Get structure
   * @return structure
  **/
  @ApiModelProperty(value = "")


  public String getStructure() {
    return structure;
  }

  public void setStructure(String structure) {
    this.structure = structure;
  }

  public ChemicalElement hardness(BigDecimal hardness) {
    this.hardness = hardness;
    return this;
  }

   /**
   * Get hardness
   * @return hardness
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getHardness() {
    return hardness;
  }

  public void setHardness(BigDecimal hardness) {
    this.hardness = hardness;
  }

  public ChemicalElement uses(String uses) {
    this.uses = uses;
    return this;
  }

   /**
   * Get uses
   * @return uses
  **/
  @ApiModelProperty(value = "")


  public String getUses() {
    return uses;
  }

  public void setUses(String uses) {
    this.uses = uses;
  }

  public ChemicalElement reactions(ChemicalElementReactions reactions) {
    this.reactions = reactions;
    return this;
  }

   /**
   * Get reactions
   * @return reactions
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ChemicalElementReactions getReactions() {
    return reactions;
  }

  public void setReactions(ChemicalElementReactions reactions) {
    this.reactions = reactions;
  }

  public ChemicalElement hydrides(String hydrides) {
    this.hydrides = hydrides;
    return this;
  }

   /**
   * Get hydrides
   * @return hydrides
  **/
  @ApiModelProperty(value = "")


  public String getHydrides() {
    return hydrides;
  }

  public void setHydrides(String hydrides) {
    this.hydrides = hydrides;
  }

  public ChemicalElement oxides(String oxides) {
    this.oxides = oxides;
    return this;
  }

   /**
   * Get oxides
   * @return oxides
  **/
  @ApiModelProperty(value = "")


  public String getOxides() {
    return oxides;
  }

  public void setOxides(String oxides) {
    this.oxides = oxides;
  }

  public ChemicalElement chlorides(String chlorides) {
    this.chlorides = chlorides;
    return this;
  }

   /**
   * Get chlorides
   * @return chlorides
  **/
  @ApiModelProperty(value = "")


  public String getChlorides() {
    return chlorides;
  }

  public void setChlorides(String chlorides) {
    this.chlorides = chlorides;
  }

  public ChemicalElement polarizability(String polarizability) {
    this.polarizability = polarizability;
    return this;
  }

   /**
   * Get polarizability
   * @return polarizability
  **/
  @ApiModelProperty(value = "")


  public String getPolarizability() {
    return polarizability;
  }

  public void setPolarizability(String polarizability) {
    this.polarizability = polarizability;
  }

  public ChemicalElement heatAtomization(BigDecimal heatAtomization) {
    this.heatAtomization = heatAtomization;
    return this;
  }

   /**
   * Get heatAtomization
   * @return heatAtomization
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getHeatAtomization() {
    return heatAtomization;
  }

  public void setHeatAtomization(BigDecimal heatAtomization) {
    this.heatAtomization = heatAtomization;
  }

  public ChemicalElement yearDiscovered(BigDecimal yearDiscovered) {
    this.yearDiscovered = yearDiscovered;
    return this;
  }

   /**
   * Get yearDiscovered
   * @return yearDiscovered
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getYearDiscovered() {
    return yearDiscovered;
  }

  public void setYearDiscovered(BigDecimal yearDiscovered) {
    this.yearDiscovered = yearDiscovered;
  }

  public ChemicalElement abundance(ChemicalElementAbundance abundance) {
    this.abundance = abundance;
    return this;
  }

   /**
   * Get abundance
   * @return abundance
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ChemicalElementAbundance getAbundance() {
    return abundance;
  }

  public void setAbundance(ChemicalElementAbundance abundance) {
    this.abundance = abundance;
  }

  public ChemicalElement cost(ChemicalElementCost cost) {
    this.cost = cost;
    return this;
  }

   /**
   * Get cost
   * @return cost
  **/
  @ApiModelProperty(value = "")

  @Valid

  public ChemicalElementCost getCost() {
    return cost;
  }

  public void setCost(ChemicalElementCost cost) {
    this.cost = cost;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChemicalElement chemicalElement = (ChemicalElement) o;
    return Objects.equals(this.number, chemicalElement.number) &&
        Objects.equals(this.symbol, chemicalElement.symbol) &&
        Objects.equals(this.group, chemicalElement.group) &&
        Objects.equals(this.period, chemicalElement.period) &&
        Objects.equals(this.category, chemicalElement.category) &&
        Objects.equals(this.name, chemicalElement.name) &&
        Objects.equals(this.mass, chemicalElement.mass) &&
        Objects.equals(this.meltingPoint, chemicalElement.meltingPoint) &&
        Objects.equals(this.boilingPoint, chemicalElement.boilingPoint) &&
        Objects.equals(this.state, chemicalElement.state) &&
        Objects.equals(this.density, chemicalElement.density) &&
        Objects.equals(this.electronegativity, chemicalElement.electronegativity) &&
        Objects.equals(this.ionizationPotentialEv, chemicalElement.ionizationPotentialEv) &&
        Objects.equals(this.ionizationPotentialKjmol, chemicalElement.ionizationPotentialKjmol) &&
        Objects.equals(this.electronAffinityEv, chemicalElement.electronAffinityEv) &&
        Objects.equals(this.electronAffinityKjmol, chemicalElement.electronAffinityKjmol) &&
        Objects.equals(this.commonOxidationStates, chemicalElement.commonOxidationStates) &&
        Objects.equals(this.commonIons, chemicalElement.commonIons) &&
        Objects.equals(this.electronConfiguration, chemicalElement.electronConfiguration) &&
        Objects.equals(this.radii, chemicalElement.radii) &&
        Objects.equals(this.volume, chemicalElement.volume) &&
        Objects.equals(this.crystalStructure, chemicalElement.crystalStructure) &&
        Objects.equals(this.casRegistryNo, chemicalElement.casRegistryNo) &&
        Objects.equals(this.electricalConductivity, chemicalElement.electricalConductivity) &&
        Objects.equals(this.specificHeat, chemicalElement.specificHeat) &&
        Objects.equals(this.heatOfFusion, chemicalElement.heatOfFusion) &&
        Objects.equals(this.heatOfVaporization, chemicalElement.heatOfVaporization) &&
        Objects.equals(this.thermalConductivity, chemicalElement.thermalConductivity) &&
        Objects.equals(this.densityInEarthCrust, chemicalElement.densityInEarthCrust) &&
        Objects.equals(this.densityInSeawater, chemicalElement.densityInSeawater) &&
        Objects.equals(this.percentHumanBody, chemicalElement.percentHumanBody) &&
        Objects.equals(this.source, chemicalElement.source) &&
        Objects.equals(this.colour, chemicalElement.colour) &&
        Objects.equals(this.characteristics, chemicalElement.characteristics) &&
        Objects.equals(this.structure, chemicalElement.structure) &&
        Objects.equals(this.hardness, chemicalElement.hardness) &&
        Objects.equals(this.uses, chemicalElement.uses) &&
        Objects.equals(this.reactions, chemicalElement.reactions) &&
        Objects.equals(this.hydrides, chemicalElement.hydrides) &&
        Objects.equals(this.oxides, chemicalElement.oxides) &&
        Objects.equals(this.chlorides, chemicalElement.chlorides) &&
        Objects.equals(this.polarizability, chemicalElement.polarizability) &&
        Objects.equals(this.heatAtomization, chemicalElement.heatAtomization) &&
        Objects.equals(this.yearDiscovered, chemicalElement.yearDiscovered) &&
        Objects.equals(this.abundance, chemicalElement.abundance) &&
        Objects.equals(this.cost, chemicalElement.cost);
  }

  @Override
  public int hashCode() {
    return Objects.hash(number, symbol, group, period, category, name, mass, meltingPoint, boilingPoint, state, density, electronegativity, ionizationPotentialEv, ionizationPotentialKjmol, electronAffinityEv, electronAffinityKjmol, commonOxidationStates, commonIons, electronConfiguration, radii, volume, crystalStructure, casRegistryNo, electricalConductivity, specificHeat, heatOfFusion, heatOfVaporization, thermalConductivity, densityInEarthCrust, densityInSeawater, percentHumanBody, source, colour, characteristics, structure, hardness, uses, reactions, hydrides, oxides, chlorides, polarizability, heatAtomization, yearDiscovered, abundance, cost);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChemicalElement {\n");
    
    sb.append("    number: ").append(toIndentedString(number)).append("\n");
    sb.append("    symbol: ").append(toIndentedString(symbol)).append("\n");
    sb.append("    group: ").append(toIndentedString(group)).append("\n");
    sb.append("    period: ").append(toIndentedString(period)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    mass: ").append(toIndentedString(mass)).append("\n");
    sb.append("    meltingPoint: ").append(toIndentedString(meltingPoint)).append("\n");
    sb.append("    boilingPoint: ").append(toIndentedString(boilingPoint)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    density: ").append(toIndentedString(density)).append("\n");
    sb.append("    electronegativity: ").append(toIndentedString(electronegativity)).append("\n");
    sb.append("    ionizationPotentialEv: ").append(toIndentedString(ionizationPotentialEv)).append("\n");
    sb.append("    ionizationPotentialKjmol: ").append(toIndentedString(ionizationPotentialKjmol)).append("\n");
    sb.append("    electronAffinityEv: ").append(toIndentedString(electronAffinityEv)).append("\n");
    sb.append("    electronAffinityKjmol: ").append(toIndentedString(electronAffinityKjmol)).append("\n");
    sb.append("    commonOxidationStates: ").append(toIndentedString(commonOxidationStates)).append("\n");
    sb.append("    commonIons: ").append(toIndentedString(commonIons)).append("\n");
    sb.append("    electronConfiguration: ").append(toIndentedString(electronConfiguration)).append("\n");
    sb.append("    radii: ").append(toIndentedString(radii)).append("\n");
    sb.append("    volume: ").append(toIndentedString(volume)).append("\n");
    sb.append("    crystalStructure: ").append(toIndentedString(crystalStructure)).append("\n");
    sb.append("    casRegistryNo: ").append(toIndentedString(casRegistryNo)).append("\n");
    sb.append("    electricalConductivity: ").append(toIndentedString(electricalConductivity)).append("\n");
    sb.append("    specificHeat: ").append(toIndentedString(specificHeat)).append("\n");
    sb.append("    heatOfFusion: ").append(toIndentedString(heatOfFusion)).append("\n");
    sb.append("    heatOfVaporization: ").append(toIndentedString(heatOfVaporization)).append("\n");
    sb.append("    thermalConductivity: ").append(toIndentedString(thermalConductivity)).append("\n");
    sb.append("    densityInEarthCrust: ").append(toIndentedString(densityInEarthCrust)).append("\n");
    sb.append("    densityInSeawater: ").append(toIndentedString(densityInSeawater)).append("\n");
    sb.append("    percentHumanBody: ").append(toIndentedString(percentHumanBody)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    colour: ").append(toIndentedString(colour)).append("\n");
    sb.append("    characteristics: ").append(toIndentedString(characteristics)).append("\n");
    sb.append("    structure: ").append(toIndentedString(structure)).append("\n");
    sb.append("    hardness: ").append(toIndentedString(hardness)).append("\n");
    sb.append("    uses: ").append(toIndentedString(uses)).append("\n");
    sb.append("    reactions: ").append(toIndentedString(reactions)).append("\n");
    sb.append("    hydrides: ").append(toIndentedString(hydrides)).append("\n");
    sb.append("    oxides: ").append(toIndentedString(oxides)).append("\n");
    sb.append("    chlorides: ").append(toIndentedString(chlorides)).append("\n");
    sb.append("    polarizability: ").append(toIndentedString(polarizability)).append("\n");
    sb.append("    heatAtomization: ").append(toIndentedString(heatAtomization)).append("\n");
    sb.append("    yearDiscovered: ").append(toIndentedString(yearDiscovered)).append("\n");
    sb.append("    abundance: ").append(toIndentedString(abundance)).append("\n");
    sb.append("    cost: ").append(toIndentedString(cost)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

