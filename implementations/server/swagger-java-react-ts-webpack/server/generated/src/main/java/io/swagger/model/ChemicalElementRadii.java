package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChemicalElementRadii
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

public class ChemicalElementRadii   {
  @JsonProperty("atomic")
  private BigDecimal atomic = null;

  @JsonProperty("ionic")
  private BigDecimal ionic = null;

  @JsonProperty("covalent")
  private BigDecimal covalent = null;

  @JsonProperty("atomicPm")
  private BigDecimal atomicPm = null;

  @JsonProperty("twoPlusIon")
  private BigDecimal twoPlusIon = null;

  public ChemicalElementRadii atomic(BigDecimal atomic) {
    this.atomic = atomic;
    return this;
  }

   /**
   * Get atomic
   * @return atomic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getAtomic() {
    return atomic;
  }

  public void setAtomic(BigDecimal atomic) {
    this.atomic = atomic;
  }

  public ChemicalElementRadii ionic(BigDecimal ionic) {
    this.ionic = ionic;
    return this;
  }

   /**
   * Get ionic
   * @return ionic
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getIonic() {
    return ionic;
  }

  public void setIonic(BigDecimal ionic) {
    this.ionic = ionic;
  }

  public ChemicalElementRadii covalent(BigDecimal covalent) {
    this.covalent = covalent;
    return this;
  }

   /**
   * Get covalent
   * @return covalent
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getCovalent() {
    return covalent;
  }

  public void setCovalent(BigDecimal covalent) {
    this.covalent = covalent;
  }

  public ChemicalElementRadii atomicPm(BigDecimal atomicPm) {
    this.atomicPm = atomicPm;
    return this;
  }

   /**
   * Get atomicPm
   * @return atomicPm
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getAtomicPm() {
    return atomicPm;
  }

  public void setAtomicPm(BigDecimal atomicPm) {
    this.atomicPm = atomicPm;
  }

  public ChemicalElementRadii twoPlusIon(BigDecimal twoPlusIon) {
    this.twoPlusIon = twoPlusIon;
    return this;
  }

   /**
   * Get twoPlusIon
   * @return twoPlusIon
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getTwoPlusIon() {
    return twoPlusIon;
  }

  public void setTwoPlusIon(BigDecimal twoPlusIon) {
    this.twoPlusIon = twoPlusIon;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChemicalElementRadii chemicalElementRadii = (ChemicalElementRadii) o;
    return Objects.equals(this.atomic, chemicalElementRadii.atomic) &&
        Objects.equals(this.ionic, chemicalElementRadii.ionic) &&
        Objects.equals(this.covalent, chemicalElementRadii.covalent) &&
        Objects.equals(this.atomicPm, chemicalElementRadii.atomicPm) &&
        Objects.equals(this.twoPlusIon, chemicalElementRadii.twoPlusIon);
  }

  @Override
  public int hashCode() {
    return Objects.hash(atomic, ionic, covalent, atomicPm, twoPlusIon);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChemicalElementRadii {\n");
    
    sb.append("    atomic: ").append(toIndentedString(atomic)).append("\n");
    sb.append("    ionic: ").append(toIndentedString(ionic)).append("\n");
    sb.append("    covalent: ").append(toIndentedString(covalent)).append("\n");
    sb.append("    atomicPm: ").append(toIndentedString(atomicPm)).append("\n");
    sb.append("    twoPlusIon: ").append(toIndentedString(twoPlusIon)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

