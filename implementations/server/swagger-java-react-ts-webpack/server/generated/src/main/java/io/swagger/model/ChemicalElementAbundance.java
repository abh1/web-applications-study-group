package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChemicalElementAbundance
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

public class ChemicalElementAbundance   {
  @JsonProperty("solarSystem")
  private BigDecimal solarSystem = null;

  @JsonProperty("earthCrust")
  private BigDecimal earthCrust = null;

  public ChemicalElementAbundance solarSystem(BigDecimal solarSystem) {
    this.solarSystem = solarSystem;
    return this;
  }

   /**
   * Get solarSystem
   * @return solarSystem
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getSolarSystem() {
    return solarSystem;
  }

  public void setSolarSystem(BigDecimal solarSystem) {
    this.solarSystem = solarSystem;
  }

  public ChemicalElementAbundance earthCrust(BigDecimal earthCrust) {
    this.earthCrust = earthCrust;
    return this;
  }

   /**
   * Get earthCrust
   * @return earthCrust
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getEarthCrust() {
    return earthCrust;
  }

  public void setEarthCrust(BigDecimal earthCrust) {
    this.earthCrust = earthCrust;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChemicalElementAbundance chemicalElementAbundance = (ChemicalElementAbundance) o;
    return Objects.equals(this.solarSystem, chemicalElementAbundance.solarSystem) &&
        Objects.equals(this.earthCrust, chemicalElementAbundance.earthCrust);
  }

  @Override
  public int hashCode() {
    return Objects.hash(solarSystem, earthCrust);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChemicalElementAbundance {\n");
    
    sb.append("    solarSystem: ").append(toIndentedString(solarSystem)).append("\n");
    sb.append("    earthCrust: ").append(toIndentedString(earthCrust)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

