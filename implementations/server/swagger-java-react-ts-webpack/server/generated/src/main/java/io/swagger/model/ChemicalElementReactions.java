package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChemicalElementReactions
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

public class ChemicalElementReactions   {
  @JsonProperty("withAir")
  private String withAir = null;

  @JsonProperty("withWater")
  private String withWater = null;

  @JsonProperty("with6mhci")
  private String with6mhci = null;

  @JsonProperty("with15mhno3")
  private String with15mhno3 = null;

  @JsonProperty("with6MNaOH")
  private String with6MNaOH = null;

  public ChemicalElementReactions withAir(String withAir) {
    this.withAir = withAir;
    return this;
  }

   /**
   * Get withAir
   * @return withAir
  **/
  @ApiModelProperty(value = "")


  public String getWithAir() {
    return withAir;
  }

  public void setWithAir(String withAir) {
    this.withAir = withAir;
  }

  public ChemicalElementReactions withWater(String withWater) {
    this.withWater = withWater;
    return this;
  }

   /**
   * Get withWater
   * @return withWater
  **/
  @ApiModelProperty(value = "")


  public String getWithWater() {
    return withWater;
  }

  public void setWithWater(String withWater) {
    this.withWater = withWater;
  }

  public ChemicalElementReactions with6mhci(String with6mhci) {
    this.with6mhci = with6mhci;
    return this;
  }

   /**
   * Get with6mhci
   * @return with6mhci
  **/
  @ApiModelProperty(value = "")


  public String getWith6mhci() {
    return with6mhci;
  }

  public void setWith6mhci(String with6mhci) {
    this.with6mhci = with6mhci;
  }

  public ChemicalElementReactions with15mhno3(String with15mhno3) {
    this.with15mhno3 = with15mhno3;
    return this;
  }

   /**
   * Get with15mhno3
   * @return with15mhno3
  **/
  @ApiModelProperty(value = "")


  public String getWith15mhno3() {
    return with15mhno3;
  }

  public void setWith15mhno3(String with15mhno3) {
    this.with15mhno3 = with15mhno3;
  }

  public ChemicalElementReactions with6MNaOH(String with6MNaOH) {
    this.with6MNaOH = with6MNaOH;
    return this;
  }

   /**
   * Get with6MNaOH
   * @return with6MNaOH
  **/
  @ApiModelProperty(value = "")


  public String getWith6MNaOH() {
    return with6MNaOH;
  }

  public void setWith6MNaOH(String with6MNaOH) {
    this.with6MNaOH = with6MNaOH;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChemicalElementReactions chemicalElementReactions = (ChemicalElementReactions) o;
    return Objects.equals(this.withAir, chemicalElementReactions.withAir) &&
        Objects.equals(this.withWater, chemicalElementReactions.withWater) &&
        Objects.equals(this.with6mhci, chemicalElementReactions.with6mhci) &&
        Objects.equals(this.with15mhno3, chemicalElementReactions.with15mhno3) &&
        Objects.equals(this.with6MNaOH, chemicalElementReactions.with6MNaOH);
  }

  @Override
  public int hashCode() {
    return Objects.hash(withAir, withWater, with6mhci, with15mhno3, with6MNaOH);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChemicalElementReactions {\n");
    
    sb.append("    withAir: ").append(toIndentedString(withAir)).append("\n");
    sb.append("    withWater: ").append(toIndentedString(withWater)).append("\n");
    sb.append("    with6mhci: ").append(toIndentedString(with6mhci)).append("\n");
    sb.append("    with15mhno3: ").append(toIndentedString(with15mhno3)).append("\n");
    sb.append("    with6MNaOH: ").append(toIndentedString(with6MNaOH)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

