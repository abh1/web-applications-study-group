package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChemicalElementCost
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

public class ChemicalElementCost   {
  @JsonProperty("pure")
  private BigDecimal pure = null;

  public ChemicalElementCost pure(BigDecimal pure) {
    this.pure = pure;
    return this;
  }

   /**
   * Get pure
   * @return pure
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getPure() {
    return pure;
  }

  public void setPure(BigDecimal pure) {
    this.pure = pure;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChemicalElementCost chemicalElementCost = (ChemicalElementCost) o;
    return Objects.equals(this.pure, chemicalElementCost.pure);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pure);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChemicalElementCost {\n");
    
    sb.append("    pure: ").append(toIndentedString(pure)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

