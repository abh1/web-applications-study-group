package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChemicalElementIonizationPotentialEv
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

public class ChemicalElementIonizationPotentialEv   {
  @JsonProperty("1")
  private BigDecimal _1 = null;

  @JsonProperty("2")
  private BigDecimal _2 = null;

  @JsonProperty("3")
  private BigDecimal _3 = null;

  public ChemicalElementIonizationPotentialEv _1(BigDecimal _1) {
    this._1 = _1;
    return this;
  }

   /**
   * Get _1
   * @return _1
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal get1() {
    return _1;
  }

  public void set1(BigDecimal _1) {
    this._1 = _1;
  }

  public ChemicalElementIonizationPotentialEv _2(BigDecimal _2) {
    this._2 = _2;
    return this;
  }

   /**
   * Get _2
   * @return _2
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal get2() {
    return _2;
  }

  public void set2(BigDecimal _2) {
    this._2 = _2;
  }

  public ChemicalElementIonizationPotentialEv _3(BigDecimal _3) {
    this._3 = _3;
    return this;
  }

   /**
   * Get _3
   * @return _3
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal get3() {
    return _3;
  }

  public void set3(BigDecimal _3) {
    this._3 = _3;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChemicalElementIonizationPotentialEv chemicalElementIonizationPotentialEv = (ChemicalElementIonizationPotentialEv) o;
    return Objects.equals(this._1, chemicalElementIonizationPotentialEv._1) &&
        Objects.equals(this._2, chemicalElementIonizationPotentialEv._2) &&
        Objects.equals(this._3, chemicalElementIonizationPotentialEv._3);
  }

  @Override
  public int hashCode() {
    return Objects.hash(_1, _2, _3);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChemicalElementIonizationPotentialEv {\n");
    
    sb.append("    _1: ").append(toIndentedString(_1)).append("\n");
    sb.append("    _2: ").append(toIndentedString(_2)).append("\n");
    sb.append("    _3: ").append(toIndentedString(_3)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

