package io.swagger.api;

import io.swagger.model.ChemicalElement;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

@Controller
public class ElementApiController implements ElementApi {



    public ResponseEntity<List<ChemicalElement>> findElementsByAtomicNumberRange( @NotNull@ApiParam(value = "Minimum atomic number, inclusive", required = true) @RequestParam(value = "min", required = true) Integer min,
         @NotNull@ApiParam(value = "Maximum atomic number, inclusive", required = true) @RequestParam(value = "max", required = true) Integer max,
        @ApiParam(value = "Language code") @RequestParam(value = "lang", required = false) String lang) {
        // do some magic!
        return new ResponseEntity<List<ChemicalElement>>(HttpStatus.OK);
    }

    public ResponseEntity<ChemicalElement> getElementByAtomicNumber(@ApiParam(value = "atomic number of element to return",required=true ) @PathVariable("atomicNumber") Integer atomicNumber,
        @ApiParam(value = "Language code") @RequestParam(value = "lang", required = false) String lang) {
        // do some magic!
        return new ResponseEntity<ChemicalElement>(HttpStatus.OK);
    }

    public ResponseEntity<ChemicalElement> getElementByCategoryAndSymbol( @NotNull@ApiParam(value = "category to search.  Valid categories are: nonmetal metalloid halogen noble-gas alkali-metal alkaline-earth-metal lanthanoid actinoid transition-metal post-transition-metal", required = true) @RequestParam(value = "category", required = true) String category,
         @NotNull@ApiParam(value = "Element symbol to match.  E.g. Xe", required = true) @RequestParam(value = "symbol", required = true) String symbol,
        @ApiParam(value = "Language code") @RequestParam(value = "lang", required = false) String lang) {
        // do some magic!
        return new ResponseEntity<ChemicalElement>(HttpStatus.OK);
    }

}
