package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ChemicalElementElectronConfiguration
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:25:37.094Z")

public class ChemicalElementElectronConfiguration   {
  @JsonProperty("predicted")
  private String predicted = null;

  @JsonProperty("observed")
  private String observed = null;

  public ChemicalElementElectronConfiguration predicted(String predicted) {
    this.predicted = predicted;
    return this;
  }

   /**
   * Get predicted
   * @return predicted
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getPredicted() {
    return predicted;
  }

  public void setPredicted(String predicted) {
    this.predicted = predicted;
  }

  public ChemicalElementElectronConfiguration observed(String observed) {
    this.observed = observed;
    return this;
  }

   /**
   * Get observed
   * @return observed
  **/
  @ApiModelProperty(value = "")


  public String getObserved() {
    return observed;
  }

  public void setObserved(String observed) {
    this.observed = observed;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChemicalElementElectronConfiguration chemicalElementElectronConfiguration = (ChemicalElementElectronConfiguration) o;
    return Objects.equals(this.predicted, chemicalElementElectronConfiguration.predicted) &&
        Objects.equals(this.observed, chemicalElementElectronConfiguration.observed);
  }

  @Override
  public int hashCode() {
    return Objects.hash(predicted, observed);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChemicalElementElectronConfiguration {\n");
    
    sb.append("    predicted: ").append(toIndentedString(predicted)).append("\n");
    sb.append("    observed: ").append(toIndentedString(observed)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

