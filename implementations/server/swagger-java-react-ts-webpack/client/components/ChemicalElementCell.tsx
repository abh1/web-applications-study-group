import * as React from 'react';
import { ChemicalElement } from '../generated/api.ts';
import { IDetailDisplayer } from "./PeriodicTable.tsx";

interface IChemicalElementProps {
    info: ChemicalElement, 
    languageCode: string,
    detailDisplayer: IDetailDisplayer
}

export default class ChemicalElementCell extends React.Component<IChemicalElementProps, any> {

    constructor(props: IChemicalElementProps) {
        super(props);
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }


    render() {
        if (this.props.info) {
            let atomicNumber = this.props.info.number;
            return (
                <td className={"element " + this.props.info.category} onClick={this.handleClick}>
                    <div className="atomic-number">{atomicNumber}</div>
                    <div className="symbol">{this.props.info.symbol}</div>
                    <div className="name">{this.props.info.name}</div>
                </td>
            );
        } else {
            return <td>Bad!</td>
        }
    }

    handleClick() {
        this.props.detailDisplayer.displayElementDetails(this.props.info);
    }
}
