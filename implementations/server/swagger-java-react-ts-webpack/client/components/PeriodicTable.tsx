import * as React from 'react';
import PeriodicTableRow from './PeriodicTableRow.tsx';
import BohrDiagram from './BohrDiagram.tsx';
import { ChemicalElement } from '../generated/api.ts';
import translationManager from '../TranslationManager.ts';

export interface IDetailDisplayer {
    displayElementDetails(element: ChemicalElement); 
}

interface IPeriodicTableState {
    languageCode: string,
    currentSelectedElement: ChemicalElement
}

export default class PeriodicTable extends React.Component<any, IPeriodicTableState> implements IDetailDisplayer {

    constructor() {
        super();
        this.state = {
            languageCode: 'en',
            currentSelectedElement: null,
        };
        this.handleSelectLanguage = this.handleSelectLanguage.bind(this);
    }

    colGroupElements(count) {
        let ret = [];
        for (let i = 0; i < count; i++) {
            ret.push(<col key={i.toString()} className="group" />)
        }
        return ret;
    }

    colHeaders(count) {
        let ret = [];
        for (let i = 0; i < count; i++) {
            ret.push(<th key={i.toString()}>{i + 1}</th>)
        }
        return ret;
    }

    handleSelectLanguage(e) {
        let newLanguageCode = e.target.value;
        translationManager.loadTranslations(newLanguageCode);
    
        this.setState({languageCode: newLanguageCode});
        document.title = translationManager.t("Heading.PeriodicTable", "The periodic table of the elements");
    
    }

    displayElementDetails(element) {
        this.setState({currentSelectedElement: element});
    }

    render() {

        return (
            
            <div className="periodic-table-app">

                <h1>{translationManager.t("Heading.PeriodicTable", "The periodic table of the elements")}</h1>
                <dt>{translationManager.t("Label.SelectLanguage", "Select language:")}</dt><dd>
                    <select id="language-selector" value={this.state.languageCode} onChange={this.handleSelectLanguage}>
                        <option value="en">English</option>
                        <option value="de">deutsch</option>
                        <option value="fr">français</option>
                        <option value="es">español</option>
                        <option value="it">italiano</option>
                    </select>
                </dd>

                <h2>{translationManager.t("Heading.MainTable", "Main Table")}</h2>
                <table id="main-table">
                    <colgroup>
                        <col className="periods" />
                        {this.colGroupElements(18)}
                    </colgroup>		
                    <thead>
                        <tr>
                            <th/>
                            {this.colHeaders(18)}
                        </tr>
                    </thead>
                    <tbody>
                        <PeriodicTableRow period="Period 1" rowLength={18} left={1} leftCount={1} right={2} rightCount={1} languageCode={this.state.languageCode} detailDisplayer={this} />
                        <PeriodicTableRow period="Period 2" rowLength={18} left={3} leftCount={2} right={10} rightCount={6} languageCode={this.state.languageCode} detailDisplayer={this} />
                        <PeriodicTableRow period="Period 3" rowLength={18} left={11} leftCount={2} right={18} rightCount={6} languageCode={this.state.languageCode} detailDisplayer={this}  />
                        <PeriodicTableRow period="Period 4" rowLength={18} left={19} leftCount={18} right={0} rightCount={0} languageCode={this.state.languageCode} detailDisplayer={this} />
                        <PeriodicTableRow period="Period 5" rowLength={18} left={37} leftCount={18} right={0} rightCount={0} languageCode={this.state.languageCode} detailDisplayer={this} />
                        <PeriodicTableRow period="Period 6" rowLength={18} left={55} leftCount={2} right={86} rightCount={15} languageCode={this.state.languageCode} detailDisplayer={this} />
                        <PeriodicTableRow period="Period 7" rowLength={18} left={87} leftCount={2} right={118} rightCount={15} languageCode={this.state.languageCode} detailDisplayer={this} />
                    </tbody>
                </table>


                <h2>{translationManager.t("Heading.Lanthanides", "Lanthanides")}</h2>

                <table id="lanthanides-table">
                    <colgroup>
                        <col className="periods" />
                        {this.colGroupElements(15)}
                    </colgroup>		

                    <tbody>
                        <PeriodicTableRow period="" rowLength={15} left={57} leftCount={15} right={0} rightCount={0} languageCode={this.state.languageCode} detailDisplayer={this} />
                    </tbody>
                </table>	

                <h2>{translationManager.t("Heading.Actinides", "Actinides")}</h2>

                <table id="actinides-table">
                    <colgroup>
                        <col className="periods" />
                        {this.colGroupElements(15)}
                    </colgroup>		

                    <tbody>
                        <PeriodicTableRow period="" rowLength={15} left={89} leftCount={15} right={0} rightCount={0} languageCode={this.state.languageCode} detailDisplayer={this} />
                    </tbody>
                </table>

                <BohrDiagram elementToDisplay={this.state.currentSelectedElement} languageCode={this.state.languageCode}/>
            </div>
        );
    }
}
