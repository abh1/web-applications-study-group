import * as React from 'react';
import ChemicalElementCell from "./ChemicalElementCell.tsx";
import { IDetailDisplayer } from "./PeriodicTable.tsx";
import { ElementApi, ChemicalElement } from "../generated/api.ts";

interface IPeriodicTableRowProps {
    languageCode: string,
    rowLength: number,
    left: number,
    leftCount: number,
    right: number,
    rightCount: number,
    period: string,
    detailDisplayer: IDetailDisplayer
}

interface IPeriodicTableRowState {
    loadingError: boolean,
    leftLoading: boolean,
    leftElements: Array<ChemicalElement>,
    rightLoading: boolean,
    rightElements: Array<ChemicalElement>
}

export default class PeriodicTableRow extends React.Component<IPeriodicTableRowProps, IPeriodicTableRowState> {

    constructor(props: IPeriodicTableRowProps) {
        super(props);
        this.state = {loadingError: false, leftLoading: true, leftElements: null, rightLoading: true, rightElements: null};

        let api = new ElementApi(undefined, "http://localhost:8081/periodic-table/v1");
        // TODO: ignored!
        let customHeaders : Headers = new Headers({"Accept": "application/json"});
        api.findElementsByAtomicNumberRange(
            {"min": this.props.left, "max": this.props.left + this.props.leftCount - 1, "lang": this.props.languageCode},
            {headers: customHeaders}
        ).then(elementList => {
            // re-render with loaded elements
            this.setState({leftLoading: false, leftElements: elementList});
        }).catch(error => {
            this.setState({loadingError: true, leftLoading: false, leftElements: null});
        });

        api.findElementsByAtomicNumberRange(
            {"min": this.props.right - this.props.rightCount + 1, "max": this.props.right, "lang": this.props.languageCode},
            {headers: customHeaders}
        ).then(elementList => {
            // re-render with loaded elements
            this.setState({rightLoading: false, rightElements: elementList});
        }).catch(error => {
            this.setState({loadingError: true, rightLoading: false, rightElements: null});
        });

        
    }

    populateTdListWithElements(tdList: Array<any>) {

        let rowLength: number = this.props.rowLength;
        let leftmostAtomicNumber: number = this.props.left;
        let leftCount: number = this.props.leftCount;
        let rightmostAtomicNumber: number = this.props.right;
        let rightCount: number = this.props.rightCount;
        
        for (let i = 0; i < leftCount; i++) {
            let atomicNumber = leftmostAtomicNumber + i;
            tdList[i] = 
                <ChemicalElementCell key={i} info={this.state.leftElements[i]} 
                languageCode={this.props.languageCode} detailDisplayer={this.props.detailDisplayer}/>
        }
        let fillTo = rowLength;
        if (rightmostAtomicNumber > leftmostAtomicNumber) {
            for (let i = 0; i < rightCount; i++) {
                let atomicNumber = rightmostAtomicNumber - rightCount + i + 1;
                let index = rowLength - rightCount + i + 1;
                tdList[index] = <ChemicalElementCell key={index} info={this.state.rightElements[i]}
                languageCode={this.props.languageCode} detailDisplayer={this.props.detailDisplayer}/>
            }
            fillTo = rowLength - rightCount;
        }
        for (let i = leftCount; i < fillTo; i++) {
            tdList[i] = <td key={i} />
        }
    }

    fillTdList(tdList: Array<any>, message: string) {

        let rowLength: number = this.props.rowLength;
        let leftmostAtomicNumber: number = this.props.left;
        let leftCount: number = this.props.leftCount;
        let rightmostAtomicNumber: number = this.props.right;
        let rightCount: number = this.props.rightCount;
        
        for (let i = 0; i < leftCount; i++) {
            let atomicNumber = leftmostAtomicNumber + i;
            tdList[i] = <td key={i}>{message}</td>
        }
        let fillTo = rowLength;
        if (rightmostAtomicNumber > leftmostAtomicNumber) {
            for (let i = 0; i < rightCount; i++) {
                let atomicNumber = rightmostAtomicNumber - i;
                let index = rowLength - i;
                tdList[index] = <td key={index}>{message}</td>
            }
            fillTo = rowLength - rightCount;
        }
        for (let i = leftCount; i < fillTo; i++) {
            tdList[i] = <td key={i} />
        }
    }
        
    render() {
        let tdList = new Array(this.props.rowLength);

        if (this.state.loadingError) {
            this.fillTdList(tdList, "ERR");
        } else if (this.state.leftLoading || this.state.rightLoading) {
            this.fillTdList(tdList, "...");
        } else {
            this.populateTdListWithElements(tdList);
        }
        return (
            <tr className="period">
                <th>{this.props.period}</th>
                {tdList}
            </tr>
        );
    }
}
