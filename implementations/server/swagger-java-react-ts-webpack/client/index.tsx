require('./periodic-table.css');
//core-js polyfills for IE
require('core-js');

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './components/App.tsx';

ReactDOM.render(<App />, document.getElementById('root'));
