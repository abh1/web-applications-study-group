const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './client/index.html',
    filename: 'index.html',
    inject: 'body'
})

module.exports = {
    devtool: 'inline-source-map',
    entry: {
        'app': ['./client/index.tsx']
    },

    output: {
        path: path.resolve('dist'),
        filename: 'index_bundle.js'
    },
    module: {
        loaders: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ },
            { test: /\.tsx?$/, loader: 'ts-loader', exclude: [/node_modules/, /swagger\-editor/] },
            { test: /\.css$/, use: ["style-loader", "css-loader"] },
            { test: /\.xlf$/, use: 'raw-loader' }
        ]
    },
    plugins: [HtmlWebpackPluginConfig]
}