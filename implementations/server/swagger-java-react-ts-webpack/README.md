Periodic Table implementation in swagger + react + typescript + webpack + Java Swing Boot

You need to install node.js and yarn


Compile
-------
yarn run comile-server

Run
---

yarn run start-server
yarn run start-dev

Go to http://localhost:8080/ to see the table.

Generate
--------

Edit periodic-table-swagger.yaml.  You might like to copy/paste into the Swagger Editor website.  Then run:

yarn run generate-server
yarn run generate-api-client

The generation process writes a "generated" code folder under "server" or "client".


Bugs
----
Fix these sometime

- Swagger UI on the server is broken
- Can't set the Accept header in the AJAX request.

