using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace angular4_ts_jpa1.Controllers
{
    [Route("api")]
    public class ElementsController : Controller
    {
        private static readonly JArray _elementsArray = LoadElements();

        [HttpGet("[action]")]
        public ContentResult Elements()
        {
            return this.Content(_elementsArray.ToString());
        }

        [HttpGet("[action]/{number}")]
        public ContentResult Elements(int number)
        {
            JObject element = (JObject)_elementsArray.First(x => ((int)(JValue)x["number"]) == number);
            return this.Content(element.ToString());
        }

        private static JArray LoadElements()
        {
            JArray elements = (JArray)JToken.Parse(System.IO.File.ReadAllText(@"Elements.json"));
            int[] nonmetals = { 1, 6, 7, 8, 15, 16, 34 };
            int[] metalloids = { 5, 14, 32, 33, 51, 52, 84 };
            int[] halogens = { 9, 17, 35, 53, 85, 117 };
            int[] noble_gases = { 2, 10, 18, 36, 54, 86, 118 };
            int[] alkali_metal = { 3, 11, 19, 37, 55, 87 };
            int[] alkaline_earth_metal = { 4, 12, 20, 38, 56, 88 };
            int[] lanthanoid = { 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71 };
            int[] actinoid = { 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103 };
            int[] transition_metal = { 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 72, 73, 74, 75, 76, 77, 78, 79, 80, 104, 105, 106, 107, 108, 109, 110, 111, 112 };
            int[] post_transition_metal = { 13, 31, 49, 50, 81, 82, 83, 113, 114, 115, 116 };
            foreach (JObject element in elements)
            {
                int number = (int)(JValue)element["number"];
                if (nonmetals.Contains(number))
                {
                    element["class"] = "OtherNonMetals";
                }
                else if (metalloids.Contains(number))
                {
                    element["class"] = "Metalloids";
                }
                else if (halogens.Contains(number))
                {
                    element["class"] = "Halogens";
                }
                else if (noble_gases.Contains(number))
                {
                    element["class"] = "NobelGases";
                }
                else if (alkali_metal.Contains(number))
                {
                    element["class"] = "AlkaliMetals";
                }
                else if (alkaline_earth_metal.Contains(number))
                {
                    element["class"] = "AlkalineEarthMetals";
                }
                else if (lanthanoid.Contains(number))
                {
                    element["class"] = "";
                }
                else if (actinoid.Contains(number))
                {
                    element["class"] = "Actinoids";
                }
                else if (transition_metal.Contains(number))
                {
                    element["class"] = "TransitionMetals";
                }
                else if (post_transition_metal.Contains(number))
                {
                    element["class"] = "PostTransitionMetals";
                }
            }
            return elements;
        }
    }
}
