    export class ElementCategoryData {
        nonmetal: number[];
        metalloid: number[];
        halogen: number[];
        noble_gas: number[];
        alkali_metal: number[];
        alkaline_earth_metal: number[];
        lanthanoid: number[];
        actinoid: number[];
        transition_metal: number[];
        post_transition_metal: number[];
    }
