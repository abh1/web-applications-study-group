import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Element } from './element';
import { ELEMENTS, ELEMENT_CATEGORY_DATA } from './mock-elements';

@Injectable()
export class ElementService {

    private elementsUrl = "api/elements";
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) { }


    getElements(): Promise<Element[]> {
        return this.http.get(this.baseUrl + this.elementsUrl).toPromise().then(response => <Element[]>(response.json())).catch(this.handleError);
    }

    getElement(number: number): Promise<Element> {
        return this.http.get(this.baseUrl + this.elementsUrl + "/" + number.toString()).toPromise().then(response => <Element>(response.json())).catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

    async findElementDataBySymbol(elementSymbol: string): Promise<Element | null> {
        let elements : Element[] = await this.getElements();
        for (let i = 0; i < elements.length; i++) {
            if (elementSymbol === elements[i].symbol) {
                return elements[i];
            }
        }
        return null;
    }
}