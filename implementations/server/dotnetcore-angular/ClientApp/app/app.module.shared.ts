import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { PeriodicTableComponent } from './components/periodic-table/periodic-table.component';
import { TableElementComponent } from './components/table-element/table-element.component';
import { ElementDetailsComponent } from './components/element-details/element-details.component';
import { BohrDiagramComponent } from './components/bohr-diagram/bohr-diagram.component';
import { AppComponent } from './components/app/app.component';
import { ElementService } from './element.service';

@NgModule({
    declarations: [
        PeriodicTableComponent,
        TableElementComponent,
        BohrDiagramComponent,
        AppComponent,
        ElementDetailsComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'periodictable', pathMatch: 'full' },
            { path: 'periodictable', component: PeriodicTableComponent },
            { path: 'elements/:number', component: ElementDetailsComponent },
            { path: '**', redirectTo: 'periodictable' }
        ])
    ],
    providers: [
        ElementService]
})
export class AppModuleShared {
}
