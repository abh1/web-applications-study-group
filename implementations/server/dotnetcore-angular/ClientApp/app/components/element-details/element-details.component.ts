﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import { Element } from '../../element';
import { ElementService } from '../../element.service';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/Map';

@Component({
    selector: 'element-detais',
    templateUrl: './element-details.component.html'
})
export class ElementDetailsComponent implements OnInit {
    element: Element;
    constructor(
        private elementService: ElementService,
        private route: ActivatedRoute,
        private location: Location)
    { }


    async ngOnInit() {
        this.route.paramMap
            .switchMap((params: ParamMap) => this.elementService.getElement(parseInt(params.get('number') + "")))
            .subscribe((element: Element) => { console.log('running');  console.log(element.number); this.element = element });
    }

}