import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import {Element, ElectronConfiguration} from '../../element';
import { ElementService } from '../../element.service';

@Component({
  selector: 'bohr-diagram',
  templateUrl: './bohr-diagram.component.html'
})
export class BohrDiagramComponent implements OnChanges {

    @Input()
    element: Element;
    elements : Element[];
    electronRings : number[] = new Array(7);
    electrons: any[];
    isInited: boolean = false;

    constructor(private elementService: ElementService) { }

    // See ngOnChanges
//    ngOnInit()
//    {
//    }

    findElementDataBySymbol(elementSymbol : string) : Element | null
    {
        for (let i = 0; i <this.elements.length; i++)
            {
            if (elementSymbol === this.elements[i].symbol)
            {
                return this.elements[i];
            }
        }
        return null;      
    }

    getElectronConfiguration(elementData : Element) : number[] {
        let ret : number[] = [];
        let atomicNumber = elementData.number;
        let electronCount = atomicNumber;

        let electronsSoFar = 0;

        if (elementData.electronConfiguration) {
            let configInfo : ElectronConfiguration = elementData.electronConfiguration;
            if (configInfo.observed) {
                let observedConfig : string = configInfo.observed;
                let electronConfigurationDescription : RegExpMatchArray | null = observedConfig.match(/(\[([A-Za-z]+)\])?([0-9spdfg ]*)/);
                let baseNobleGasSymbol : string | null = null;
                if (electronConfigurationDescription != null) {
                    baseNobleGasSymbol = electronConfigurationDescription[2];
                }
                if (baseNobleGasSymbol) {
                    let baseNobleGasElementData: Element | null = this.findElementDataBySymbol(baseNobleGasSymbol);
                    if (baseNobleGasElementData) {
                        ret = this.getElectronConfiguration(baseNobleGasElementData);
                    }
                }
                if (electronConfigurationDescription != null && 3 in electronConfigurationDescription) {
                    // specialisation of noble gas
                    let extraSubshellDescription = electronConfigurationDescription[3];
                    let subshellPattern = /([0-9])([spdfg])([0-9]+)/g;
                    while (true) {
                        let subshellSpec = subshellPattern.exec(extraSubshellDescription);
                        if (subshellSpec === null) {
                            break;
                        }
                        let shellNumber = parseInt(subshellSpec[1]);
                        let subshellLetter = subshellSpec[2];
                        let electronCount = parseInt(subshellSpec[3]);
                        if (shellNumber in ret) {
                            ret[shellNumber] += electronCount; //TODO: show subshells somehow
                        } else {
                            ret[shellNumber] = electronCount;
                        }
                    }
                }

            }
        }
        return ret;
    }

    async ngOnChanges(changes : SimpleChanges)
    {
        // This gets called before OnOnit! Therefore use this method to
        // do initialization logic rather than OnInit and keep track of whether
        // we have already inited
        if (!this.isInited)
            {
                this.elements = await this.elementService.getElements();
                this.isInited = true;             
            }
        if (changes.element)
        {
            for (let i = 0; i < 7; i++)
                {
                    this.electronRings[i] = 70 + i * 30;
                }
        
                let configuration : number[] = this.getElectronConfiguration(this.element);
                this.electrons = new Array();
                for (let electronShellNumber : number = 1; electronShellNumber < configuration.length; electronShellNumber++)
                {
                    let filledSlots = configuration[electronShellNumber];
                    let displayRadius : number = 40 + electronShellNumber * 30;
                    let angleIncrement : number = Math.PI * 2 / filledSlots;
                    let angle : number = 0;
                    //go anticlockwise from 12 o'clock.  x = -r * sin(theta), y = -r * cos(theta)
                    for (let slotIndex = 0; slotIndex < filledSlots; slotIndex++) {
                        let x = 280 - displayRadius * Math.sin(angle);
                        let y = 280 - displayRadius * Math.cos(angle);
        
                        let electron = {x: x, y:y, key: electronShellNumber * 100 + slotIndex};
        
                        this.electrons.push(electron);
        
                        angle += angleIncrement;
                    }
                }
        
        }
    }
}
