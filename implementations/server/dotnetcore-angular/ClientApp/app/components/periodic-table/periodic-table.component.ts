import { Component, OnInit } from '@angular/core';
import { Element } from '../../element';
import { ElementService } from '../../element.service';
import { Router } from '@angular/router';


@Component({
  selector: 'periodic-table',
  templateUrl: './periodic-table.component.html',
  styleUrls: ['./periodic-table.component.css']
})
export class PeriodicTableComponent implements OnInit {
  title = 'Periodic Table';
  elements : Element[];
  selectedElement : Element;
  constructor(private elementService: ElementService, private _router : Router) { }

  async ngOnInit() : Promise<void>
  {
    this.elements = await this.elementService.getElements();
  }

  onSelect(element: Element): void {
      this._router.navigate(['/elements', element.number] )
  }

}
