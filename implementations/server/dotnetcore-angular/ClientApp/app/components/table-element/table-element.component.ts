import { Component, Input } from '@angular/core';
import { Element } from '../../element';

@Component({
  selector: '[table-element]',
  templateUrl: './table-element.component.html',
  styleUrls: ['./table-element.component.css']
})
export class TableElementComponent {
    @Input()
    element: Element;

    classes() : string {
      return 'element ' + this.element.class;
    }
}